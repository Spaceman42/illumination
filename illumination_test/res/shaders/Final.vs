#version 330

in vec4 vertex;

out vec2 texCoord;

void main() {
	gl_Position = vertex;
	texCoord = ((vertex/2) + 0.5).xy;
}