#version 330
uniform sampler2D ao;
uniform sampler2D gColor;

uniform vec2 buffersize;
in vec2 samplePosition;
out vec4 outColor;

void main() {
	vec4 color = texture2D(ao, samplePosition);
	vec2 texelSize = 1/buffersize;
	float result = 0.0;
	for (int i = -2; i < 2; ++i) {
		for (int j = -2; j < 2; ++j) {
			vec2 offset = vec2(texelSize.x * float(j), texelSize.y * float(i));
			result += texture(ao, samplePosition + offset).r;
		}
   } 
   result = result/16*2;
   vec4 pixelColor = texture2D(gColor, samplePosition);
  
   outColor = vec4((1-result)*pixelColor);
 
}