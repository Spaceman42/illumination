#version 330
in vec4 vertex;

out vec2 samplePosition;

void main() {
	samplePosition = samplePosition = ((vertex+1)/2).xy;
	gl_Position = vertex;
}