#version 330

in vec4 vertex;

uniform mat4 lMat;

void main() {
	gl_Position = lMat * vertex;
}