#version 330


in vec2 texCoord;
in vec3 surfaceNormal;
in vec3 surfaceTangent;
in vec3 surfaceBitangent;

uniform sampler2D texture;
uniform sampler2D normalMap;
uniform sampler2D specular;

uniform float specOverride;
uniform int hasNormalMap;

uniform vec3 colorIN;

uniform mat3 nMat;

uniform mat3 mvMat3;

layout (location = 0) out vec4 color;
layout (location = 1) out vec4 outNormal;


void main() {
	vec3 normalViewspace = vec3(0);
	
	if (hasNormalMap == 1) {
		vec3 tangent = nMat * normalize(surfaceTangent);
		vec3 bitangent = nMat * normalize(surfaceBitangent);
		vec3 normalIn = nMat * normalize(surfaceNormal);
		mat3 tbn = mat3(tangent, bitangent, normalIn);
		normalViewspace = tbn * normalize(texture2D(normalMap, texCoord).rgb*2-1);
	} else {
		normalViewspace = normalize(nMat * surfaceNormal);
	}
		
	
	normalViewspace.y *= -1;
	//normalViewspace.z *= -1;
	outNormal = vec4(normalViewspace*0.5+0.5,0.1); //added ambient light
	
	float spec = texture2D(specular, texCoord).x;
	if (specOverride != -1) { //Make this better
		spec = specOverride;
	}
	color = vec4(texture2D(texture, texCoord).xyz, spec);// * vec4(colorIN,1); //Added 4th term as specular intensity
}