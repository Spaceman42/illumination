#version 330
in vec4 vertex;
uniform mat4 mMat;
uniform vec2 texSize;
uniform vec2 texLocation;
out vec2 samplePosition;
void main() {
	vec2 samplePosition2 = ((vertex * 0.5) + 0.5).xy;
	samplePosition2 *= texSize;
	samplePosition2 += texLocation;
	samplePosition2.y -= texSize.y;
	samplePosition = vec2(samplePosition2.x, -samplePosition2.y);
	gl_Position = mMat * vertex;
}