#version 330

uniform sampler2D image;
uniform vec4 color;

in vec2 samplePosition;

out vec4 outColor;

void main() {
	outColor = texture2D(image, samplePosition) * color;
}