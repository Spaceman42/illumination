#version 330
in vec4 vertex;
uniform mat4 mMat;
out vec2 samplePosition;
void main() {
	vec2 samplePosition2 = ((vertex * 0.5) + 0.5).xy;
	samplePosition = vec2(samplePosition2.x, -samplePosition2.y);
	gl_Position = mMat * vertex;
}