#version 330

uniform sampler2D gColor;

in vec2 texCoord;

out vec4 color;

void main() {
	
	float min = 0.495;
	float max = 0.505;
	if (texCoord.x > min && texCoord.x < max && texCoord.y > min && texCoord.y < max) {
		color = vec4(0,1,0,1);
	} else {
		color = texture2D(gColor, texCoord);
	}
	
}