#version 330

uniform sampler2D uColor;
uniform float cutoff;
uniform int blurSize;
uniform vec2 buffersize;

in vec2 samplePosition;

out vec4 outColor;

vec3 getColor(vec2 samplePosition);
vec3 getBloomColor(vec2 samplePosition);
vec3 getBloomColor2(vec2 samplePosition, vec3 pixelColor);

void main() {

	vec2 texelSize = 1/buffersize;
	vec3 result = vec3(0.0);
	
	int halfBlurSize = blurSize/2;
	vec3 pixelColor = getColor(samplePosition);
	if (length(pixelColor) > cutoff) {
	for (int i = -halfBlurSize; i < halfBlurSize; ++i) {
		for (int j = -halfBlurSize; j < halfBlurSize; ++j) {
			vec2 offset = vec2(texelSize.x * float(j), texelSize.y * float(i));
			result += getBloomColor2(samplePosition + offset, pixelColor);
		}
   } 
   
   outColor = vec4(result/(blurSize*blurSize), 1);
  } else {
	outColor = vec4(pixelColor,1);
	}
}

vec3 getColor(vec2 samplePosition) {
	return texture2D(uColor, samplePosition).xyz;
}

vec3 getBloomColor(vec2 samplePosition) {
	vec3 color = getColor(samplePosition);
	vec3 bloomColor = vec3(0);
	bloomColor.x = max(0.0, color.x-cutoff);
	bloomColor.y = max(0.0, color.y-cutoff);
	bloomColor.z = max(0.0, color.z-cutoff);
	return bloomColor;
}
vec3 getBloomColor2(vec2 samplePosition, vec3 pixelColor) {
	vec3 color = getColor(samplePosition);
	vec3 bloomColor = pixelColor;
	if (length(color) > cutoff) {
		bloomColor = color;
	}
	return bloomColor;
}
//Accumulation might help this