#version 330

in vec4 vertex;

uniform mat4 lMat;
uniform mat4 pMat;
uniform mat4 mMat;

void main() {

	gl_Position = pMat * lMat * mMat * vertex;
	
}