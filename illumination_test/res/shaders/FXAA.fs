#version 330

uniform sampler2D gColor;
uniform sampler2D depth;

uniform int samples;
uniform vec2 buffersize;

in vec2 samplePosition;

out vec4 pixelColor;

float isEdge(vec4 color);
vec4 getPixel(vec2 uv, vec2 pixelOffset, vec2 texelSize);
float averageIntensity(vec4 color);
float threshold(float min, float max, float value);


void main() {
	vec4 inColor = texture2D(gColor, samplePosition);
	float edge = isEdge(inColor);
	
	vec4 endColor = vec4(inColor);
	if (edge == 1) {
		vec2 texelSize =  1/buffersize;
		for (int i = -4; i<5; i+=1) {
			for (int j = -4; j<5; j+=1) {
				endColor += getPixel(samplePosition, vec2(i,j), texelSize);
			}
		}
		endColor /= 81;
	} else {
		endColor = inColor;
	}
	pixelColor = vec4(endColor);
	
}


float isEdge(vec4 color) {
	vec2 texelSize = 1/buffersize;
	float pix[9];
	int k = -1;
	for (int i = -1; i<2; i+=1) {
		for (int j = -1; j<2; j+=1) {
			k++;
			pix[k] = averageIntensity(getPixel(samplePosition, vec2(i,j), texelSize));
		}
	}
	float delta = (abs(pix[1]-pix[7])+
          abs(pix[5]-pix[3]) +
          abs(pix[0]-pix[8])+
          abs(pix[2]-pix[6])
           )/4;
	return threshold(0.25, 0.4, clamp(1.9*delta,0,1));
}

vec4 getPixel(vec2 uv, vec2 pixelOffset, vec2 texelSize) {
	return texture2D(gColor, uv +(texelSize * pixelOffset));
}

float averageIntensity(vec4 color) {
	return (color.x +color.y + color.z)/3;
}

float threshold(float min, float max, float value) {
	if (value < min) {return 0.0;}
	if (value > max) {return 1.0;}
	return value;
}