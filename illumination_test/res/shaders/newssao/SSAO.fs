#version 330

uniform sampler2D gNormal;
uniform sampler2D gDepth;
uniform sampler2D uRandom;

uniform float uRadius;
uniform float scale;
uniform float bias;
uniform float intensity;
uniform vec2 noiseScale;
uniform int samples;
uniform mat4 pMat;
uniform vec4 frustrumData;

uniform vec2 buffersize;

in vec2 samplePosition;

out vec4 pixelColor;


const vec3 kernel[16] = vec3[16](
	vec3(-0.02328596579519189, 0.034735921183989195, 0.05566357534654999),
	vec3(-0.01234617349454392, 0.013588351395025195, 0.011189876129240063),
	vec3(0.09235499645187982, 0.030140272579124962, 0.021280169920214448),
	vec3(0.020538336931784758, 0.003594963632020669, 0.01599492809529027),
	vec3(-0.03222859449372361, -0.02572387791508195, 0.016583255404924186),
	vec3(0.04707402775804572, -0.07395866657487592, 0.08570901878352731),
	vec3(6.529764945196446E-4, -0.0016264599032655568, 0.124613298272432E-4),
	vec3(0.022328388170995982, 0.009839639036574577, 0.031044456289104835),
	vec3(0.15534601301575227, -0.19378688088478016, 0.18077682579364449),
	vec3(-0.1322347962820745, 0.0687876713634948, 0.15715602916981494),
	vec3(-0.13395449663503675, 0.06059824699713091, 0.23260198885806532),
	vec3(0.07057907382038717, 0.19195689595370433, 0.022657704186020575),
	vec3(-0.00838693890926142, 0.01117846453003823, 0.017819498259124437),
	vec3(0.23516859097437315, 0.15520098621175596, 0.343318044722547),
	vec3(-0.15063077489966917, 0.23467399635873995, 0.14316480789428834),
	vec3(-0.04091084361872285, 0.06022208497292778, 0.023911253016718766)
);

vec3 getPosition(vec2 uv, float depth);
float getDepth(vec2 uv);
vec3 getRandom(vec2 uv);
float getLinearDepth(float depth);
void main() {
	float depth = texture2D(gDepth, samplePosition);
	vec3 normal = normalize(texture2D(gNormal, samplePosition)*2-1).xyz;
	
	vec3 random = getRandom(samplePosition);
	
	vec3 tangent = normalize(random - normal * dot(random, normal));
	vec3 bitangent = normalize(cross(normal, tangent));
	mat3 tbn;
	tbn[0] = tangent;//vec3(tangent.x, bitangent.x, normal.x);
	tbn[1] = bitangent;//vec3(tangent.y, bitangent.y, normal.y);
	tbn[2] = normal;//vec3(tangent.z, bitangent.z, normal.z);
	
	//tbn[0] = vec3(tangent.x, bitangent.x, normal.x);
	//tbn[1] = vec3(tangent.y, bitangent.y, normal.y);
	//tbn[2] = vec3(tangent.z, bitangent.z, normal.z);
	
	vec3 origin = getPosition(samplePosition, getDepth(samplePosition));
	float occlusion = 0.0;
	for (int i = 0; i<samples; i++) {
		
		vec3 vec = tbn*kernel[i];
		vec *= uRadius;
		vec = vec + origin;
		
		vec4 offset = vec4(vec, 1);
		offset = pMat * offset;
		offset.xy = offset.xy/offset.w;
		offset.xy = offset.xy * 0.5 - 0.5;
		
		float sampleDepth = getLinearDepth(getDepth(offset.xy));
		float range = 1;
		if (origin.z - sampleDepth > uRadius) {
			range = 0;
		}
		
		if (sampleDepth > origin.z + bias) {
			occlusion += 1*range;
		}
	}
//	if (occlusion/samples != 1) {
//		pixelColor = vec4(1,0,0,1);
//	} else {
//		pixelColor = vec4(0,1,0,1);
//	}
	pixelColor = vec4(1-occlusion/samples);

}

float getDepth(vec2 uv) {
	return texture2D(gDepth, uv).x;
}

float getLinearDepth(float depth) {
	float near = frustrumData.x;
	float far = frustrumData.y;
	float right = frustrumData.z;
	float top = frustrumData.w;
	return (near * far / ((depth * (far - near)) - far));
}

vec3 getPosition(vec2 uv, float depth) {
	float near = frustrumData.x;
	float far = frustrumData.y;
	float right = frustrumData.z;
	float top = frustrumData.w;
	vec2 ndc;           
	vec3 eye;             
	eye.z = (near * far / ((depth * (far - near)) - far));
	ndc.x = ((uv.x) - 0.5) * 2.0; 
	ndc.y = ((uv.y) - 0.5) * 2.0;
	eye.x = (-ndc.x * eye.z) * right/near;
	eye.y = (-ndc.y * eye.z) * top/near;
	
	return eye;
}

vec3 getRandom(vec2 uv) {
	return normalize(texture2D(uRandom, buffersize * uv/noiseScale).xyz * 2.0 - 1.0);
}