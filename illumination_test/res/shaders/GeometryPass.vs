#version 330

in vec4 vertex;
in vec4 normal;
in vec2 uv;
in vec4 tangent;
in vec4 bitangent;


uniform mat4 mvpMat;

out vec3 surfaceNormal;
out vec2 texCoord;
out vec3 surfaceTangent;
out vec3 surfaceBitangent;

void main() {
	vec3 normalVec3 = vec3(normal.x, normal.y, normal.z);
	texCoord = uv;
	surfaceNormal = normalVec3;
	surfaceTangent = tangent.xyz;
	surfaceBitangent = bitangent.xyz;
	gl_Position = mvpMat * vec4(vertex);
}

