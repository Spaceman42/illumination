package test;

import java.io.IOException;

import com.ten.GameEngine;
import com.ten.game.AbstractGame;
import com.ten.game.DebugCamera;
import com.ten.game.Scene;
import com.ten.math.Vector4;

public class Main extends AbstractGame {

	public static void main(String[] args) throws IOException {
		Main main = new Main();
		
		GameEngine engine = new GameEngine(main);
		engine.run();
		
		DebugCamera cam = new DebugCamera(.1f, 100, 60, 800 / 600);
		main.setCamera(cam);
		main.setScene(new Scene("Yo"));
	}

	@Override
	public void onPause() {
		System.out.println("Paused");
	}

	@Override
	public void onClick(Object objectId, int screenX, int screenY,
			Vector4 worldPosition) {
		System.out.println("Clicked");
	}

	@Override
	public void onLoop(int deltaTime) {
		System.out.println("Looped");
	}
}
