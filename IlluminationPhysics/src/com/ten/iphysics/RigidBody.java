package com.ten.iphysics;

import com.ten.math.Matrix4;
import com.ten.math.Vector3;
import com.ten.math.Vector4;

public class RigidBody {

	private Matrix4 transform;
	private float mass;
	private Vector3 momentum;
	private BoundingShape bounding;
	private Vector3 scale;
	
	private Group group;
	
	public RigidBody(BoundingShape bounding, Vector3 scale, float mass, Vector3 momentum, Vector3 position, Group group) {
		this.bounding = bounding;
		this.scale = scale;
		this.mass = mass;
		this.momentum = momentum;
		this.group = group;
		transform = new Matrix4().setToTranslate(new Vector4(position));
	}
	/**
	 * adds a default empty group
	 * @param bounding
	 * @param scale
	 * @param mass
	 * @param momentum
	 * @param position
	 */
	public RigidBody(BoundingShape bounding, Vector3 scale, float mass, Vector3 momentum, Vector3 position) {
		this(bounding, scale, mass, momentum, position, new Group(true));
	}
	
	
	public boolean collides(RigidBody r) {
		
		return false;
	}
	
	public AABoundingBox generateAABB() {
		return null;
		//TODO: Make this work
	}
	
	public void setGroup(Group g) {
		group = g;
	}
	
	public Group getGroup() {
		return group;
	}
	
	public Vector3 getPosition() {
		return new Vector3(transform.getTranslation());
	}
}
