package com.ten.iphysics.convex;

import com.ten.graphics.renderer.model.Mesh;
import com.ten.math.Vector3;

public class ConvexHull {
	private Vector3[] vertices;
	
	/**
	 * The shape formed by the given vertices is assumed to be convex. Use {@code build()} to generate a convex hull from a mesh.
	 * @param vertices
	 */
	public ConvexHull(Vector3[] vertices) {
		this.vertices = vertices;
	}
	
	
	
	public static ConvexHull build(Mesh m) {
		//Generate a convex hull
		return null;
	}
}
