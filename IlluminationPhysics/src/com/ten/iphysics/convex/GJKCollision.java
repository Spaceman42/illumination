package com.ten.iphysics.convex;

import com.ten.math.Vector3;

public class GJKCollision {

	/**
	 * Returns the distance separating two convex hulls, will be negative is
	 * there is a collision.
	 * 
	 * @param obj1
	 *            The set of vertices for the first convex hull
	 * @param obj2
	 *            The set of vertices for the second convex hull
	 * @return The distance between obj2 and obj1
	 */
	public float collides(Vector3[] obj1, Vector3[] obj2) {
		return 0;
	}
}
