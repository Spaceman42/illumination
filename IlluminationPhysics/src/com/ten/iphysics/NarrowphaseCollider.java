package com.ten.iphysics;

import java.util.List;

public interface NarrowphaseCollider {
	public void doNarrowPhaseCollisions(List<CollisionPair> collisionPairs);
}
