package com.ten.iphysics;

public class CollisionPair {
	private RigidBody r0;
	private RigidBody r1;
	
	public CollisionPair(RigidBody r0, RigidBody r1) {
		this.r0 =r0;
		this.r1 = r1;
	}
	
	public RigidBody getFirstBody() {
		return r0;
	}
	
	public RigidBody getSecondBody() {
		return r1;
	}
}
