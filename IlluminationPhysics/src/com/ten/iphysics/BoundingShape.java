package com.ten.iphysics;

public interface BoundingShape {

	public boolean collides(BoundingShape shape);
	
	
}
