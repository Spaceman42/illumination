package com.ten.iphysics;

import com.ten.math.Vector3;

public class SimpleMesh {

	private Vector3[] vertices;
	
	public SimpleMesh(float[] vertices) {
		this.vertices = new Vector3[vertices.length / 3];
		for (int i = 0; i < this.vertices.length; i++) {
			this.vertices[i] = new Vector3(vertices[(i * 3)], vertices[(i * 3) + 1], vertices[(i * 3) + 2]);
		}
	}
	
	public SimpleMesh(Vector3[] vertices) {
		this.vertices = vertices;
	}
	
	public float[] getVerticesArray() {
		float[] vertices = new float[this.vertices.length * 3];
		for (int i = 0; i < this.vertices.length; i++) {
			vertices[(i * 3)] = this.vertices[i].x;
			vertices[(i * 3) + 1] = this.vertices[i].y;
			vertices[(i * 3) + 2] = this.vertices[i].z;
		}
		return vertices;
	}
	
	public Vector3[] getVertices() {
		return vertices;
	}
}
