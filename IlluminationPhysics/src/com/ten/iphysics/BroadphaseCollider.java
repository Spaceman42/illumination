package com.ten.iphysics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ten.math.Vector3;

public class BroadphaseCollider {

	
	public List<CollisionPair> doBroadphaseCollisions(Collection<RigidBody> bodies) {
		List<CollisionPair> pairs = new ArrayList<CollisionPair>();
		Map<RigidBody, Set<RigidBody>> compareLists = new HashMap<RigidBody, Set<RigidBody>>();
		
		for (RigidBody r : bodies) {
			Set<RigidBody> compairedToSet = new HashSet<RigidBody>();
			compareLists.put(r, compairedToSet);
			
			for (RigidBody cR : bodies) {
				if (!compareLists.get(cR).contains(cR)) {
					if (r.getGroup().collidesWithGroup(cR.getGroup()) && cR.getGroup().collidesWithGroup(r.getGroup())) {
						if (objectsCollide(r, cR)) {
							pairs.add(new CollisionPair(r, cR));
						}
					}
					compairedToSet.add(cR);
				}
			}
		}
		return pairs;
	}
	/**
	 * 
	 * @param r0
	 * @param r1
	 * @return {@code true} if the two AABBs of the objects collide. Otherwise returns {@code false}
	 */
	private boolean objectsCollide(RigidBody r0, RigidBody r1) {
		AABoundingBox bb0 = r0.generateAABB();
		AABoundingBox bb1 = r1.generateAABB();
		
		Vector3 origin0 = bb0.getOrigin().add(r0.getPosition());
		Vector3 origin1 = bb1.getOrigin().add(r1.getPosition());
		float[] deltaLocation = new Vector3(origin0.subtract(origin1)).toArray();
		
		float[] radii0 = bb0.getRadii();
		float[] radii1 = bb1.getRadii();
		
		//TODO: Check if both radii arrays contain the same number of values, if they don't throw an exception
		
		for (int i = 0; i<radii0.length; i++) {
			if (Math.abs(deltaLocation[i]) > radii0[i] + radii1[i]) {
				return false;
			}
		}
		return true;
	}
}
