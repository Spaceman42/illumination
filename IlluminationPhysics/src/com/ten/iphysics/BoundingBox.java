package com.ten.iphysics;

import com.ten.math.Vector3;

public class BoundingBox extends AbstractBoundingBox {

	public BoundingBox(float minX, float maxX, float minY, float maxY,
			float minZ, float maxZ) {
		super(minX, maxX, minY, maxY, minZ, maxZ);
	}
	
	public BoundingBox(Vector3 origin, float radiusX, float radiusY, float radiusZ) {
		super(origin, radiusX, radiusY, radiusZ);
	}

	public BoundingBox(SimpleMesh s) {
		super(s);
	}
	
	

	@Override
	public boolean collides(BoundingShape shape) {
		// TODO Auto-generated method stub
		return false;
	}

}
