package com.ten.iphysics;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PhysicsWorld {
	private BroadphaseCollider broadphase;
	private NarrowphaseCollider narrowphase;
	private Set<RigidBody> objects;
	
	private float gravity = 0;
	
	public PhysicsWorld(BroadphaseCollider broadphase, NarrowphaseCollider narrowphase) {
		this.broadphase = broadphase;
		this.narrowphase = narrowphase;
		objects = new HashSet<RigidBody>();
	}
	
	public void stepSimulation(int deltaTime) {
		List<CollisionPair> broadphaseCollisions = broadphase.doBroadphaseCollisions(objects);
		narrowphase.doNarrowPhaseCollisions(broadphaseCollisions);
		//application of forces (will need the delta time)
	}
}
