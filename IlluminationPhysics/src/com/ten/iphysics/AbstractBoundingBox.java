package com.ten.iphysics;

import com.ten.math.Vector3;

public abstract class AbstractBoundingBox implements BoundingShape {

	private Vector3 origin;
	private float radiusX;
	private float radiusY;
	private float radiusZ;
	
	public AbstractBoundingBox(Vector3 origin, float radiusX, float radiusY, float radiusZ) {
		this.origin = origin;
		this.radiusX = radiusX;
		this.radiusY = radiusY;
		this.radiusZ = radiusZ;
	}
	
	public AbstractBoundingBox(float minX, float maxX, float minY, float maxY,
			float minZ, float maxZ) {
		calcPointRadius(minX, maxX, minY, maxY, minZ, maxZ);
	}

	public AbstractBoundingBox(SimpleMesh s) {
		float minX = 0, maxX = 0;
		float minY = 0, maxY = 0;
		float minZ = 0, maxZ = 0;
		for (Vector3 v : s.getVertices()) {
			if (v.x > maxX)
				maxX = v.x;
			if (v.x < minX)
				minX = v.x;

			if (v.y > maxY)
				maxY = v.y;
			if (v.y < minY)
				minY = v.y;

			if (v.z > maxZ)
				maxZ = v.z;
			if (v.z < minZ)
				minZ = v.z;
		}
		calcPointRadius(minX, maxX, minY, maxY, minZ, maxZ);
	}
	
	private void calcPointRadius(float minX, float maxX, float minY, float maxY,
			float minZ, float maxZ) {
		calcOrigin(minX, maxX, minY, maxY, minZ, maxZ);
		radiusX = Math.abs(minX - maxX) / 2;
		radiusY = Math.abs(minY - maxY) / 2;
		radiusZ = Math.abs(minZ - maxZ) / 2;
	}

	private Vector3 calcOrigin(float minX, float maxX, float minY, float maxY,
			float minZ, float maxZ) {
		return new Vector3((minX + maxX) / 2, (minY + maxY) / 2,
				(minZ + maxZ) / 2);
	}
	
	public float[] getRadii() {
		return new float[] {radiusX, radiusY, radiusZ};
	}
	
	public Vector3 getOrigin() {
		return new Vector3(origin);
	}
}
