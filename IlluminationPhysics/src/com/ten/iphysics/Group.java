package com.ten.iphysics;

import java.util.HashMap;
import java.util.Map;

public class Group {
	private Map<Group, Boolean> collides;
	private boolean defualtAction;
	
	public Group(boolean defualtAction) {
		this.defualtAction = defualtAction;
		collides = new HashMap<Group, Boolean>();
	}
	
	public Group() {
		this(true);
	}
	
	public void addMapping(Group g, boolean collides) {
		this.collides.put(g, new Boolean(collides));
	}
	
	public boolean collidesWithGroup(Group g) {
		Boolean b = collides.get(g);
		if (b == null) {
			return defualtAction;
		} else {
			return b;
		}
	}
}
