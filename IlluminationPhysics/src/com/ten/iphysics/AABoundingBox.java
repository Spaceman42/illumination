package com.ten.iphysics;

import com.ten.math.Vector3;

public class AABoundingBox extends AbstractBoundingBox {

	public AABoundingBox(float minX, float maxX, float minY, float maxY,
			float minZ, float maxZ) {
		super(minX, maxX, minY, maxY, minZ, maxZ);
	}
	
	public AABoundingBox(Vector3 origin, float radiusX, float radiusY, float radiusZ) {
		super(origin, radiusX, radiusY, radiusZ);
	}

	public AABoundingBox(SimpleMesh s) {
		super(s);
	}

	@Override
	public boolean collides(BoundingShape s) {
		if (s instanceof AABoundingBox) {
			
		}
		return false;
	}
}
