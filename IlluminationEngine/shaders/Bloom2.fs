#version 330

uniform sampler2D bloomColor;
uniform sampler2D inColor;
//uniform sampler2D bColor;

uniform int blurSize;
uniform vec2 buffersize;

in vec2 samplePosition;

out vec4 color;

void main() {

	int halfBlurSize = blurSize/2;
	
	vec2 texelSize = 1/buffersize;
	vec4 pixelColor =texture2D(inColor, samplePosition);
	vec4 result = vec4(0,0,0,0);
	for (int i = -halfBlurSize; i < halfBlurSize; i+=1) {
		for (int j = -halfBlurSize; j < halfBlurSize; j+=1) {
			vec2 offset = vec2(texelSize.x * float(j), texelSize.y * float(i));
			vec4 res = texture2D(bloomColor, samplePosition + offset);
			result += res;
		}
   } 
 //  vec4 pixelColor = texture2D(gColor, samplePosition);
   color = pixelColor + (result/(blurSize*blurSize));
}