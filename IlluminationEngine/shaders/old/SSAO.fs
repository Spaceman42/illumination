#version 330
uniform sampler2D gDepth;
uniform sampler2D uRandom;
uniform sampler2D gColor;
uniform sampler2D gNormal; //temp, contains ambient term (how convenient)
uniform int samples;
uniform vec2 buffersize;
uniform vec4 frustrumData;

out vec4 color;

float occlude(vec2 offset, float depth, float deltaD);
float getDepth(vec2 uv);
vec3 positionFromDepth(float depth);
const vec3[16] kernel = vec3[16](
	vec3(0.75196034, -0.0018082703, -0.16099077),
	vec3(-0.42991608, 0.79414004, 0.31120867),
	vec3(-0.43083864, -0.08252132, -0.21659595),
	vec3(0.39661628, 0.72799563, -0.1377703),
	vec3(0.82530206, -0.21528216, 0.4731983),
	vec3(-0.74861884, 0.36201343, -0.6615516),
	vec3(0.984415, -0.89390814, -0.65319216),
	vec3(-0.9338356, -0.57709706, -0.030132106),
	vec3(0.16010784, -0.69638604, 0.7576938),
	vec3(-0.4930113, -0.71105695, -0.66575897),
	vec3(-0.24943624, 0.3235983, -0.58927405),
	vec3(0.5267412, -0.5825002, -0.35004857),
	vec3(-0.54607975, -0.10894632, 0.6222402),
	vec3(8.183538E-4, -0.54768693, 0.5887389),
	vec3(-0.8921414, 0.22206913, 0.566243),
	vec3(0.38095742, 0.23525165, -0.56502813)
);

void main() {
/*	vec2 vectors[12] = vec2[12]( 
		vec2(0.9, -0.1),
		vec2(-0.1, 0.9),
		vec2(1.0, 1.0),
		vec2(0.67, -0.1),
		vec2(0.1, 2),
		vec2(0.867, 0.4542),
		vec2(5, 0.57657),
		vec2(4, 0.45765),
		vec2(6, 0.12404),
		vec2(0.4567, 0.1245),
		vec2(0.1245, -.693),
		vec2(-0.1244, -0.4256),
	); */
/*	float floats[17] = float[17](-1.613839, 0.779439, -1.773267, 0.882721, -0.878087, 1.059520, -1.236240, 1.475564, -0.400269, 0.319108, .963, .981, 1.234, 1.773267
	-1.236240, 0.779439, 0.059520, -1.613839);
	float floats2[17] = float[17](-0.6252789, 0.71809727, 0.23305269, -0.65771735, -0.18283108, -0.70699584, 0.009405001, 0.5211781, 
		0.6575828, -0.96378195, -0.24983051, 0.85664123, 0.77949995, -0.4125111, 0.49936762, -0.57254404, 0.7974265); */
	vec2 samplePosition;
	samplePosition.x = (2*gl_FragCoord.x +1)/(2*buffersize.x);
	samplePosition.y = (2*gl_FragCoord.y +1)/(2*buffersize.y);
	vec2 rVec = normalize(texture2D(uRandom, buffersize * samplePosition/64).xy * 2.0 - 1.0);
	vec3 sampleOrigin = positionFromDepth(texture2D(gDepth,samplePosition));
	float occlusion = 0;
	float radius = 0.5/sampleOrigin.z;
	for (int i = 0; i<samples; i++) {
		vec2 offset = reflect(vec2(kernel[i].xy*radius)/(buffersize/4), rVec);
		float randSampleDepth = getDepth(samplePosition+offset);
		if (randSampleDepth < sampleOrigin.z) {
			occlusion += occlude(offset, randSampleDepth, randSampleDepth-sampleOrigin.z);
		}
	}
	
	float fSamples = float(samples);
//	vec4 pixelColor = texture2D(gColor, samplePosition);
	occlusion = occlusion/fSamples; 
	float abientTerm = texture2D(gNormal, samplePosition).w;
	color = vec4(1-occlusion);
}

float occlude(vec2 offset, float depth, float deltaD) {
	float factor = 0;
	if (deltaD < 0.3) {
		factor = .5;
	}
	return factor;
}
vec3 positionFromDepth(float depth) {
	float far = frustrumData.y;
	float near = frustrumData.x;
	float right = frustrumData.z;
	float top = frustrumData.w;
	vec2 ndc;           
	vec3 eye;             
	eye.z = (2.0 * near) / (far + near - depth * (far - near));//(near * far / ((depth * (far - near)) - far));
	ndc.x = ((gl_FragCoord.x/buffersize.x) - 0.5) * 2.0; 
	ndc.y = ((gl_FragCoord.y/buffersize.y) - 0.5) * 2.0;
	eye.x = (-ndc.x * eye.z) * right/near;
	eye.y = (-ndc.y * eye.z) * top/near;
	
	return eye;
}

float getDepth(vec2 uv) {
	float z = texture2D(gDepth, uv).x;
	float n = 1.0; //near plane
	float f = 100.0; //far plane;
	return (2.0 * n) / (f + n - z * (f - n));
}