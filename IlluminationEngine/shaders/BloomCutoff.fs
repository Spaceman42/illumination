#version 330

uniform sampler2D gColor;


uniform float cutoff;

in vec2 samplePosition;

out vec4 color;
float computeLuminance(vec4 inColor);
void main() {
	vec3 pixelColor = texture2D(gColor, samplePosition).xyz;
	
	vec4 bloomColor = vec4(0,0,0,1);
	
	if (computeLuminance(vec4(pixelColor,1)) > cutoff) {
		bloomColor = vec4(pixelColor-cutoff,1);
	}
	
	color = vec4(bloomColor);
	
}

float computeLuminance(vec4 inColor) {
	return (0.27*inColor.r) + (0.67 * inColor.g) + (0.06 * inColor.b);
}