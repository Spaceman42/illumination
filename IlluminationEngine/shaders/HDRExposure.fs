#version 330

in vec2 samplePosition;

uniform sampler2D luminance;
uniform sampler2D luminanceOld;
uniform sampler2D inColor;

uniform int frame;
uniform int maxFrame;

out vec4 color;

vec4 getLDR(float luminance, vec4 color);
vec4 getLDR2(float luminance, vec4 color);
vec4 toneMap(vec4 color);
vec4 toneMap(vec4 color, float lum);
float computeLuminance(vec4 color);
float interpolate(float a, float b, int current, int upper);
void main() {
	vec4 pixelColor = texture2D(inColor, samplePosition);
	float l = texture2D(luminance, samplePosition).x;
	float lOld = texture2D(luminanceOld, samplePosition).x;
	float luminance = interpolate(lOld,l , frame, maxFrame);
	color = getLDR(luminance, pixelColor);//vec4(texture2D(luminance, samplePosition).x);//toneMap(pixelColor);//getLDR(texture2D(luminance, samplePosition).x, texture2D(inColor, samplePosition));
	if (samplePosition.x > .75 && samplePosition.y > 0.75) {
		color = vec4(l-lOld);
	}
}

float interpolate(float a, float b, int current, int upper) {
	float v = (float(upper) - float(current))/float(upper);
	float k = (v-1)*-1;
	return ((a*v)+(b*k))/2;
}

vec4 getLDR(float luminance, vec4 color) {
	float exposureMod = 1;
	float expo =exposureMod/(luminance+1);
	return (vec4(1) - exp2(-expo * color));
}

vec4 getLDR2(float luminance, vec4 color) {
	float expo =  exp2(luminance);
	expo*= 1.5;
	return toneMap(color*expo)/toneMap(vec4(1));
}

float computeLuminance(vec4 color) {
	return length(color);//0.27*color.r) + (0.67 * color.g) + (0.06 * color.b);
}

vec4 toneMap(vec4 color, float lum) {
	return vec4(0);
}

vec4 toneMap(vec4 color) {
    float A = 0.15;
	float B = 0.50;
	float C = 0.10;
	float D = 0.20;
	float E = 0.02;
	float F = 0.30;

    return (color)/(color+1);//((color*(A*color+C*B)+D*E)/(color*(A*color+B)+D*F))-E/F;
}