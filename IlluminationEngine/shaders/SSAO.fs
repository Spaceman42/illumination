#version 330
uniform sampler2D gDepth;
uniform sampler2D gNormal;
uniform sampler2D uRandom;

uniform int samples;
uniform vec2 buffersize;
uniform vec2 noiseScale;
uniform vec4 frustrumData;
uniform mat4 invPMat;

uniform float uRadius; //radius of occludies considered
uniform float scale; //rate of attenuation for occlusion
uniform float bias; //determine cone of considered occludies
uniform float intensity; //intensity of occlusion.

in vec2 samplePosition;
out vec4 color;

const vec2 kernel[4] = vec2[4](
	vec2(1,0),
	vec2(-1,0),
	vec2(0,1),
	vec2(0,-1)
);

float occlude(vec2 uv, vec2 offsetUV, vec3 origin, vec3 normal);
vec3 positionFromDepth(float depth);
vec3 getPosition(vec2 uv, float depth);
float getDepth(vec2 uv);
vec2 getRandom(vec2 uv);
bool getInScreen(vec2 location);

void main() {
	
	vec3 origin = positionFromDepth(texture2D(gDepth, samplePosition));
	vec3 normal = (texture2D(gNormal, samplePosition).xyz);
	normal = normalize(normal*2-1);

	vec2 random = getRandom(samplePosition);
	float minusO = -origin.z;
	float radius = uRadius/minusO;
	float occlusion = 0.0;
	int iterations = samples/4;
	
	//if (radius > 0.004) {
	for (int i = 0; i<iterations; i++) {
		vec2 coord1 = reflect(kernel[i], random) * radius;
		vec2 coord2 = vec2(coord1.x*0.707 - coord1.y*0.707, coord1.x*0.707 + coord1.y*0.707);
		occlusion += occlude(samplePosition, coord1 * 0.25, origin, normal);
		occlusion += occlude(samplePosition, coord2 * 0.50, origin, normal);
		occlusion += occlude(samplePosition, coord1 * 0.75, origin, normal);
		occlusion += occlude(samplePosition, coord2, origin, normal);
	}
//	}
	occlusion /= iterations;
	if (-origin.z == frustrumData.y) {
		occlusion = 1;
	}
	
	color = vec4(occlusion,1,1, -origin.z/frustrumData.y);
	
	
}



vec2 getRandom(vec2 uv) {
	return normalize(texture2D(uRandom, buffersize * uv/noiseScale).xy * 2.0 - 1.0);
}

bool getInScreen(vec2 location) {
	return location.x < 1 || location.y < 1 || location.x > 0 || location.y > 0;
}

float occlude(vec2 uv, vec2 offsetUV, vec3 origin, vec3 normal) {
	vec2 pos = vec2 (clamp(uv.x + offsetUV.x, 0, 1), clamp(uv.y + offsetUV.y, 0, 1));
	vec3 diff = getPosition(pos, texture2D(gDepth, pos))-origin;
	vec3 vec = normalize(diff);
	float dist = length(diff) * scale/3;
	float value = dot(vec,normal)+bias;
	return max(0.0,value)*(1.0/(1.0+dist))*intensity;
}

float getDepth(vec2 uv) {
	float z = texture2D(gDepth, uv).x;
	float n = frustrumData.x; //near plane
	float f = frustrumData.y; //far plane;
	return (2.0 * n) / (f + n - z * (f - n));
}

vec3 positionFromDepth(float depth) {
	float near = frustrumData.x;
	float far = frustrumData.y;
	float right = frustrumData.z;
	float top = frustrumData.w;
	vec2 ndc;           
	vec3 eye;             
	eye.z = (near * far / ((depth * (far - near)) - far));//(2.0 * near) / (far + near - depth * (far - near));
	ndc.x = ((gl_FragCoord.x/buffersize.x) - 0.5) * 2.0; 
	ndc.y = ((gl_FragCoord.y/buffersize.y) - 0.5) * 2.0;
	eye.x = (-ndc.x * eye.z) * right/near;
	eye.y = (-ndc.y * eye.z) * top/near;
	
	return eye;
}

vec3 getPosition(vec2 uv, float depth) {
	float near = frustrumData.x;
	float far = frustrumData.y;
	float right = frustrumData.z;
	float top = frustrumData.w;
	vec2 ndc;           
	vec3 eye;             
	eye.z = (near * far / ((depth * (far - near)) - far));
	ndc.x = ((uv.x) - 0.5) * 2.0; 
	ndc.y = ((uv.y) - 0.5) * 2.0;
	eye.x = (-ndc.x * eye.z) * right/near;
	eye.y = (-ndc.y * eye.z) * top/near;
	
	return eye;
}
