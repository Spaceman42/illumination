#version 330

uniform sampler2D gDepth;
uniform sampler2D uShadow;
uniform sampler2D gNormal;
uniform sampler2D uColor;
uniform sampler2D gColor;

uniform mat4 lMat;
uniform mat4 invVMat;
uniform mat4 vMat;
uniform float uBias;
uniform vec3 lightLocation;
uniform vec3 lightColor;
uniform vec4 frustrumData;
uniform float lightIntensity;


in vec2 uv;

out vec4 color;

vec3 getPosition(vec2 uv, float depth);


void main() {
	vec4 vertexC = vec4(getPosition(uv, texture2D(gDepth, uv)),1);
	mat4 invMat = inverse(vMat); 
	vec4 vertex = invMat * vertexC;
	vec4 vertexS = lMat * vertex;
	vec4 vertexT = vMat * vertex;
	
	vec3 normal = texture2D(gNormal, uv).xyz*2-1;
	normal = normalize(normal);
	
	vec3 location = vertexC.xyz;
	
	
	vec3 lPosition = (vMat * vec4(lightLocation,1)).xyz;//(vMat * vec4(0,0,8,1)).xyz;//
	vec3 lDirection =(lPosition - location);
	float lDist = length(lDirection);
	lDirection = normalize(lDirection);
	
	vec3 accLight = vec3(0);
	float diffuse = max(0.0, dot(normal, lDirection)) ;
	
	float spec = 0.0;
	if (diffuse != 0) {
		vec3 reflectionDirection = normalize(reflect(-lDirection, normal));
		float specular = max(0.0, dot(reflectionDirection,  -normalize(location)));
		float fspecular = pow(specular, 64);
		spec = fspecular;
	}
	float shadowDepth = texture2D(uShadow, vertexS.xy).x;
	float isShadowed = 1;
	if (shadowDepth <= vertexS.z - 0.01) {
		diffuse = diffuse *.95;
		spec = 0;
		isShadowed = 0;
	}
	accLight = vec3(diffuse);
	
	
	vec4 gcolor = texture2D(gColor, uv);
	accLight += (spec*gcolor.w);
	accLight *= gcolor.xyz;
	if (isShadowed == 1) {
		accLight *= lightIntensity;
	}
	accLight += texture2D(uColor, uv).xyz;


	color = vec4(accLight,1);
}

vec3 getPosition(vec2 uv, float depth) {
	float near = frustrumData.x;
	float far = frustrumData.y;
	float right = frustrumData.z;
	float top = frustrumData.w;
	vec2 ndc;           
	vec3 eye;             
	eye.z = (near * far / ((depth * (far - near)) - far));
	ndc.x = ((uv.x) - 0.5) * 2.0; 
	ndc.y = ((uv.y) - 0.5) * 2.0;
	eye.x = (-ndc.x * eye.z) * right/near;
	eye.y = (-ndc.y * eye.z) * top/near;	
	return eye;
}