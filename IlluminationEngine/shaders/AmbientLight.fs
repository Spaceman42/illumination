#version 330

uniform sampler2D gColor;
uniform sampler2D gNormal;
uniform sampler2D inColor;

in vec2 samplePosition;

out vec4 color;


void main() {
	float ambientTerm = texture2D(gNormal, samplePosition).w;
	vec4 gbufferColor = vec4(texture2D(gColor, samplePosition).xyz, 1);
	vec4 pixelColor = vec4(texture2D(inColor, samplePosition).xyz,1);
	vec4 ambientLight = gbufferColor * ambientTerm;
	
	color = pixelColor + ambientLight;
}