#version 330
in vec4 vertex;
uniform mat4 mMat;
out vec2 samplePosition;
void main() {
	samplePosition = ((vertex * 0.5) + 0.5).xy;
	gl_Position = mMat*vertex;
}