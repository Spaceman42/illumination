#version 330

in vec2 samplePosition;

uniform sampler2D inColor;

out float luminance;

float computeLuminance(vec4 color);

void main() {
	vec4 color = texture2D(inColor, samplePosition);
	luminance = computeLuminance(color);
}
float computeLuminance(vec4 color) {
	return (0.27*color.r) + (0.67 * color.g) + (0.06 * color.b);
}