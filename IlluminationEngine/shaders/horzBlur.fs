#version 330

uniform sampler2D uColor;

uniform int blurSize;
uniform vec2 buffersize;

in vec2 samplePosition;

out vec4 color;

void main() {
	vec4 results = vec4(0);
	vec2 texelSize = 1/buffersize;
	for (int i = -blurSize/2; i<blurSize/2; i++) {
		results += texture2D(uColor, samplePosition+vec2(texelSize.x*i, 0));
	}
	color = results/blurSize;
}