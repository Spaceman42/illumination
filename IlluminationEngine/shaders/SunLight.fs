#version 330

uniform sampler2D gColor;
uniform sampler2D gNormal;
uniform sampler2D gDepth;

uniform mat4 vMat;

uniform vec4 lightColor;

uniform vec4 frustrumData;
uniform vec2 buffersize; //screen resolution

in float lightIntensity;
uniform vec3 lightLocation;
in vec2 samplePosition;
layout (location = 0) out vec4 pixelColor;

vec3 positionFromDepth(float depth);

void main() {
	vec4 color0data = texture2D(gColor, samplePosition.xy);
	vec4 color1data = texture2D(gNormal, samplePosition.xy);  //TODO: Get normals as a two component vector to save fillrate
	float depth = texture2D(gDepth, samplePosition.xy).x;
	
	vec3 normal = normalize((color1data.xyz*2)-1);
	//normal.y *= -1;
	
	vec3 color = color0data.xyz;
	float specularIntensity = color0data.w;
	
	vec3 location = positionFromDepth(depth);
	vec3 lPosition = (vMat * vec4(lightLocation,1)).xyz;//(vMat * vec4(0,0,8,1)).xyz;//
	vec3 lDirection =(lPosition - location);
	
	lDirection = normalize(lDirection);
	
	vec3 accLight = vec3(0);
	float diffuse = max(dot(normal, lDirection), 0) ;
	accLight += vec3(diffuse, diffuse, diffuse);
	if (diffuse != 0) {
		vec3 reflectionDirection = normalize(reflect(-lDirection, normal));
		float specular = max(0.0, dot(reflectionDirection,  -normalize(location)));
		float fspecular = pow(specular, 64);
		accLight += fspecular*specularIntensity;
	}
	accLight *= lightColor.rgb;
	
	accLight*=color;
	accLight*=lightColor.a;
	
	//if (accLight == 0) {
	//	accLight = vec3(1);
	//}
	pixelColor = vec4(accLight, 1);
}

vec3 positionFromDepth(float depth) {
	float near = frustrumData.x;
	float far = frustrumData.y;
	float right = frustrumData.z;
	float top = frustrumData.w;
	vec2 ndc;           
	vec3 eye;             
	eye.z = near * far / ((depth * (far - near)) - far);
	ndc.x = ((gl_FragCoord.x/buffersize.x) - 0.5) * 2.0; 
	ndc.y = ((gl_FragCoord.y/buffersize.y) - 0.5) * 2.0;
	eye.x = (-ndc.x * eye.z) * right/near;
	eye.y = (-ndc.y * eye.z) * top/near;
	return eye;
}