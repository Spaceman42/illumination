#version 330

in vec4 vertex;

uniform vec3 location;
uniform float intensity;
uniform mat4 lMat;
uniform mat4 pMat;

out float lightIntensity;
out vec3 lightLocation;
out vec2 texCoord;
void main() {
	lightIntensity = intensity;
	lightLocation = location;
	texCoord = ((vertex+1)/2).xy;
	vec4 pos = lMat *  vertex;
	//pos.z += intensity * 1.8;
	gl_Position = pos;
	
}