#version 330

in vec4 vertex;

out vec2 uv;

void main() {
	uv = (vertex*0.5+0.5).xy;
	gl_Position = vertex;
}