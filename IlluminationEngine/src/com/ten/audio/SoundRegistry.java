package com.ten.audio;

import java.util.HashSet;

public class SoundRegistry {
	private SoundRegistry() {};
	
	private static HashSet<Sound> sounds = new HashSet<Sound>();
	
	protected void addSound(Sound sound) {
		sounds.add(sound);
	}
	
	protected void removeSound(Sound sound) {
		if (containsSound(sound)) {
			sounds.remove(sounds);
		}
	}
	
	protected boolean containsSound(Sound sound) {
		return sounds.contains(sound);
	}
}
