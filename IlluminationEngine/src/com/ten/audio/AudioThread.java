package com.ten.audio;

import java.io.FileNotFoundException;
import java.util.Queue;

import org.apache.log4j.Logger;
import org.lwjgl.LWJGLException;

import com.ten.request.AudioRequest;

public class AudioThread extends Thread {
	private static final Logger logger = Logger.getLogger(AudioThread.class.getName());
	
	private AudioEngine engine;
	
	public AudioThread() {
		super("Audio-Thread");	
		engine = new AudioEngine(256);
	}
	
	@Override
	public void run() {
		try {
			engine.init();
			engine.run();
		} catch (Exception e) {
			e.printStackTrace();
			logger.fatal(e);
			System.exit(-1);
		}
	}
	
	public void makeAudioRequest(AudioRequest request) {
		engine.addRequest(request);
	}
	
}
