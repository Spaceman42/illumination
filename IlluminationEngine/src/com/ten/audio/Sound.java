package com.ten.audio;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.IntBuffer;
import java.util.logging.Logger;

import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL10;
import org.lwjgl.util.WaveData;

import com.ten.math.Vector4;
import com.ten.request.AudioLoadRequest;

public class Sound {
	private Vector4 location;
	private int sound = -1;
	private static final Logger logger = Logger.getLogger(Logger.class.getName());
	
	public Sound(String location) {
		new AudioLoadRequest(this, location).make();
	}
	
	public void setLocation(Vector4 location) {
		this.location = location;
	}
	
	public int getBuffer() {
		return sound;	
	}

	public void setBuffer(int sound) {
		this.sound = sound;
	}
}
