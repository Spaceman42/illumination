package com.ten.audio;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.openal.AL;
import org.lwjgl.openal.AL10;
import org.lwjgl.util.WaveData;

import static org.lwjgl.openal.AL10.*;

import com.ten.AppState;
import com.ten.game.DebugCamera;
import com.ten.request.AudioRequest;

public class AudioEngine {
	private static final Logger logger = Logger.getLogger(AudioEngine.class.getName());
	private BlockingQueue<AudioRequest> requests;
	
	private IntBuffer scratchBuffer;
	private int[] sources;
	private int[] buffers;
	private int bufferIndex = 0;
	private int sourceIndex = 0;
	private int channels;
	
	public AudioEngine(int channels) {
		requests = new LinkedBlockingQueue<AudioRequest>();
		
		scratchBuffer = BufferUtils.createIntBuffer(256);
		sources = new int[channels];
		buffers = new int[256];
		this.channels = channels;
		logger.info("Audio engine created with " + channels + " channels");
	}

	public void init() throws LWJGLException {
		AL.create();
		
		scratchBuffer.limit(channels);
		alGenSources(scratchBuffer);
		scratchBuffer.rewind();
		scratchBuffer.get(sources);
		logger.info("OpenAL inited");
		setUpListener();
		
		
		checkALerror(logger, "OpenAL failed to init");
		
	}
	
	private void setUpListener() {
		FloatBuffer  buffer = BufferUtils.createFloatBuffer(3).put(new float[] {0.0f, 0.0f, 0.0f});
		buffer.flip();
		alListener(AL_POSITION, buffer);
	}
	
	public void run() throws InterruptedException {
		while(true) {
			AudioRequest r = requests.take();
			logger.info("proccesing audio request");
			r.doRequest(this);
		}
	}
	
	public int addSound(String location) throws FileNotFoundException {
		scratchBuffer.rewind().position(0).limit(1);
		alGenBuffers(scratchBuffer);
		buffers[bufferIndex] = scratchBuffer.get(0);
		
		WaveData waveData = WaveData.create(new BufferedInputStream(new FileInputStream(new File(location))));
		
		alBufferData(buffers[bufferIndex], waveData.format, waveData.data, waveData.samplerate);
		
		waveData.dispose();
		logger.info("Sound added to buffer position " + bufferIndex);
		return bufferIndex++;
	}
	
	public void stopSound(Sound sound) {
		int buffer = sound.getBuffer();
	}
	
	public void startSound(Sound sound) {
		int buffer = sound.getBuffer();
		int channel = sources[(sourceIndex++ % (sources.length-1))];
		alSourcei(channel, AL_BUFFER, buffers[buffer]);
		alSourcePlay(channel);
		checkALerror(logger, "starting sound");
		if (alGetBoolean(AL_PLAYING)) {
			logger.info("OpenAL playing sound");
		} else {
			logger.info("OpenAL not playing sound");
		}
	}
		
	public void addRequest(AudioRequest request) {
		requests.add(request);
	}
	
	public static boolean checkALerror(Logger logger, String msg) {
		boolean error = alGetError() == AL_NO_ERROR;
		if (error) {
			logger.error("OpenAL error: " + msg);
			System.err.println("OpenAL error: " + msg + " " + alGetString(alGetError()));
		}
		return error;
	}

}
