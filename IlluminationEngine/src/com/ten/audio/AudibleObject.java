package com.ten.audio;

public interface AudibleObject {
	public Sound getSound();
}
