package com.ten.request;

import java.io.IOException;

import com.ten.graphics.renderer.model.Mesh;
import com.ten.graphics.renderer.model.Model;
import com.ten.graphics.renderer.model.Model2;
import com.ten.graphics.renderer.model.ModelMesh;
import com.ten.graphics.renderer.model.Texture;
import com.ten.physics.shape.BoundingShape;
import com.ten.physics.shape.ShapeConstructionInfo;

public class IOModelLoadRequest extends IORequest{
	String[] meshLocations;
	String[] textureLocations;
	ShapeConstructionInfo shapeInfo;
	Model2 model;
	
	public IOModelLoadRequest(String[] meshLocations, String[] textureLocations, ShapeConstructionInfo shapeInfo, Model2 model) {
		model = model;
		this.textureLocations = textureLocations;
		this.meshLocations = meshLocations;
		this.shapeInfo = shapeInfo;
	}
	
	@Override
	public void doRequest() throws IOException {
		Texture[] textures = new Texture[textureLocations.length];
		ModelMesh[] meshs = new ModelMesh[meshLocations.length];
		BoundingShape shape = null;
		for (int i = 0; i<textureLocations.length; i++) {
			textures[i] = Texture.loadTexture(textureLocations[i]);
		}
		//Load all meshs, use lowest LOD for BoundingShape. Will be configerable later 
		for (int i = 0; i<meshLocations.length; i++) {
			if (i == meshLocations.length-1) {
				meshs[i] = ModelMesh.loadBinary(meshLocations[i], shapeInfo);
				shape = meshs[i].getShape();
			} else {
				meshs[i] = ModelMesh.loadBinary(meshLocations[i], null);
			}
		}
	}
}
