package com.ten.request;

import java.io.IOException;

import com.ten.ThreadManager;





public abstract class IORequest implements Request {
	public abstract void doRequest() throws IOException;
	
	@Override
	public void make() {
		ThreadManager.getInstance().makeIoRequest(this);
	}
}
