package com.ten.request;

import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.apache.log4j.Logger;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.GL11;

import com.ten.graphics.GraphicsEngine;
import com.ten.graphics.renderer.model.Mesh2;
import com.ten.graphics.renderer.model.ModelMesh;

public class GLMesh2LoadRequest extends GLRequest{
	private static final Logger logger = Logger
			.getLogger(GLModelMeshLoadRequest.class.getName());

	private Mesh2 mesh;
	private float[] vertices;
	private float[] textureCoords;
	private float[] normals;
	private int[] indices;
	private float[] bitangents;
	private float[] tangents;
	private int indicesAmount;

	public GLMesh2LoadRequest(Mesh2 mesh, float[] vertices,
			float[] textureCoords, float[] normals, int[] indices, float[] tangents, float[] bitangents) {
		this.mesh = mesh;
		this.vertices = vertices;
		this.textureCoords = textureCoords;
		this.normals = normals;
		this.indices = indices;
		this.bitangents = bitangents;
		this.tangents = tangents;
	}

	@Override
	public boolean doRequest() throws LWJGLException {
		// TODO: Maybe do this in mesh, gives less control but would reduce the
		// amount of synchronization required
		int vao = glGenVertexArrays();
		int verticesID = createVertexBuffer(vao, vertices);
		int normalsID = createNormalBuffer(vao, normals);
		int texCoordsID = -1;
		int tansID = -1;
		int bitansID = -1;
		if (textureCoords != null) {
			System.err.println("Creating vbo");
		//	System.exit(0);
			texCoordsID = createUVBuffer(vao, textureCoords);
			tansID = createTangentBuffer(vao, tangents);
			bitansID = createBitangentBuffer(vao, bitangents);
		}
		
		int indicesID = createIndicesBuffer(indices);
		
		logger.info("Tangents ID: " + tansID + " Bitangents ID: " + bitansID);
		
		
		mesh.update(vao, indicesID, verticesID, texCoordsID, normalsID, tansID, bitansID,
				indicesAmount);
		return true;
	}

	private int createIndicesBuffer(int[] indices) {
		indicesAmount = indices.length;
		IntBuffer buffer = BufferUtils.createIntBuffer(indicesAmount);
		buffer.put(indices);
		buffer.flip();
		int id = glGenBuffers();
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		return id;
	}

	private int createVertexBuffer(int vaoID, float[] vertices) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(vertices.length);
		buffer.put(vertices);
		buffer.flip();
		glBindVertexArray(vaoID);
		int vboID = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, vboID);
		glBufferData(GL_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 4, GL11.GL_FLOAT, false, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		buffer = null;
		GraphicsEngine.checkGLError(logger);
		return vboID;

	}

	private int createNormalBuffer(int vaoID, float[] normals) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(normals.length);
		buffer.put(normals);
		buffer.flip();
		glBindVertexArray(vaoID);
		int nboID = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, nboID);
		glBufferData(GL_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
		glVertexAttribPointer(1, 4, GL11.GL_FLOAT, false, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		buffer = null;
		return nboID;
	}

	private int createUVBuffer(int vaoID, float[] textureCoords) {
		FloatBuffer buffer = BufferUtils
				.createFloatBuffer(textureCoords.length);
		buffer.put(textureCoords);
		buffer.flip();
		glBindVertexArray(vaoID);
		int uvboID = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, uvboID);
		glBufferData(GL_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
		glVertexAttribPointer(2, 2, GL11.GL_FLOAT, false, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		buffer = null;
		return uvboID;
	}
	
	private int createTangentBuffer(int vaoID, float[] tangents) {
		logger.info("Size of tangents: " + tangents.length);
		FloatBuffer buffer = BufferUtils
				.createFloatBuffer(tangents.length);
		buffer.put(tangents);
		buffer.flip();
		glBindVertexArray(vaoID);
		int tanID = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, tanID);
		glBufferData(GL_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
		glVertexAttribPointer(3, 4, GL11.GL_FLOAT, false, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		buffer = null;
		return tanID;
	}
	
	private int createBitangentBuffer(int vaoID, float[] bitangents) {
		logger.info("Size of bitangents: " + bitangents.length);
		FloatBuffer buffer = BufferUtils
				.createFloatBuffer(bitangents.length);
		buffer.put(bitangents);
		buffer.flip();
		glBindVertexArray(vaoID);
		int bitanID = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, bitanID);
		glBufferData(GL_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
		glVertexAttribPointer(4, 4, GL11.GL_FLOAT, false, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		buffer = null;
		return bitanID;
	}
}
