package com.ten.request;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ten.ThreadManager;

import com.ten.asset.AssetMesh;
import com.ten.asset.ModelLoader;
import com.ten.asset.binaryasset.AssetFile;
import com.ten.asset.binaryasset.Chunk;
import com.ten.asset.binaryasset.ModelChunk;
import com.ten.graphics.renderer.model.Mesh;
import com.ten.physics.shape.BoundingShape;
import com.ten.physics.shape.ShapeConstructionInfo;

public class IOBinaryMeshLoadRequest extends IORequest {
	private String location;
	private Mesh mesh;
	private ShapeConstructionInfo shapeInfo;
	private static final Logger logger = Logger
			.getLogger(IOBinaryMeshLoadRequest.class.getName());

	public IOBinaryMeshLoadRequest(String meshLocation, Mesh mesh, ShapeConstructionInfo shapeInfo) {
		this.shapeInfo = shapeInfo;
		location = meshLocation;
		this.mesh = mesh;
	}

	public void doRequest() throws IOException {
		try {
			AssetFile file = AssetFile.load(new File(location + ".ilm_a"));
			for (Chunk c : file.getChunks()) {
				logger.info(c);
			}
			ModelChunk chunk = (ModelChunk) file.findChunk("mesh");
			logger.info(chunk);

			logger.info("Everything loaded fine");
			//System.exit(0);
			
			BoundingShape shape = shapeInfo.constructShape(chunk.getVertices(), chunk.getIndices());
			
			new GLMeshLoadRequest(mesh, chunk.getVertices(), chunk.getTextureCoordinates(), chunk.getNormals(), chunk.getIndices(), shape).make();
		} catch (Exception e) {
			e.printStackTrace();
			logger.fatal(e);
			System.exit(-1);
		}
	}
}
