package com.ten.request;

import org.lwjgl.LWJGLException;

import com.ten.ThreadManager;

public abstract class GLRequest implements Request {
	
	public abstract boolean doRequest() throws LWJGLException;
	@Override
	public void make() {
		ThreadManager.getInstance().makeGLRequest(this);
	}
}
