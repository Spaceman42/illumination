package com.ten.request;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.*;

import java.nio.ByteBuffer;

import org.apache.log4j.Logger;
import org.lwjgl.LWJGLException;

import com.ten.graphics.renderer.model.Texture;

public class GLTextureLoadRequest extends GLRequest {
	private Texture texture;
	private ByteBuffer data;
	private int width;
	private int height;
	private static Logger logger = Logger.getLogger(GLTextureDeleteRequest.class);
	public GLTextureLoadRequest(Texture texture, ByteBuffer data, int width, int height) {
		this.texture = texture;
		this.data = data;
		this.width = width;
		this.height = height;
	}

	@Override
	public boolean doRequest() throws LWJGLException {
		logger.debug("Doing request");
		int id = glGenTextures();
        glBindTexture(GL_TEXTURE_2D, id);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
        texture.update(id, width, height, data);
        return true;
	}

}
