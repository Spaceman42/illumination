package com.ten.request;

import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.apache.log4j.Logger;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.GL11;

import com.ten.Utils;
import com.ten.graphics.GraphicsEngine;
import com.ten.graphics.renderer.model.Mesh;
import com.ten.physics.shape.BoundingBox;
import com.ten.physics.shape.BoundingMesh;
import com.ten.physics.shape.BoundingShape;
import com.ten.physics.shape.ConvexBoundingShape;

public class GLMeshLoadRequest extends GLRequest {

	private static final Logger logger = Logger
			.getLogger(GLMeshLoadRequest.class.getName());

	private Mesh mesh;
	private float[] vertices;
	private float[] textureCoords;
	private float[] normals;
	private int[] indices;
	private int indicesAmount;
	private BoundingShape shape;

	public GLMeshLoadRequest(Mesh mesh, float[] vertices,
			float[] textureCoords, float[] normals, int[] indices, BoundingShape shape) {
		this.mesh = mesh;
		this.vertices = vertices;
		this.textureCoords = textureCoords;
		this.normals = normals;
		this.indices = indices;
		this.shape = shape;
	}

	@Override
	public boolean doRequest() throws LWJGLException {
		// TODO: Maybe do this in mesh, gives less control but would reduce the
		// amount of synchronization required
		int vao = glGenVertexArrays();
		int verticesID = createVertexBuffer(vao, vertices);
		int normalsID = createNormalBuffer(vao, normals);
		int texCoordsID = -1;
		if (textureCoords != null) {
			System.err.println("Creating vbo");
		//	System.exit(0);
			texCoordsID = createUVBuffer(vao, textureCoords);
		}
		
		int indicesID = createIndicesBuffer(indices);
		
		
		
		
		mesh.update(vao, indicesID, verticesID, texCoordsID, normalsID,
				indicesAmount, shape);
		return true;
	}

	private int createIndicesBuffer(int[] indices) {
		indicesAmount = indices.length;
		IntBuffer buffer = BufferUtils.createIntBuffer(indicesAmount);
		buffer.put(indices);
		buffer.flip();
		int id = glGenBuffers();
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		return id;
	}

	private int createVertexBuffer(int vaoID, float[] vertices) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(vertices.length);
		buffer.put(vertices);
		buffer.flip();
		glBindVertexArray(vaoID);
		int vboID = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, vboID);
		glBufferData(GL_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 4, GL11.GL_FLOAT, false, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		buffer = null;
		GraphicsEngine.checkGLError(logger);
		return vboID;

	}

	private int createNormalBuffer(int vaoID, float[] normals) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(normals.length);
		buffer.put(normals);
		buffer.flip();
		glBindVertexArray(vaoID);
		int nboID = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, nboID);
		glBufferData(GL_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
		glVertexAttribPointer(1, 4, GL11.GL_FLOAT, false, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		buffer = null;
		return nboID;
	}

	private int createUVBuffer(int vaoID, float[] textureCoords) {
		FloatBuffer buffer = BufferUtils
				.createFloatBuffer(textureCoords.length);
		buffer.put(textureCoords);
		buffer.flip();
		glBindVertexArray(vaoID);
		int uvboID = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, uvboID);
		glBufferData(GL_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
		glVertexAttribPointer(2, 2, GL11.GL_FLOAT, false, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		buffer = null;
		return uvboID;
	}

}
