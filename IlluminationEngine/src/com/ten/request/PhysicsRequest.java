package com.ten.request;

import com.bulletphysics.dynamics.DynamicsWorld;
import com.ten.ThreadManager;

public abstract class PhysicsRequest implements Request{

	public abstract void doRequest(DynamicsWorld world);
	
	@Override
	public void make() {
		ThreadManager.getInstance().makePhysRequest(this);
	}
}
