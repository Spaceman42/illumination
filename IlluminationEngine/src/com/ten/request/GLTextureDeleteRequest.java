package com.ten.request;

import static org.lwjgl.opengl.GL11.glDeleteTextures;

import com.ten.graphics.renderer.model.Texture;

public class GLTextureDeleteRequest extends GLRequest {

	private int textureId;

	public GLTextureDeleteRequest(Texture texture) {
		textureId = texture.getTextureId();
	}

	@Override
	public boolean doRequest() {
		if (textureId != -1) {
			glDeleteTextures(textureId);
			return true;
		} else {
			return false;
		}
	}
}
