package com.ten.request;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.vecmath.Vector3f;

import org.apache.log4j.Logger;

import com.ten.Utils;
import com.ten.asset.AssetMesh;
import com.ten.asset.Material;
import com.ten.asset.MaterialFileLoader;
import com.ten.asset.ModelLoader;
import com.ten.asset.binaryasset.AssetFile;
import com.ten.asset.binaryasset.Chunk;
import com.ten.asset.binaryasset.ModelChunk;
import com.ten.graphics.renderer.model.CompoundModel;
import com.ten.graphics.renderer.model.Mesh2;
import com.ten.graphics.renderer.model.Model2;
import com.ten.math.Vector3;
import com.ten.physics.shape.BoundingBox;
import com.ten.physics.shape.BoundingShape;
import com.ten.physics.shape.CompoundBoundingShape;
import com.ten.physics.shape.ShapeConstructionInfo;

public class IOCompoundModelLoadRequst extends IORequest {
	private static final Logger logger = Logger
			.getLogger(IOCompoundModelLoadRequst.class.getName());

	private String ilmLocations;
	private String mtlLocation;
	private CompoundModel output;
	private ShapeConstructionInfo shapeInfo;
	private static final String SEP = "::";
	private float scale;

	/**
	 * 
	 * @param ilmLocation
	 * @param mtlLocation
	 * @param shapeInfo
	 *            defines the type of shapes that will be used in the
	 *            CompoundBoundingShape for this compound model
	 * @param output
	 */
	public IOCompoundModelLoadRequst(String ilmLocation, String mtlLocation,
			ShapeConstructionInfo shapeInfo, float localScale, CompoundModel output) {
		this.ilmLocations = ilmLocation;
		this.mtlLocation = mtlLocation;
		this.output = output;
		this.shapeInfo = shapeInfo;
		this.scale = localScale;
	}

	@Override
	public void doRequest() throws IOException {
		long startTime = Utils.getTime();
		try {

			logger.info("Loading mtl file");
			Map<String, Material> materials = MaterialFileLoader.loadMTLFile(
					new File(mtlLocation), "res/");

			AssetFile ilmFile = AssetFile.load(new File(ilmLocations));

			Chunk[] chunks = ilmFile.getChunks();
			Map<String, Mesh2> meshs = new HashMap<String, Mesh2>();
			List<ModelChunk> modelChunks = new ArrayList<ModelChunk>();
			Map<String, ModelChunk> test = new HashMap<String, ModelChunk>();
			logger.info(chunks.length);
			for (Chunk c : chunks) {
				if (c.getType() == Chunk.MODEL_CHUNK) {

					ModelChunk chunk = (ModelChunk) c;
					for (int i = 0; i<chunk.getVertices().length; i++) {
						chunk.getVertices()[i] = chunk.getVertices()[i]*scale;
					}
					modelChunks.add(chunk);
					Mesh2 m = new Mesh2(chunk.getId());
					logger.info("Chunk id: " + chunk.getId());
					meshs.put(chunk.getId(), m);
					logger.info("There are:"
							+ (float) chunk.getVertices().length / 4f
							+ " vertices");
					new GLMesh2LoadRequest(m, chunk.getVertices(),
							chunk.getTextureCoordinates(), chunk.getNormals(),
							chunk.getIndices(), chunk.getTangents(),
							chunk.getBitangents()).make();

				}
				// TODO: Handle lights
			}
			// Map<String, Mesh2> meshs = loadBinary(ilmLocations);
			logger.info("loaded " + meshs.values().size() + " meshs");
			Object[] obids = meshs.keySet().toArray();
			String[] ids = new String[obids.length];

			for (int i = 0; i < obids.length; i++) {
				ids[i] = (String) obids[i];
			}
			Model2[] models = new Model2[meshs.size()];
			for (int i = 0; i < meshs.size(); i++) {
				// if (i == 10) {
				String id = ids[i];
				Material mat = materials.get(getMaterialFromId(id));
				Mesh2 m = meshs.get(id);
				if (mat == null) {
					mat = new Material("default", null, null, null);
				}
				logger.info("Building model: " + id);
				models[i] = new Model2(m, mat);
				// }
			}
			BoundingShape shape = generateBoundingShape(modelChunks);
			output.update(models, shape);

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
		logger.info("Loaded model in "
				+ (Math.abs(Utils.getTime()) - startTime) + "milliseconds");
	}

	private String getMaterialFromId(String id) {
		String mat = null;
		String[] data = id.split(SEP);
		if (data.length > 1) {
			mat = data[1];
		}
		return mat;
	}

	private CompoundBoundingShape generateBoundingShape(List<ModelChunk> meshs) {
		BoundingShape[] shapes = new BoundingShape[meshs.size()];
		Vector3f[] locations = new Vector3f[meshs.size()];
		for (int i = 0; i < meshs.size(); i++) {
			shapes[i] = shapeInfo.constructShape(meshs.get(i).getVertices(),
					meshs.get(i).getIndices());
			locations[i] = new Vector3f(0, 0, 0);// CompoundBoundingShape.findCenter(Utils
			// .floatsToVector3s(meshs.get(i).getVertices(), 4));
		}
		return CompoundBoundingShape.generateShape(shapes, locations);
	}

	private Map<String, Mesh2> loadBinary(String ilmLocations) {
		AssetFile ilmFile = AssetFile.load(new File(ilmLocations));

		Chunk[] chunks = ilmFile.getChunks();
		Map<String, Mesh2> meshs = new HashMap<String, Mesh2>();
		// List<ModelChunk> modelChunks = new ArrayList<ModelChunk>();
		logger.info(chunks.length);
		for (Chunk c : chunks) {
			if (c.getType() == Chunk.MODEL_CHUNK) {
				ModelChunk chunk = (ModelChunk) c;
				Mesh2 m = new Mesh2(chunk.getId());
				logger.info("Chunk id: " + chunk.getId());
				meshs.put(chunk.getId(), m);
				logger.info("There are:" + (float) chunk.getVertices().length
						/ 4f + " vertices");
				new GLMesh2LoadRequest(m, chunk.getVertices(),
						chunk.getTextureCoordinates(), chunk.getNormals(),
						chunk.getIndices(), chunk.getTangents(),
						chunk.getBitangents()).make();
			}
			// TODO: Handle lights
		}
		return meshs;
	}

	private Map<String, Mesh2> loadObj(String ilmLocations) {
		Map<String, Mesh2> meshs = new HashMap<String, Mesh2>();
		Map<String, AssetMesh> as = ModelLoader
				.loadModel(new File(ilmLocations));

		Object[] obids = as.keySet().toArray();
		String[] ids = new String[obids.length];

		for (int i = 0; i < obids.length; i++) {
			ids[i] = (String) obids[i];
		}

		for (int i = 0; i < ids.length; i++) {
			float[] verts = as.get(ids[i]).getValue(AssetMesh.VERTICES);
			float[] norms = as.get(ids[i]).getValue(AssetMesh.NORMALS);
			float[] texs = as.get(ids[i]).getValue(AssetMesh.TEXTURES);
			int[] indices = as.get(ids[i]).getIndices();
			logger.info("there are " + verts.length / 4f + " vertices");
			Mesh2 m = new Mesh2(ids[i]);
			meshs.put(ids[i], m);
			new GLMesh2LoadRequest(m, verts, texs, norms, indices, new float[] {
					0, 0, 0, 0 }, new float[] { 0, 0, 0, 0 }).make();
		}
		return meshs;
	}

}
