package com.ten.request;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

import org.apache.log4j.Logger;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.CollisionWorld.ClosestRayResultCallback;
import com.bulletphysics.collision.dispatch.CollisionWorld.RayResultCallback;
import com.bulletphysics.dynamics.DynamicsWorld;
import com.ten.ThreadManager;
import com.ten.game.DebugCamera;
import com.ten.graphics.GraphicsEngine;
import com.ten.io.mouse.IOMouse;
import com.ten.io.mouse.MouseMetadata;
import com.ten.math.Matrix3;
import com.ten.math.Matrix4;
import com.ten.math.Vector3;
import com.ten.math.Vector4;
import com.ten.physics.PhysicalObject;

public class PhysicsPickRequest extends PhysicsRequest {
	private Vector3 rayTo;
	private DebugCamera camera;
	
	private int x;
	private int y;
	
	private static final Logger logger = Logger.getLogger(PhysicsPickRequest.class.getName());
	
	
	public PhysicsPickRequest(int mouseX, int mouseY, Vector4 worldPosition, DebugCamera camera) {
		x = mouseX;
		y = mouseY;
		
		this.camera = camera;
		Vector3 startRay = new Vector3(getFrom(mouseX, mouseY, camera));
		Vector3 endRay = new Vector3(getTo(mouseX, mouseY, camera));
		
		endRay.multiply(new Vector3(1,-1,1));
		startRay.multiply(new Vector3(1, -1, -1));
		
		logger.info("startRay: " + startRay);
		logger.info("endRay: " + endRay);
		
		Vector3 rayFrom = new Vector3(camera.getViewMatrix().getTranslation()).multiply(new Vector3(1,1,1));
		rayTo = new Vector3(endRay);
		logger.info("rayTo: " + rayTo);
	}
	
	
	private Vector4 getFrom(int x, int y, DebugCamera camera) {
		Vector4 frustrumData = camera.getFrustrumData();
		float near = frustrumData.x;
		float far = frustrumData.y;
		float right = frustrumData.z;
		float top = frustrumData.w;
		float ndcX, ndcY;           
		Vector3 eye = new Vector3();             
		eye.z = near * far / ((0 * (far - near)) - far);
		ndcX = (((float)x/(float)GraphicsEngine.DISPLAY_WIDTH) - 0.5f) * 2.0f; 
		ndcY = (((float)y/(float)GraphicsEngine.DISPLAY_HEIGHT) - 0.5f) * 2.0f;
		eye.x = (-ndcX * eye.z) * right/near;
		eye.y = (-ndcY * eye.z) * top/near;
		
		Matrix4 invView = camera.getViewMatrix().inverse();
		
		logger.info("Start Ray eyespace: " + eye);
		
		Vector4 eye4 = new Vector4(eye.x, eye.y, eye.z);
		
		return eye4.mutiplyByMatrix(invView);
	}
	
	private Vector4 getTo(int x, int y, DebugCamera camera) {
		Vector4 frustrumData = camera.getFrustrumData();
		float near = frustrumData.x;
		float far = frustrumData.y;
		float right = frustrumData.z;
		float top = frustrumData.w;
		float ndcX, ndcY;           
		Vector3 eye = new Vector3();             
		eye.z = near * far / ((1 * (far - near)) - far);
		ndcX = (((float)x/(float)GraphicsEngine.DISPLAY_WIDTH) - 0.5f) * 2.0f; 
		ndcY = (((float)y/(float)GraphicsEngine.DISPLAY_HEIGHT) - 0.5f) * 2.0f;
		eye.x = (-ndcX * eye.z) * right/near;
		eye.y = (-ndcY * eye.z) * top/near;
		
		Matrix4 invView = camera.getViewMatrix().inverse();
		Vector4 eye4 = new Vector4(eye.x, eye.y, eye.z, 1);
		logger.info("End Ray eyespace: " + eye4);
		
		
		eye4.mutiplyByMatrix(invView);
		
		
		
		return eye4;
	}
	
	@Override
	public void doRequest(DynamicsWorld world) {
		logger.info("Doing physics pick request!");
		//Objects might be in wrong space
		Vector3 rayFrom = new Vector3(camera.getViewMatrix().getTranslation()).multiply(new Vector3(1,1,1));
		rayFrom = new Vector3(new Vector4(0,0,0,1).mutiplyByMatrix(camera.getViewMatrix().inverse()));
		rayFrom.multiply(new Vector3(1,-1,1));
		
		
//		Vector4 rotation = new Vector4(1,0,0);
//		rotation.mutiplyByMatrix(camera.getViewMatrix().inverse());
//		Matrix3 rotMat = new Matrix4().setToRotate(rotation.x, rotation.y, rotation.z, (float)(Math.PI)).getMatrix3();
//		rayFrom.mutiplyByMatrix(rotMat);
		
		logger.info("rayFrom: " + rayFrom);
	//	rayTo = new Vector3(rayFrom).add(new Vector3(0,0,500));
		
		
		ClosestRayResultCallback callback = new ClosestRayResultCallback(rayFrom.toJavaX(), rayTo.toJavaX());
		world.rayTest(rayFrom.toJavaX(), rayTo.toJavaX(), callback);
		PhysicalObject userPointer = null;
		Vector3 worldLocationOut = null;
		if (callback.hasHit()) {
			logger.info("An object has been clicked");
			
			CollisionObject selection = callback.collisionObject;
			userPointer = (PhysicalObject) selection.getUserPointer();
			
			logger.info("Object: " + userPointer + " clicked!");
			Vector3f location = callback.hitPointWorld;
			worldLocationOut = new Vector3(location.x, location.y, location.z);
			ThreadManager.getInstance().onClick(userPointer, x, y, new Vector4(location.x, location.y, location.z));
		}
		IOMouse.returnMouseMetaData(new MouseMetadata(worldLocationOut, userPointer, x, y));
	}
}	
