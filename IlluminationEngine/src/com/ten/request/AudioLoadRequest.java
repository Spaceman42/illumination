package com.ten.request;

import java.io.FileNotFoundException;

import org.apache.log4j.Logger;

import com.ten.audio.AudioEngine;
import com.ten.audio.Sound;

public class AudioLoadRequest extends AudioRequest {
	private static final Logger logger = Logger.getLogger(AudioRequest.class.getName());
	private Sound sound;
	private String location;
	
	public AudioLoadRequest(Sound sound, String location) {
		this.sound = sound;
		this.location = location;
	}
	
	public void doRequest(AudioEngine audioEngine) {
		try {
			sound.setBuffer(audioEngine.addSound(location));
		} catch (FileNotFoundException e) {
			logger.error(e);
			e.printStackTrace();
		}
	}

}
