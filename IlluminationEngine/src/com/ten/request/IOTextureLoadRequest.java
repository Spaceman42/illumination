package com.ten.request;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.lwjgl.BufferUtils;

import com.ten.ThreadManager;
import com.ten.graphics.renderer.model.Texture;

public class IOTextureLoadRequest extends IORequest {
	
	private File location;
	private Texture texture;
	private static Logger logger = Logger.getLogger(IOTextureLoadRequest.class);
	
	public IOTextureLoadRequest(Texture texture, File location) {
		this.texture = texture;
		this.location = location;
	}
	
	@Override
	public void doRequest() throws IOException {
		logger.debug("Loading texture from disk");
        InputStream in = new FileInputStream(location);
        Image tempImage = ImageIO.read(in);
        BufferedImage image = new BufferedImage(tempImage.getWidth(null), tempImage.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        image.getGraphics().drawImage(tempImage, 0, 0, null);
        int pixels[] = new int[image.getWidth() * image.getHeight()];
        image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());
        ByteBuffer buffer = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * 4);
        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                int pixel = pixels[y * image.getWidth() + x];
                buffer.put((byte) ((pixel >> 16) & 0xFF));     // Red component
                buffer.put((byte) ((pixel >> 8) & 0xFF));      // Green component
                buffer.put((byte) (pixel & 0xFF));               // Blue component
                buffer.put((byte) ((pixel >> 24) & 0xFF));    // Alpha component. Only for RGBA
            }
        }
        buffer.flip();
        ThreadManager.getInstance().makeGLRequest(new GLTextureLoadRequest(texture, buffer, image.getWidth(), image.getHeight()));
	}

}
