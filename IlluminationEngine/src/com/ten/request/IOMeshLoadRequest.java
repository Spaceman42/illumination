package com.ten.request;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ten.ThreadManager;
import com.ten.asset.AssetMesh;
import com.ten.asset.ModelLoader;
import com.ten.graphics.renderer.model.Mesh;
import com.ten.physics.shape.BoundingShape;
import com.ten.physics.shape.ShapeConstructionInfo;

public class IOMeshLoadRequest extends IORequest {

	private String location;
	private Mesh mesh;
	private ShapeConstructionInfo shapeInfo;
	private static final Logger logger = Logger.getLogger(IOMeshLoadRequest.class.getName());

	public IOMeshLoadRequest(String meshLocation, Mesh mesh, ShapeConstructionInfo shapeInfo) {
		location = meshLocation;
		this.mesh = mesh;
		this.shapeInfo = shapeInfo;
	}

	public void doRequest() throws IOException {
		Map<String, com.ten.asset.AssetMesh> meshs = ModelLoader.loadModel(new File(
				location+".obj"));
		com.ten.asset.AssetMesh fileMesh = meshs.get("default");
		logger.info("Mesh loaded " + fileMesh.getValue(fileMesh.NORMALS).length);
		
		BoundingShape shape = shapeInfo.constructShape(fileMesh.getValue(AssetMesh.VERTICES), fileMesh.getIndices());
		
		Request glRequest = new GLMeshLoadRequest(mesh,
				fileMesh.getValue(fileMesh.VERTICES),
				fileMesh.getValue(fileMesh.TEXTURES),
				fileMesh.getValue(fileMesh.NORMALS), fileMesh.getIndices(), shape);
		ThreadManager.getInstance().makeGLRequest(glRequest);
	}
}
