package com.ten.request;

import com.ten.audio.AudioEngine;
import com.ten.audio.Sound;

public class AudioPlayStartRequest extends AudioRequest{
	private Sound sound;
	
	public AudioPlayStartRequest(Sound sound) {
		this.sound = sound;
	}

	@Override
	public void doRequest(AudioEngine audioEngine) {
		if (sound.getBuffer() != -1) {
			audioEngine.startSound(sound);
		}
		
	}
	
	
}
