package com.ten.request;

public interface Request {
	/**
	 * Sends the requests to the appropriate place
	 */
	public void make();
}
