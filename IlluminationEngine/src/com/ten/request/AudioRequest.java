package com.ten.request;

import com.ten.ThreadManager;
import com.ten.audio.AudioEngine;

public abstract class AudioRequest implements Request {

	public abstract void doRequest(AudioEngine audioEngine);
	
	@Override
	public void make() {
		ThreadManager.getInstance().makeALRequest(this);
	}

}
