package com.ten;

import java.nio.ByteBuffer;



public class FloatByteBuffer{
	private ByteBuffer buf;
	
	public FloatByteBuffer(int numberOfFloats) {
		buf = ByteBuffer.allocate(numberOfFloats*4);
	}
	
	public FloatByteBuffer(float[] data) {
		buf = ByteBuffer.allocate(data.length*4);
		putFloatArray(data);
	}
	
	public void putFloat(float f) {
		buf.putFloat(f);
	}
	
	public void putFloatArray(float[] data) {
		for (float f : data) {
			buf.putFloat(f);
		}
	}
	
	public ByteBuffer getRawBuffer() {
		return buf;
	}
}	
