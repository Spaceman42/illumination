package com.ten.math;

import javax.vecmath.Vector3f;

/**
 * @author David Gronlund
 */
public class Vector3 {

    public float x;
    public float y;
    public float z;

    public Vector3(Vector3 vec) {
        this.x = vec.x;
        this.y = vec.y;
        this.z = vec.z;
    }

    public Vector3(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3(float val) {
        this.x = this.y = this.z = val;
    }

    public Vector3(boolean initToZero) {
        if (initToZero) {
            this.x = this.y = this.z = 0;
        } else {
            this.x = this.y = this.z = 1;
        }
    }

    public Vector3() {
    }

    public Vector3(Vector4 translation) {
		x = translation.x;
		y = translation.y;
		z = translation.z;
	}

	public float[] toArray() {
        return new float[]{x, y, z};
    }

    public float get(int row) {
        if (row == 0) {
            return x;
        } else if (row == 1) {
            return y;
        } else if (row == 2) {
            return z;
        }
        return 0;
    }

    public void set(int row, float value) {
        if (row == 0) {
            x = value;
        } else if (row == 1) {
            y = value;
        } else if (row == 2) {
            z = value;
        }
    }

    public Vector3 fromArray(float[] args) {
        this.x = args[0];
        this.y = args[1];
        this.z = args[2];
        return this;
    }

    public float magnitude() {
        return GameMath.sqrt(GameMath.sqr(x) + GameMath.sqr(y) + GameMath.sqr(z));
    }

    public Vector3 normalise() {
        float mag = this.magnitude();
        this.x /= mag;
        this.y /= mag;
        this.z /= mag;
        return this;
    }

    public Vector3 translate(Vector3 d) {
        this.x += d.x;
        this.y += d.y;
        this.z += d.z;
        return this;
    }

    public Vector3 translate(float dx, float dy, float dz) {
        this.x += dx;
        this.y += dy;
        this.z += dz;
        return this;
    }

    public Vector3 translate(float d) {
        this.x += d;
        this.y += d;
        this.z += d;
        return this;
    }

    public Vector3 scale(Vector3 s) {
        this.x *= s.x;
        this.y *= s.y;
        this.z *= s.z;
        return this;
    }

    public Vector3 scale(float sx, float sy, float sz) {
        this.x *= sx;
        this.y *= sy;
        this.z *= sz;
        return this;
    }

    public Vector3 scale(float s) {
        this.x *= s;
        this.y *= s;
        this.z *= s;
        return this;
    }

    public Vector3 add(Vector3 operand) {
        this.x += operand.x;
        this.y += operand.y;
        this.z += operand.z;
        return this;
    }

    public Vector3 add(float operand) {
        this.x += operand;
        this.y += operand;
        this.z += operand;
        return this;
    }

    public Vector3 subtract(Vector3 operand) {
        this.x -= operand.x;
        this.y -= operand.y;
        this.z -= operand.z;
        return this;
    }

    public Vector3 subtract(float operand) {
        this.x -= operand;
        this.y -= operand;
        this.z -= operand;
        return this;
    }

    public Vector3 multiply(Vector3 operand) {
        this.x *= operand.x;
        this.y *= operand.y;
        this.z *= operand.z;
        return this;
    }

    public Vector3 multiply(float operand) {
        this.x *= operand;
        this.y *= operand;
        this.z *= operand;
        return this;
    }

    public Vector3 divide(Vector3 operand) {
        this.x /= operand.x;
        this.y /= operand.y;
        this.z /= operand.z;
        return this;
    }

    public Vector3 divide(float operand) {
        this.x /= operand;
        this.y /= operand;
        this.z /= operand;
        return this;
    }

    @Override
    public String toString() {
        return "Vector3: {" + this.x + ", " + this.y + ", " + this.z + "}";
    }
    
    public Vector3f toJavaX() {
    	return new Vector3f(x, y, z);
    }
    
    public Vector3 mutiplyByMatrix(Matrix3 mat) {
		return fromArray(GameMath.multiplyVector(mat.matrix, toArray(),  3));
	}
    
    public Vector3 fromCross(Vector3 v1, Vector3 v2) {
    	x = (v1.y * v2.z) - (v2.y * v1.z);
    	y = (v1.x * v2.z) - (v2.x * v1.z);
    	z = (v1.x * v2.y) - (v2.x * v1.y);
    	return this;
    }
}
