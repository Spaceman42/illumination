package com.ten.math.geom;

import com.ten.math.*;

/**
 * @author David Gronlund
 */
public class Line {

    private Vector4 origin;
    private Vector4 direction;

    public Line(Vector4 origin, Vector4 direction) {
        this.origin = origin;
        this.direction = direction;
        this.direction.normalise();
    }

    public Vector4 getOrigin() {
        return origin;
    }

    public void setOrigin(Vector4 origin) {
        this.origin = origin;
    }

    public Vector4 getDirection() {
        return direction;
    }

    public void setDirection(Vector4 direction) {
        this.direction = direction;
    }

    public Vector4 calculate(float value) {
        return new Vector4(origin).add(new Vector4(direction).multiply(value));
    }
}
