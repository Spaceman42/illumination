package com.ten.io;

import java.io.IOException;

import com.ten.request.IORequest;

public class IOThreadRunnable implements Runnable{
        private IORequest request;
        
        public IOThreadRunnable(IORequest request) {
                this.request = request;
        }
        
        @Override
        public void run() {
                try {
                        request.doRequest();
                } catch (IOException e) {
                        e.printStackTrace();
                }
        }
}
