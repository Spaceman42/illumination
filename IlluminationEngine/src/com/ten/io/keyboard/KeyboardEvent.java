package com.ten.io.keyboard;

import com.ten.io.Event;

public class KeyboardEvent implements Event{

	private Key key;
	private char character;
	private EventType type;

	public KeyboardEvent(Key key, char character, EventType type) {
		this.key = key;
		this.character = character;
		this.type = type;
	}

	public EventType getType() {
		return type;
	}
}
