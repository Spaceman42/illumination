package com.ten.io.keyboard;

import org.lwjgl.input.Keyboard;

public enum Key {
	KEY_1(Keyboard.KEY_1), KEY_2(Keyboard.KEY_2), KEY_3(Keyboard.KEY_3), KEY_4(Keyboard.KEY_4), KEY_5(Keyboard.KEY_5), 
		KEY_6(Keyboard.KEY_6), KEY_7(Keyboard.KEY_7), KEY_8(Keyboard.KEY_8), KEY_9(Keyboard.KEY_9), KEY_0(Keyboard.KEY_0),
	
	KEY_ESC(Keyboard.KEY_ESCAPE), KEY_CTRL(Keyboard.KEY_LCONTROL), KEY_SHIFT(Keyboard.KEY_LSHIFT), KEY_TAB(Keyboard.KEY_TAB),
		KEY_PAGEUP(Keyboard.KEY_NEXT), KEY_PAGEDOWN(Keyboard.KEY_BACK), KEY_END(Keyboard.KEY_END), KEY_DEL(Keyboard.KEY_DELETE), 
		KEY_RETURN(Keyboard.KEY_RETURN), KEY_PLUS(Keyboard.KEY_ADD), KEY_MINUS(Keyboard.KEY_MINUS),
	
	KEY_NUMPAD1(Keyboard.KEY_NUMPAD1), KEY_NUMPAD2(Keyboard.KEY_NUMPAD2), KEY_NUMPAD3(Keyboard.KEY_NUMPAD3), KEY_NUMPAD4(Keyboard.KEY_NUMPAD4), 
		KEY_NUMPAD5(Keyboard.KEY_NUMPAD5), KEY_NUMPAD6(Keyboard.KEY_NUMPAD6), KEY_NUMPAD7(Keyboard.KEY_7), KEY_NUMPAD8(Keyboard.KEY_NUMPAD8), 
		KEY_NUMPAD9(Keyboard.KEY_NUMPAD9), KEY_NUMPAD0(Keyboard.KEY_NUMPAD0),
		
	KEY_NUMBADRETURN(Keyboard.KEY_NUMPADENTER),
	
	KEY_A(Keyboard.KEY_A), KEY_B(Keyboard.KEY_B), KEY_C(Keyboard.KEY_C), KEY_D(Keyboard.KEY_D), KEY_E(Keyboard.KEY_E), KEY_F(Keyboard.KEY_F), KEY_G(Keyboard.KEY_G), 
		KEY_H(Keyboard.KEY_H), KEY_I(Keyboard.KEY_I), KEY_J(Keyboard.KEY_J), KEY_K(Keyboard.KEY_K), KEY_L(Keyboard.KEY_L), KEY_M(Keyboard.KEY_M),
		KEY_N(Keyboard.KEY_N), KEY_O(Keyboard.KEY_O), KEY_P(Keyboard.KEY_P), KEY_Q(Keyboard.KEY_Q), KEY_R(Keyboard.KEY_R), KEY_S(Keyboard.KEY_S),
		KEY_T(Keyboard.KEY_T), KEY_U(Keyboard.KEY_U), KEY_V(Keyboard.KEY_V), KEY_W(Keyboard.KEY_W), KEY_X(Keyboard.KEY_X), KEY_Y(Keyboard.KEY_Y),
		KEY_Z(Keyboard.KEY_Z), 
	
	NO_KEY(-1), KEY_SPACE(Keyboard.KEY_SPACE);
	
	private boolean down;
	private char lastChar;
	private int key;
	
	Key (int key) {
		this.key = key;
	}
 	
	public boolean isDown() {
		return down;
	}
	
	protected void setDown(boolean down) {
		this.down = down;
	}
	
	protected boolean isKey(int key) {
		return this.key == key;
	}
	
	protected void setChar(char character) {
		lastChar = character;
	}
	/**
	 * 
	 * @return - Character that the key last represented. For example, if the KEY_G is was pressed, its character would be set to either 'g' 
	 * or 'G' depending on the state of shift when KEY_G was pressed.
	 */
	public char getChar() {
		return lastChar;
	}
}
