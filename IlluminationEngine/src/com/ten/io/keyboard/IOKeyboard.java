package com.ten.io.keyboard;

import static org.lwjgl.input.Keyboard.*;

import java.util.ArrayList;
import java.util.Set;

import java.util.List;

import org.lwjgl.input.Keyboard;

import com.ten.io.Event.EventType;
import com.ten.io.EventListnerMap;

/**
 * If the key requested was down last time the keyboard was polled the key's
 * value will return true
 * 
 * @author Ethan Hunter
 * 
 */
public class IOKeyboard {
	private static EventListnerMap<Key, KeyboardEventListener> eventListeners = new EventListnerMap<Key, KeyboardEventListener>();

	public static boolean isKeyDown(Key key) {
		return key.isDown();
	}

	protected static void registerEventListener(KeyboardEventListener listener) {
		eventListeners.add(listener.getKeys(), listener);
	}

	protected static void removeEventListener(KeyboardEventListener listener) {
		eventListeners.remove(listener.getKeys(), listener);
	}

	public static void updateKeys() {
		if (Keyboard.isCreated()) {
			while (next()) {
				if (getEventKeyState()) {
					char character = getEventCharacter();
					int key = getEventKey();
					Key downKey = checkKeys(key);
					downKey.setChar(character);
					downKey.setDown(true);
					Set<KeyboardEventListener> keyList = eventListeners
							.getValuesList(downKey);
					if (keyList != null) {
						for (KeyboardEventListener l : keyList) {
							l.keyboardEventPerformed(new KeyboardEvent(downKey,
									character, EventType.PRESSED));
						}
					}
				} else {
					int key = getEventKey();
					Key upKey = checkKeys(key);
					upKey.setDown(false);
					Set<KeyboardEventListener> keyList = eventListeners
							.getValuesList(upKey);
					if (keyList != null) {
						for (KeyboardEventListener l : keyList) {
							l.keyboardEventPerformed(new KeyboardEvent(upKey,
									getEventCharacter(), EventType.RELEASED));
						}
					}
				}
			}
			for (Key k : Key.values()) {
				if (k.isDown()) {
					Set<KeyboardEventListener> keyList = eventListeners
							.getValuesList(k);
					if (keyList != null) {
						for (KeyboardEventListener l : keyList) {
							l.keyboardEventPerformed(new KeyboardEvent(k, k
									.getChar(), EventType.IS_DOWN));
						}
					}
				}
			}
		}
	}

	private static Key checkKeys(int key) {
		for (Key k : Key.values()) {
			if (k.isKey(key)) {
				return k;
			}
		}
		return Key.NO_KEY;
	}
}
