package com.ten.io.keyboard;

import java.util.List;

public abstract class KeyboardEventListener {
	private Key[] keys;
	
	public KeyboardEventListener() {
		keys = Key.values();
		register();
	}
	
	public KeyboardEventListener(Key key) {
		keys = new Key[] {key};
		register();
	}
	
	public KeyboardEventListener(Key[] keys) {
		this.keys = keys;
		register();
	}
	
	public KeyboardEventListener(List<Key> keys) {
		Object[] objectKeys = keys.toArray();
		this.keys = new Key[objectKeys.length];
		for (int i = 0; i<objectKeys.length; i++) {
			this.keys[i] = (Key) objectKeys[i];
		}
		register();
	}
	
	private void register() {
		IOKeyboard.registerEventListener(this);
	}
	
	public abstract void keyboardEventPerformed(KeyboardEvent e);
	
	protected Key[] getKeys() {
		return keys;
	}
}
