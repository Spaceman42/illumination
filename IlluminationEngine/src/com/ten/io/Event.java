package com.ten.io;

public interface Event {
	public enum EventType {
		PRESSED, IS_DOWN, RELEASED;
	}
	
	public EventType getType();
}
