package com.ten.io;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EventListnerMap<K, V> {
	private HashMap<K, Set<V>> map;
	
	public EventListnerMap() {
		map = new HashMap<K, Set<V>>();
	}
	public void add(K key, V value) {
		if (map.containsKey(key)) {
			Set<V> valuesList = getValuesList(key);
			valuesList.add(value);
		} else {
			Set<V> valuesList = new HashSet<V>();
			valuesList.add(value);
			map.put(key, valuesList);
		}
	}
	
	public void add(K[] keys, V value) {
		for (K key : keys) {
			add(key, value);
		}
	}
	
	public void remove(K[] keys, V value) {
		for (K key : keys) {
			remove(key, value);
		}
	}
	/**
	 * @param key
	 * @return list of values associated with the key. Returns {@code null} if there were never any values associated with the given key
	 */
	public Set<V> getValuesList(K key) {
		return map.get(key);
	}
	
	public boolean remove(K key, V value) {
		Set<V> valuesList = getValuesList(key);
		if (valuesList != null) {
			return valuesList.remove(value);
		} else {
			return false;
		}
	}
	/** 
	 * Removes the object {@code value} from every key mapping that it exists. 
	 * @param value
	 */
	public void remove(V value) {
		for (K key : map.keySet()) {
			Set<V> valuesList = getValuesList(key);
			if (valuesList != null) {
				valuesList.remove(valuesList);
			}
		}
	}
}
