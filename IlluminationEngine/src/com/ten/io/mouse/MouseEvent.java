package com.ten.io.mouse;

import com.ten.io.Event;
import com.ten.math.Vector3;
import com.ten.physics.PhysicalObject;

public class MouseEvent implements Event{

	private int button;
	private EventType type;
	
	private MouseMetadata metadata;
	
	public MouseEvent(int button, MouseMetadata metadata, EventType type) {
		this.button = button;
		this.metadata = metadata;
		this.type = type;
	}
	
	public int getButton() {
		return button;
	}
	
	public EventType getType() {
		return type;
	}
	
	public Vector3 getWorldLocation() {
		return metadata.getWorldLocation();
	}
	
	public int getScreenX() {
		return metadata.getScreenX();
	}
	
	public int getScreenY() {
		return metadata.getScreenY();
	}
	
	public PhysicalObject getObjectUnderMouse() {
		return metadata.getObjectUnderMouse();
	}
}
