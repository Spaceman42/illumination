package com.ten.io.mouse;
import com.ten.math.Vector3;
import com.ten.physics.PhysicalObject;
public class MouseMetadata {
	
	private Vector3 worldLocation;
	private PhysicalObject overObject;
	
	private int screenX;
	private int screenY;
	
	public MouseMetadata(Vector3 worldLocation, PhysicalObject overObject, int screenX, int screenY) {
		this.worldLocation = worldLocation;
		this.overObject = overObject;
		this.screenX = screenX;
		this.screenY = screenY;
	}
	
	public Vector3 getWorldLocation() {
		return worldLocation;
	}
	
	public PhysicalObject getObjectUnderMouse() {
		return overObject;
	}
	
	public int getScreenX() {
		return screenX;
	}
	
	public int getScreenY() {
		return screenY;
	}

}
