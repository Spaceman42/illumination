package com.ten.io.mouse;

public abstract class MouseEventListener {
	private int[] buttons;
	
	public MouseEventListener(int[] buttons) {
		this.buttons = buttons;
		register();
	}
	
	public MouseEventListener(int button) {
		buttons = new int[] {button};
		register();
	}
	
	private void register() {
		IOMouse.registerEventListener(this);
	}
	
	public abstract void mouseEventPerformed(MouseEvent e);

	public int[] getButtons() {
		return buttons;
	}
	
	public Integer[] getButtonsAsIntegers() {
		Integer[] integers = new Integer[buttons.length];
		for (int i = 0; i<buttons.length; i++) {
			integers[i] = new Integer(buttons[i]);
		}
		return integers;
	}
}
