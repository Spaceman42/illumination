package com.ten.io.mouse;

import static org.lwjgl.input.Mouse.*;

import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

import org.lwjgl.input.Mouse;

import com.ten.game.DebugCamera;
import com.ten.io.Event.EventType;
import com.ten.io.EventListnerMap;
import com.ten.math.Vector3;
import com.ten.physics.PhysicalObject;
import com.ten.request.PhysicsPickRequest;

public class IOMouse {
	private IOMouse() {
	}

	/**
	 * Buttons are numbered starting with 0. Typically button zero is the left
	 * mouse button
	 */
	private static final int buttonCount = 3;

	private static boolean[] buttonsDown = new boolean[3];

	private static LinkedBlockingQueue<MouseMetadata> metadataQueue = new LinkedBlockingQueue<MouseMetadata>();

	private static EventListnerMap<Integer, MouseEventListener> eventListeners = new EventListnerMap<Integer, MouseEventListener>();

	private static MouseMetadata metadata = null;

	public static void registerEventListener(MouseEventListener e) {
		eventListeners.add(e.getButtonsAsIntegers(), e);
	}

	protected static void removeEventListener(MouseEventListener e) {
		eventListeners.remove(e.getButtonsAsIntegers(), e);
	}

	public static void updateMouse(DebugCamera camera) {
		if (Mouse.isCreated()) {
			new PhysicsPickRequest(getX(), getY(), null, camera).make();
			try {
				metadata = metadataQueue.take();
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.exit(-1);
			}
			while (next()) {
				if (getEventButtonState()) {
					int button = getEventButton();
					buttonsDown[button] = true;
					Set<MouseEventListener> listeners = eventListeners
							.getValuesList(button);
					if (listeners != null) {
						for (MouseEventListener l : listeners) {
							l.mouseEventPerformed(new MouseEvent(button,
									metadata, EventType.PRESSED));
						}
					}
				} else {
					int button = getEventButton();
					if (button != -1) {
						buttonsDown[button] = false;
						Set<MouseEventListener> listeners = eventListeners
								.getValuesList(button);
						if (listeners != null) {
							for (MouseEventListener l : listeners) {
								l.mouseEventPerformed(new MouseEvent(button,
										metadata, EventType.RELEASED));
							}
						}
					}
				}
			}
			for (int i = 0; i < buttonCount; i++) {
				if (buttonsDown[i]) {
					Set<MouseEventListener> listeners = eventListeners
							.getValuesList(i);
					if (listeners != null) {
						for (MouseEventListener l : listeners) {
							l.mouseEventPerformed(new MouseEvent(i, metadata,
									EventType.IS_DOWN));
						}
					}
				}
			}
		}

	}

	public static boolean isButtonDown(int button) {
		return Mouse.isButtonDown(button);
	}

	public static void returnMouseMetaData(MouseMetadata o) {
		metadataQueue.add(o);
	}

	public static Vector3 getWorldLocation() {
		if (metadata != null) {
			return metadata.getWorldLocation();
		} else {
			return null;
		}
	}

	public static int getScreenX() {
		if (metadata != null) {
			return metadata.getScreenX();
		} else {
			return -1;
		}
	}

	public static int getScreenY() {
		if (metadata != null) {
			return metadata.getScreenX();
		} else {
			return -1;
		}
	}

	/**
	 * 
	 * @return the physical object under the mouse. {@code null} if there is no
	 *         object under the mouse
	 */
	public static PhysicalObject getObjectUnderMouse() {
		if (metadata != null) {
			return metadata.getObjectUnderMouse();
		} else {
			return null;
		}
	}

}
