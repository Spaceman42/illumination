package com.ten;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.ten.audio.AudioThread;
import com.ten.game.AbstractGame;
import com.ten.game.DebugCamera;
import com.ten.game.GameThread;
import com.ten.graphics.GLThread;
import com.ten.graphics.RenderableObject;
import com.ten.io.IOThreadRunnable;
import com.ten.physics.PhysicalObject;
import com.ten.physics.PhysicsEngine;
import com.ten.physics.PhysicsThread;
import com.ten.request.AudioRequest;
import com.ten.request.IORequest;
import com.ten.request.PhysicsRequest;
import com.ten.request.Request;

import com.ten.math.Vector4;

public class ThreadManager {

	private static ThreadManager instance;

	private GameThread gameThread;
	private GLThread glThread;
	private AudioThread alThread;
	private Executor ioThreads;
	private PhysicsThread physThread;

	/**
	 * Creates a thread manager with a GLThread, a GameThread, and a ThreadPool for
	 * the IO threads.
	 * 
	 */
	private ThreadManager() {
		AppState.stateThreadAmount = 3;
		AppState.stateChangeFinished = 3;
	}

	private void setup() {
		ioThreads = new ThreadPoolExecutor(5, 20, 1, TimeUnit.MILLISECONDS,
				new LinkedBlockingQueue<Runnable>());	
		gameThread = new GameThread();
		glThread = new GLThread();
		alThread = new AudioThread();
		physThread = new PhysicsThread();
	}
	
	protected void startThreads() {
		glThread.start();
		gameThread.start();
		alThread.start();
		physThread.start();
	}
	
	protected void setGame(AbstractGame game) {
		gameThread.setGame(game);
	}
	public void updatePhysicsSet(Set<PhysicalObject> set) {
		physThread.updateObjects(set);
	}
	public void requestStateChange(AppState state) {
		// TODO: Figure a real way for this to work
		AppState.setState(state);
	}

	public void updateGameData(DebugCamera camera, List<RenderableObject> objects) {
		glThread.getEngine().update(camera, objects);
	}

	public void makeGLRequest(Request request) {
		glThread.getEngine().makeGLRequest(request);
	}
	
	public void onClick(Object objectID, int screenX, int screenY, Vector4 worldPosition) {
		gameThread.getGame().onClickExternal(objectID, screenX, screenY, worldPosition);
	}
	
	public void makeALRequest(Request request) {
		alThread.makeAudioRequest((AudioRequest)request);
	}

	/**
	 * Executes an IO request using a Thread Executor.
	 * 
	 * @param request The IO request to run
	 */
	public void makeIoRequest(Request request) {
		ioThreads.execute(new IOThreadRunnable((IORequest) request));
	}

	/**
	 * Singleton method that returns an instance of ThreadManager
	 * 
	 * @return An instance of ThreadManager
	 */
	public static ThreadManager getInstance() {
		if (instance == null) {
			instance = new ThreadManager();
			instance.setup();
		}
		return instance;
	}

	public void makePhysRequest(PhysicsRequest physicsRequest) {
		physThread.makeRequest(physicsRequest);
	}
}
