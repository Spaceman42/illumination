package com.ten;

import java.nio.ByteBuffer;

public class IntByteBuffer {
	private ByteBuffer buf;
	
	public IntByteBuffer(int numberOfInts) {
		buf = ByteBuffer.allocate(numberOfInts*4);
	}
	
	public IntByteBuffer(int[] data) {
		buf = ByteBuffer.allocate(data.length*4);
		addIntArray(data);
	}

	public void addIntArray(int[] data) {
		for (int i : data) {
			buf.putInt(i);
		}
	}
	
	public void addInt(int i) {
		buf.putInt(i);
	}
	
	public ByteBuffer getRawBuffer() {
		return buf;
	}
}
