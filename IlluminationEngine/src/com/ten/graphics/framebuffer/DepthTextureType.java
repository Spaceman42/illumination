package com.ten.graphics.framebuffer;

import static org.lwjgl.opengl.GL14.*;
import static org.lwjgl.opengl.GL30.*;

public enum DepthTextureType {
	DEPTH24_STENCIL8(GL_DEPTH24_STENCIL8), DEPTH24(GL_DEPTH_COMPONENT24), DEPTH32(GL_DEPTH_COMPONENT32), DEPTH16(
			GL_DEPTH_COMPONENT16);
	protected int pointer;

	DepthTextureType(int pointer) {
		this.pointer = pointer;
	}
}
