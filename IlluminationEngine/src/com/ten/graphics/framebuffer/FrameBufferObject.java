package com.ten.graphics.framebuffer;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.OpenGLException;


import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;

public class FrameBufferObject {
	private int id;
	private Map<FBOAttachment, Integer> textures;
	private int x;
	private int y;
	private int bits;

	/**
	 * @author Ethan Hunter
	 * @param x
	 *            - width of FrameBuffer in pixels
	 * @param y
	 *            - height of FrameBuffer in pixels
	 * @param bitsPerTexel
	 *            - The size in bits of one texel of one color render target
	 *            attachment Must be: 8, 16, 32, 64, 128
	 * 
	 */
	public FrameBufferObject(int x, int y, int bitsPerTexel) {
		this.x = x;
		this.y = y;
		this.bits = bitsPerTexel;
		textures = new HashMap<FBOAttachment, Integer>();
		id = glGenFramebuffers();
	}

	/**
	 * @param textureType
	 *            - Must have the same number of bits as bitsPerTexel
	 * @see Note: If the attachment is already in use it will be replaced.
	 */
	public void addRenderTarget(TextureType textureType, FBOAttachment attachment) {
		if (textureType.bits == bits) {
			glBindFramebuffer(GL_FRAMEBUFFER, id);
			int tex = glGenTextures();
			glBindTexture(GL_TEXTURE_2D, tex);
			glTexImage2D(GL_TEXTURE_2D, 0, textureType.pointer, x, y, 0, textureType.type, GL_FLOAT,
					(ByteBuffer) null);
			GL11.glTexParameteri(GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
			GL11.glTexParameteri(GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
			GL11.glTexParameteri(GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
			GL11.glTexParameteri(GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
			glBindTexture(GL_TEXTURE_2D, 0);
			glFramebufferTexture2D(GL_FRAMEBUFFER, attachment.pointer, GL_TEXTURE_2D, tex, 0);
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			textures.put(attachment, tex);
		}
	}

	/**
	 * @param attachment
	 *            - Should be a depth attachment
	 * @see Note: If the attachment passed in is already in use the resulting texture
	 *      from this method will replace it.
	 */
	public void addDepthTexture(DepthTextureType textureType, FBOAttachment attachment) {
		glBindFramebuffer(GL_FRAMEBUFFER, id);
		int tex = glGenTextures();
		glBindTexture(GL_TEXTURE_2D, tex);
		glTexImage2D(GL_TEXTURE_2D, 0, textureType.pointer, x, y, 0, GL_DEPTH_COMPONENT, GL_FLOAT,
				(ByteBuffer) null);
		GL11.glTexParameteri(GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
		GL11.glTexParameteri(GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
		GL11.glTexParameteri(GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glTexParameteri(GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
		glBindTexture(GL_TEXTURE_2D, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, attachment.pointer, GL_TEXTURE_2D, tex, 0);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		textures.put(attachment, tex);
	}

	public void useFBO() {
		glBindFramebuffer(GL_FRAMEBUFFER, id);
	}
	
	public void clearFBO() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
	public static void unbindFBO() {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	
	public void bindTexture(FBOAttachment attachment, int glTexActiveTexture) {
		
		int texture = textures.get(attachment);
		glActiveTexture(glTexActiveTexture);
		glBindTexture(GL_TEXTURE_2D, texture);
	}
	
	public void drawBuffers() {
		glBindFramebuffer(GL_FRAMEBUFFER, id);
		IntBuffer buffer = BufferUtils.createIntBuffer(3);
		buffer.put(new int[] {GL_COLOR_ATTACHMENT0,GL_COLOR_ATTACHMENT1});
		buffer.flip();
		GL20.glDrawBuffers(buffer);
	}
	public void drawNoBuffers() {
		GL20.glDrawBuffers(GL_NONE);
	}

	public void addDepthRenderBuffer() {
		glBindFramebuffer(GL_FRAMEBUFFER, id);
		int renderBuf = glGenRenderbuffers();
		glBindRenderbuffer(GL_RENDERBUFFER, renderBuf);
		glRenderbufferStorage(GL_RENDERBUFFER,GL_DEPTH24_STENCIL8, x, y);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_STENCIL_ATTACHMENT,GL_RENDERBUFFER, renderBuf);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);	
	}
	
	public int getRawTexture(FBOAttachment attachment) {
		return textures.get(attachment).intValue();
	}

}
