package com.ten.graphics.framebuffer;

public class InvalidParameterException extends Exception {

	public InvalidParameterException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -3694484116532263987L;

}
