package com.ten.graphics.framebuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.*;

public enum TextureType {
	RGBA32F(GL_RGBA32F, 128, GL_RGBA), RGBA16F(GL_RGBA16F, 64, GL_RGBA), RGBA12(GL_RGBA12, 48, GL_RGBA), RGBA8(GL_RGBA8, 32, GL_RGBA), RG32F(GL_RG32F, 64,
			GL_RGBA), RG16F(GL_RG16F, 32, GL_RG), RG8(GL_RG8, 16, GL_RG), R32F(GL_R32F, 32, GL_R), R16F(GL_R16F, 16, GL_RED), R8(GL_R8, 8, GL_R);

	protected final int pointer;
	protected final int bits;
	protected final int type;

	TextureType(int pointer, int bits, int type) {
		this.pointer = pointer;
		this.bits = bits;
		this.type = type;
	}
}
