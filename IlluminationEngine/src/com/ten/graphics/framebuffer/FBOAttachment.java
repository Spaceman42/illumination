package com.ten.graphics.framebuffer;

import static org.lwjgl.opengl.GL30.*;
public enum FBOAttachment {
	COLOR0(GL_COLOR_ATTACHMENT0), COLOR1(GL_COLOR_ATTACHMENT1), COLOR2(GL_COLOR_ATTACHMENT2), COLOR3(
			GL_COLOR_ATTACHMENT3), DEPTH(GL_DEPTH_ATTACHMENT), STENCIL(GL_STENCIL_ATTACHMENT), DEPTH_STENCIL(
			GL_DEPTH_STENCIL_ATTACHMENT);

	protected final int pointer;

	FBOAttachment(int pointer) {
		this.pointer = pointer;
	}
}
