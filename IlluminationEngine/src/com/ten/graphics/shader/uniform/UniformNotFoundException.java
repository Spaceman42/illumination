package com.ten.graphics.shader.uniform;

public class UniformNotFoundException extends Exception {

	private static final long serialVersionUID = -4534180856736675818L;
}
