package com.ten.graphics.shader.uniform;

import com.ten.math.Vector3;

public class UniformVector3 implements Uniform {

	private Vector3 vector;
	
	public UniformVector3(Vector3 vector) {
		this.vector = vector;
	}
	
	@Override
	public Uniform.Type getType() {
		return Uniform.Type.VECTOR3;
	}
	
	public float getX() {
		return vector.x;
	}
	
	public float getY() {
		return vector.y;
	}
	
	public float getZ() {
		return vector.z;
	}
}
