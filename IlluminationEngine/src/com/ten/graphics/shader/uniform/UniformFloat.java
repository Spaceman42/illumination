package com.ten.graphics.shader.uniform;

public class UniformFloat implements Uniform{
	private float value;
	
	public UniformFloat(float value) {
		this.value = value;
	}
	
	@Override
	public Type getType() {
		return Type.FLOAT;
	}
	
	public float getValue() {
		return value;
	}

}
