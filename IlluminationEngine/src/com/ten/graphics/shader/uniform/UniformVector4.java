package com.ten.graphics.shader.uniform;

import com.ten.math.Vector4;

public class UniformVector4 implements Uniform {
	
	private Vector4 vector; 
	
	public UniformVector4(Vector4 vector) {
		this.vector = vector;
	}
	
	@Override
	public Uniform.Type getType() {
		return Uniform.Type.VECTOR4;
	}
	
	public float getX() {
		return vector.x;
	}
	
	public float getY() {
		return vector.y;
	}
	
	public float getZ() {
		return vector.z;
	}
	
	public float getW() {
		return vector.w;
	}
}
