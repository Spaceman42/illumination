package com.ten.graphics.shader.uniform;

import com.ten.graphics.shader.uniform.Uniform.Type;

public class UniformInt implements Uniform{
	private int value;
	
	public UniformInt(int value) {
		this.value = value;
	}
	
	@Override
	public Type getType() {
		return Type.INT;
	}
	
	public int getValue() {
		return value;
	}
}
