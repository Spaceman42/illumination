package com.ten.graphics.shader.uniform;

import java.nio.FloatBuffer;

import com.ten.math.Matrix4;

public class UniformMatrix4 implements Uniform {
	private Matrix4 matrix;
	
	public UniformMatrix4(Matrix4 matrix) {
		this.matrix = matrix;
	}
	
	@Override
	public Uniform.Type getType() {
		return Uniform.Type.MATRIX4;
	}
	
	public FloatBuffer getData() {
		FloatBuffer buffer = matrix.toBuffer();
		buffer.flip();
		return buffer;
	}

}
