package com.ten.graphics.shader.uniform;

import java.nio.FloatBuffer;

import com.ten.math.Matrix3;

public class UniformMatrix3 implements Uniform {

	private Matrix3 matrix;
	
	public UniformMatrix3(Matrix3 matrix) {
		this.matrix = matrix;
	}
	
	@Override
	public Uniform.Type getType() {
		return Uniform.Type.MATRIX3;
	}
	
	public FloatBuffer getData() {
		FloatBuffer buffer = matrix.toBuffer();
		buffer.flip();
		return buffer;
	}
}
