package com.ten.graphics.shader.uniform;

public interface Uniform {
	
	public enum Type {
		MATRIX4, VECTOR4, MATRIX3, VECTOR3, VECTOR2, FLOAT, INT;
	}

	public Uniform.Type getType();
}
