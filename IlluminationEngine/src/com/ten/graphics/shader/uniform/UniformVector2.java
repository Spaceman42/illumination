package com.ten.graphics.shader.uniform;


public class UniformVector2 implements Uniform{

	private float x;
	private float y;
	
	public UniformVector2(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
	public Uniform.Type getType() {
		return Uniform.Type.VECTOR2;
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
}
