package com.ten.graphics.shader;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

public class ShaderManager {
	public static final Logger logger = Logger.getLogger(ShaderManager.class
			.getName());

	private static int boundShader = -1;
	public static Map<String, Shader> shaders = new ConcurrentHashMap<String, Shader>();

	public static Shader loadShader(String name, String location) {
		Shader shader = shaders.get(name);
		if (!shaders.containsKey(name)) {
			try {
				shader = new Shader(location, name);
			} catch (IOException e) {
				logger.error("Shader not found");
				e.printStackTrace();
			}
			shaders.put(name, shader);
		}
		return shader;
	}

	public static Shader loadShader(String name, String gsLocation,
			String vsLocation, String fsLocation,
			String[] transformFeebackVaryings) {
		Shader shader = shaders.get(name);
		if (!shaders.containsKey(name)) {
			try {
				shader = new Shader(gsLocation, vsLocation, fsLocation,
						transformFeebackVaryings, name);
			} catch (IOException e) {
				logger.error("Shader not found");
				e.printStackTrace();
			}
			shaders.put(name, shader);
		}
		return shader;
	}

	public static Shader getShader(String name) {
		return shaders.get(name);
	}

	public static boolean testBoundShader(int shader) {
		return boundShader != shader;
	}
}
