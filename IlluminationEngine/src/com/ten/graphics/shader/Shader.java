package com.ten.graphics.shader;

import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL32.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.ten.Utils;
import com.ten.graphics.shader.uniform.Uniform;
import com.ten.graphics.shader.uniform.UniformFloat;
import com.ten.graphics.shader.uniform.UniformInt;
import com.ten.graphics.shader.uniform.UniformMatrix3;
import com.ten.graphics.shader.uniform.UniformMatrix4;
import com.ten.graphics.shader.uniform.UniformNotFoundException;
import com.ten.graphics.shader.uniform.UniformVector2;
import com.ten.graphics.shader.uniform.UniformVector3;
import com.ten.graphics.shader.uniform.UniformVector4;

/**
 * 
 * @author David Gronlund, Ethan Hunter
 * 
 */
// TODO: attributeLocations probably isn't needed, and uniformLocations probably
// isn't either
public class Shader {

	private static Logger logger = Logger.getLogger(Shader.class.getName());

	private int program = -1;
	private Map<String, Integer> attributeLocations;
	private Map<String, Integer> uniformLocations;
	
	private boolean hasTransformFeedbackVaryings = false;
	private boolean hasGeometryShader = false;;

	/**
	 * Creates a new shader instance from the location of two files, one for the
	 * vertex and one for the fragment shader.
	 * 
	 * @param srcLocation
	 *            The location of the shader to load
	 * @throws IOException
	 *             Throws exception if reading the shader fails
	 */
	public Shader(String srcLocation, String shaderName) throws IOException {
		this(null, srcLocation + ".vs", srcLocation + ".fs", null, shaderName);
	}

	public Shader(String geometryLoc, String vertexLoc, String fragmentLoc,
			String[] transformFeedbackVaryings, String shaderName)
			throws IOException {
		attributeLocations = new HashMap<String, Integer>();
		uniformLocations = new HashMap<String, Integer>();
		String fragmentSrc = Utils.loadFile(new File(fragmentLoc));
		String vertexSrc = Utils.loadFile(new File(vertexLoc));
		String geometrySrc = null;
		if (geometryLoc != null) {
			geometrySrc = Utils.loadFile(new File(geometryLoc));
			hasGeometryShader = true;
		}
		
		int vertexShader = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertexShader, vertexSrc);
		glCompileShader(vertexShader);

		String vertexLog = glGetShaderInfoLog(vertexShader, 65536);
		if (vertexLog.length() != 0) {
			logger.error(shaderName + " Vertex Shader Error Log:\n" + vertexLog);
		}

		int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragmentShader, fragmentSrc);
		glCompileShader(fragmentShader);

		String fragmentLog = glGetShaderInfoLog(vertexShader, 65536);
		if (fragmentLog.length() != 0) {
			logger.error(shaderName + " Fragment Shader Error Log:\n"
					+ fragmentLog);
		}
		int geometryShader = -1;
		if (geometrySrc != null) {
			geometryShader = glCreateShader(GL_GEOMETRY_SHADER);
			glShaderSource(geometryShader, geometrySrc);
			glCompileShader(geometryShader);

			String geometryLog = glGetShaderInfoLog(geometryShader, 65536);
			if (geometryLog.length() != 0) {
				logger.error(shaderName + " Geometry Shader Error Log:\n" + geometryLog);
			}
		}
		program = glCreateProgram();
		
		
		glBindAttribLocation(program, 0, "vertex");
		glBindAttribLocation(program, 1, "normal");
		glBindAttribLocation(program, 2, "uv");
		glBindAttribLocation(program, 3, "tangent");
		glBindAttribLocation(program, 4, "bitangent");
		if (geometryShader != -1) {
			glAttachShader(program, geometryShader);
		}
		glAttachShader(program, vertexShader);
		glAttachShader(program, fragmentShader);
		
		if (transformFeedbackVaryings != null) {
			glTransformFeedbackVaryings(program, transformFeedbackVaryings, GL_INTERLEAVED_ATTRIBS);
			hasTransformFeedbackVaryings = true;
		}
		
		glLinkProgram(program);

		String programLog = glGetProgramInfoLog(program, 65536);
		if (programLog.length() != 0) {
			logger.error(shaderName + " Program Error Log:\n" + programLog);
		}
		// TODO: Make samplers work
		int numAttributes = glGetProgrami(program, GL_ACTIVE_ATTRIBUTES);
		int maxAttributesLength = glGetProgrami(program,
				GL_ACTIVE_UNIFORM_MAX_LENGTH);
		for (int i = 0; i < numAttributes; i++) {
			String name = glGetActiveAttrib(program, i, maxAttributesLength);
			int location = glGetAttribLocation(program, name);
			attributeLocations.put(name, location);
		}

		int numUniforms = glGetProgrami(program, GL_ACTIVE_UNIFORMS);
		int maxUniformLength = glGetProgrami(program,
				GL_ACTIVE_UNIFORM_MAX_LENGTH);
		for (int i = 0; i < numUniforms; i++) {
			String name = glGetActiveUniform(program, i, maxUniformLength);
			int location = glGetUniformLocation(program, name);
			uniformLocations.put(name, location);
		}
	}

	public void setUniform(String name, Uniform uniform) {
		int location = glGetUniformLocation(program, name);
		if (uniform.getType() == Uniform.Type.MATRIX4) {
			UniformMatrix4 uni = (UniformMatrix4) uniform;
			glUniformMatrix4(location, false, uni.getData());
		} else if (uniform.getType() == Uniform.Type.VECTOR4) {
			UniformVector4 uniformVec4 = (UniformVector4) uniform;
			glUniform4f(location, uniformVec4.getX(), uniformVec4.getY(),
					uniformVec4.getZ(), uniformVec4.getW());
		} else if (uniform.getType() == Uniform.Type.MATRIX3) {
			glUniformMatrix3(location, false,
					((UniformMatrix3) uniform).getData());
		} else if (uniform.getType() == Uniform.Type.VECTOR3) {
			UniformVector3 uniformVec3 = (UniformVector3) uniform;
			glUniform3f(location, uniformVec3.getX(), uniformVec3.getY(),
					uniformVec3.getZ());
		} else if (uniform.getType() == Uniform.Type.VECTOR2) {
			UniformVector2 uniformVec2 = (UniformVector2) uniform;
			glUniform2f(location, uniformVec2.getX(), uniformVec2.getY());
		} else if (uniform.getType() == Uniform.Type.FLOAT) {
			UniformFloat uniformFloat = (UniformFloat) uniform;
			glUniform1f(location, uniformFloat.getValue());
		} else if (uniform.getType() == Uniform.Type.INT) {
			UniformInt uniformInt = (UniformInt) uniform;
			glUniform1i(location, uniformInt.getValue());
		}
	}

	public void setUniform(String name, float uniform) {
		Integer location = glGetUniformLocation(program, name);// uniformLocations.get(name);
		int locationInt = location.intValue();
		glUniform1f(locationInt, uniform);
	}

	public void setUniform(String name, int uniform) {
		glUniform1i(glGetUniformLocation(program, name), uniform);
	}

	public void setSampler(String name, int value) {
		glUniform1i(glGetUniformLocation(program, name), value);
	}

	public void useProgram() {
		if (ShaderManager.testBoundShader(program)) {
			glUseProgram(program);
		}
	}

	public int getProgram() {
		return program;
	}

	public Map<String, Integer> getAttributes() {
		return attributeLocations;
	}

	public Map<String, Integer> getUniforms() {
		return uniformLocations;
	}
	
	public boolean hasGeometryShader() {
		return hasGeometryShader;
	}
	
	public boolean hasTransformFeedbackVaryings() {
		return hasTransformFeedbackVaryings;
	}
}
