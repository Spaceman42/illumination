package com.ten.graphics;

import static org.lwjgl.opengl.GL11.glGetError;
import static org.lwjgl.opengl.GL11.glViewport;

import java.nio.channels.ShutdownChannelGroupException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.PixelFormat;
import org.lwjgl.util.glu.GLU;

import com.ten.SettingsTable;
import com.ten.AppState;
import com.ten.ThreadManager;
import com.ten.Utils;
import com.ten.game.DebugCamera;
import com.ten.graphics.renderer.DeferredRenderer;
import com.ten.graphics.renderer.Renderer;
import com.ten.gui.IlluminationGUI;
import com.ten.gui.nifty.NiftyGUIManager;
import com.ten.math.GameMath;
import com.ten.math.Matrix4;
import com.ten.math.Vector4;
import com.ten.request.GLRequest;
import com.ten.request.PhysicsPickRequest;
import com.ten.request.Request;

public class GraphicsEngine {

	public static int DISPLAY_WIDTH = 1024;
	public static int DISPLAY_HEIGHT = 768;

	private static Logger logger = Logger.getLogger(GraphicsEngine.class.getName());

	private long lastEsc = 0;

	private List<RenderableObject> objects;
	private DebugCamera camera;
	private Renderer renderer;
	private BlockingQueue<GLRequest> toDoRequests;
	private static SettingsTable<SettingsTable> settings = new SettingsTable<SettingsTable>(Configeration.ENABLED);
	private IlluminationGUI gui;

	private Matrix4 projectionMatrix;

	private float near = -1;
	private float far = -1;
	private float fov = -1;

	/**
	 * Creates a new Graphics Engine as well as the Display, and a deferred
	 * renderer.
	 * 
	 * @throws LWJGLException
	 *             Exception for when Display creation fails
	 */
	public GraphicsEngine(IlluminationGUI gui) {
		toDoRequests = new LinkedBlockingQueue<GLRequest>();
		logger.info("Graphics Engine Created");
		objects = new ArrayList<RenderableObject>();
		this.gui = gui;
	}

	public void update(DebugCamera camera, List<RenderableObject> objects) {
		logger.info("update called");
		DebugCamera tempCam = new DebugCamera(camera);
		List<RenderableObject> tempObjects = new ArrayList<RenderableObject>(objects);
		updateSync(tempCam, tempObjects);
	}

	private synchronized void updateSync(DebugCamera camera, List<RenderableObject> objects) {
		this.camera = camera;
		this.objects = objects;
	}

	public void makeGLRequest(Request request) {
		toDoRequests.add((GLRequest) request);
	}

	private void doRequests() throws LWJGLException {
		// TODO: Base number of request done on current frame time
		List<GLRequest> unfinishedRequests = new ArrayList<GLRequest>();
		while (toDoRequests.peek() != null) {
			GLRequest r = toDoRequests.poll();
			if (!r.doRequest()) {
				unfinishedRequests.add(r);
			}
		}
		toDoRequests.addAll(unfinishedRequests);
	}

	public void run() throws Exception {
		if (!Display.isCreated()) {
			Display.setDisplayMode(new DisplayMode(DISPLAY_WIDTH, DISPLAY_HEIGHT));
			Display.setResizable(false); //TODO: Fix deferred render for resizing
			Display.create(new PixelFormat());
			ThreadManager.getInstance().requestStateChange(AppState.RUNNING);
			glViewport(0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT);
			renderer = new DeferredRenderer();
			renderer.init();
		}
		logger.info("Engine running");
		while (AppState.getState() != AppState.SHUTTING_DOWN) {
			boolean resized = false;
			if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE) && (Utils.getTime() - lastEsc) > 200) {
				if (Mouse.isGrabbed()) {
					Mouse.setGrabbed(false);
				} else {
					Mouse.setGrabbed(true);
				}
				lastEsc = Utils.getTime();
			}
			
			if (Keyboard.isKeyDown(Keyboard.KEY_0)) {
				resizeDisplay();
				resized = true;
			}

			Matrix4 projectionMatrix = getProjectionMatrix(camera);

			if (Display.wasResized() || resized) {
				handleResize();
			}
			renderer.render(objects, camera, projectionMatrix);
			gui.render();
			doRequests();
			Display.sync(60);
			Display.update();
			if (Display.isCloseRequested()) {
				AppState.closeRequested = true;
				AppState.setState(AppState.SHUTTING_DOWN);
			}
		}
		AppState.setState(AppState.CLOSING); 
		while (AppState.getState() != AppState.CLOSING) {
			try {
				Thread.sleep(1);
				logger.info("Waiting for final shutdown request");
			} catch (InterruptedException e) {}
		}
		Display.destroy();
		System.exit(-1);
		
		
	}
	
	public static void setViewport(int x, int y) {
		glViewport(0, 0, x, y);
	}
	
	public static void resetViewport() {
		glViewport(0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT);
	}

	private void handleResize() throws LWJGLException {
		DISPLAY_WIDTH = Display.getWidth();
		DISPLAY_HEIGHT = Display.getHeight();
		glViewport(0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT);
		projectionMatrix = GameMath.projectionMatrix(near, far, 45,(float) DISPLAY_WIDTH/(float)DISPLAY_HEIGHT);
		logger.info("New Display Width: " + DISPLAY_WIDTH);
		logger.info("New Display Height: " + DISPLAY_HEIGHT);
	}
	
	private void resizeDisplay() throws LWJGLException {
		Display.setDisplayMode(new DisplayMode(1024, 768));
	}

	private Matrix4 getProjectionMatrix(DebugCamera camera) {
		if (camera != null) {
			float near = camera.getNear();
			float far = camera.getFar();
			float fov = camera.getFov();

			if ((this.near != near) || (this.far != far) || (this.fov != fov)) {
				this.near = near;
				this.far = far;
				this.fov = fov;
				projectionMatrix = GameMath.projectionMatrix(near, far, fov, (float) DISPLAY_WIDTH / (float) DISPLAY_HEIGHT);
				logger.info("creating new projection matrix");
				logger.info("new fov: " + fov);
			}
		}
		return projectionMatrix;

	}

	/**
	 * Checks if there is an OpenGL error and if so writes it to a logger and
	 * then exits the program.
	 * 
	 * @param logger
	 *            The logger to display any errors on
	 */
	public static void checkGLError(Logger logger) {
		int error = glGetError();
		if (error != GL11.GL_NO_ERROR) {
			String message = GLU.gluErrorString(error);
			logger.error(message);
			System.err.println(message);
			if (Display.isCreated()) {
				Display.destroy();
			}
			System.exit(-1);
		}
	}
	
	public static void checkGLError(Logger logger, String msg) {
		int error = glGetError();
		if (error != GL11.GL_NO_ERROR) {
			String message = GLU.gluErrorString(error);
			logger.error(message + " " + msg);
			System.err.println(message + " " + msg);
			if (Display.isCreated()) {
				Display.destroy();
			}
			
			System.exit(-1);
		}
	}

	public static void addSetting(String setting, SettingsTable settingsTable) {
		settings.setSetting(setting, settingsTable);
	}

	public static SettingsTable readSetting(String setting) {
		return settings.getSetting(setting);
	}
}
