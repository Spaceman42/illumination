package com.ten.graphics;

import com.ten.game.GameObject;
import com.ten.graphics.shader.Shader;
import com.ten.math.Matrix4;
import com.ten.physics.shape.BoundingBox;
import com.ten.physics.shape.BoundingShape;

public interface RenderableObject extends GameObject {
	
	public void draw(Shader shader);
	
	public Matrix4 getModelMatrix();

	public BoundingShape[] getBBs();
}
