package com.ten.graphics.particle;

public abstract class Particle {
	private int duration;
	
	public abstract void draw();
	public abstract void delete();
	
	public void setDuration(int value) {
		this.duration = value;
	}
	
	public int getDuration() {
		return duration;
	}
}
