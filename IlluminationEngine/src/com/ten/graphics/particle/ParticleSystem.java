package com.ten.graphics.particle;

import java.util.Map;
import java.util.Set;

public interface ParticleSystem {
	public ParticleAlgorithm getAlgorithm(Particle particle);
	public Set<Particle> getParticles();
	public Map<Particle, Integer> getSpawnRates();
}
