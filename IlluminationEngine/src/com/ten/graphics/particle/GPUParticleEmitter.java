package com.ten.graphics.particle;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.ten.math.Vector3;

public class GPUParticleEmitter implements ParticleEmitter {
	private static final Logger logger = Logger.getLogger(GPUParticleEmitter.class.getName());
	
	private final GPUParticleSystem system;
	private Vector3 location;
	private int particlesPerSecond;

	
	public GPUParticleEmitter(GPUParticleSystem system, Vector3 location, int particlesPerSecond) {
		this.system = system;
		this.location = location;
		this.particlesPerSecond = particlesPerSecond;
	}
	
	
	public int getSpawnRate(Particle particle) {
		return system.getSpawnRates().get(particle).intValue();
	}
	/**
	 * Emits a defined number of particles using particle spawn rates as a basis for the ratio of particle types
	 * @param numverOfParticles
	 */
	public void emitParticlesDefined(int numberOfParticles) {
		int totalSpawRate = 0;
		for (Particle particle : system.getParticles()) {
			totalSpawRate += getSpawnRate(particle);
		}
	}
	/**
	 * Emits particles based on particle spawn rates for the given amount of time in milliseconds
	 * @param timeMillis
	 */
	public void emitParticlesTime(int timeMillis) {
		
	}
	/**
	 * Emits particles until {@code stopEmitting} is called
	 */
	public void emitParticlesUndefined() {
		
	}
	/**
	 * Stops emitting particles
	 */
	public void stopEmitting() {
		
	}
}
