package com.ten.graphics.particle;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class GPUParticleSystem implements ParticleSystem {
	private Map<Particle, Integer> spawnRates;
	private Map<Particle, GPUParticleAlgorithm> algorithms;
	
	public GPUParticleSystem() {
		spawnRates = new HashMap<Particle, Integer>();
		algorithms = new HashMap<Particle, GPUParticleAlgorithm>();
	}
	
	public void addParticle(Particle particle, GPUParticleAlgorithm algorithm,
			int spawnFrequency) {
		spawnRates.put(particle, new Integer(spawnFrequency));
		algorithms.put(particle, algorithm);
	}
	@Override
	public GPUParticleAlgorithm getAlgorithm(Particle particle) {
		return algorithms.get(particle);
	}
	
	@Override
	public Set<Particle> getParticles() {
		return algorithms.keySet();
	}
	
	@Override
	public Map<Particle, Integer> getSpawnRates() {
		return spawnRates;	
	}

}
