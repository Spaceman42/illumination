package com.ten.graphics.particle;

import java.util.HashMap;
import java.util.Map;

import com.ten.graphics.shader.Shader;
import com.ten.graphics.shader.ShaderManager;
import com.ten.graphics.shader.uniform.Uniform;

public abstract class GPUParticleAlgorithm implements ParticleAlgorithm{
	private Shader shader;
	private Map<String, Uniform> parameters;
	
	protected GPUParticleAlgorithm(String particleShaderLocation, String shaderName) {
		String gsLocation = particleShaderLocation + ".gs";
		String vsLocation = particleShaderLocation + ".vs";
		String fsLocation = particleShaderLocation + ".fs";
		shader = ShaderManager.loadShader(shaderName, gsLocation, vsLocation, fsLocation, null);
		parameters = new HashMap<String, Uniform>();
	}
	
	protected void setParameter(String name, Uniform value) {
		parameters.put(name, value);
	}
	
	protected Uniform getParameter(String name) {
		return parameters.get(name);
	}
	
	public void setTime(int time) {
		shader.setUniform("time", time);
	}
	
	
}
