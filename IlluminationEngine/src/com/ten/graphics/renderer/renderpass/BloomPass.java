package com.ten.graphics.renderer.renderpass;

import org.apache.log4j.Logger;

import com.ten.SettingsTable;
import com.ten.graphics.Configeration;
import com.ten.graphics.GraphicsEngine;
import com.ten.graphics.framebuffer.FBOAttachment;
import com.ten.graphics.framebuffer.FrameBufferObject;
import com.ten.graphics.renderer.FBOPoolManager;
import com.ten.graphics.renderer.Quad;
import com.ten.graphics.shader.Shader;
import com.ten.graphics.shader.ShaderManager;
import com.ten.graphics.shader.uniform.UniformNotFoundException;
import com.ten.graphics.shader.uniform.UniformVector2;

import static org.lwjgl.opengl.GL13.*;

public class BloomPass {
	private static final Logger logger = Logger.getLogger(BloomPass.class
			.getName());

	private SettingsTable<Object> settings;

	public BloomPass() {
		settings = new SettingsTable<Object>(Configeration.ENABLED);
		settings.setSetting("cutoff", 0.7f);
		settings.setSetting("blurSize", 4);
		GraphicsEngine.addSetting("Bloom", settings);
	}

	public void doPass(GBuffer gbuffer) throws UniformNotFoundException {
		if (settings.getState() == Configeration.ENABLED) {
			FBOPoolManager fboPool = gbuffer.getFBOPool();
			FrameBufferObject rFBO = fboPool.getCurrentFBO();
			FrameBufferObject wFBO = fboPool.getFbo();

			wFBO.useFBO();
			rFBO.bindTexture(FBOAttachment.COLOR0, GL_TEXTURE0);
			Shader bloomShader = ShaderManager.getShader("Bloom");
			bloomShader.useProgram();
			GraphicsEngine.checkGLError(logger, "use program");
			bloomShader.setSampler("uColor", 0);
			GraphicsEngine.checkGLError(logger, "setting sampler");
			bloomShader.setUniform("cutoff",
					(Float) settings.getSetting("cutoff"));
			GraphicsEngine.checkGLError(logger, "cutoff");
			bloomShader.setUniform("blurSize",
					(Integer) settings.getSetting("blurSize"));
			GraphicsEngine.checkGLError(logger, "blursize");
			bloomShader.setUniform("buffersize",
					new UniformVector2(GraphicsEngine.DISPLAY_WIDTH,
							GraphicsEngine.DISPLAY_HEIGHT));
			GraphicsEngine.checkGLError(logger, "setting uniforms");
			Quad.getInstance().draw();

			fboPool.setCurrentFBO(wFBO);
			fboPool.addAndClearFbo(rFBO);

			GraphicsEngine.checkGLError(logger, "End of pass");
		}
	}
}
