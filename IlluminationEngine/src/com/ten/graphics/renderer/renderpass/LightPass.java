package com.ten.graphics.renderer.renderpass;

import java.util.Set;

import org.apache.log4j.Logger;
import org.lwjgl.opengl.GL20;

import com.ten.graphics.GraphicsEngine;
import com.ten.graphics.framebuffer.FBOAttachment;
import com.ten.graphics.framebuffer.FrameBufferObject;
import com.ten.graphics.renderer.Cube;
import com.ten.graphics.renderer.FBOPoolManager;
import com.ten.graphics.renderer.Quad;
import com.ten.graphics.renderer.light.Light;
import com.ten.graphics.renderer.light.LightRegistry;
import com.ten.graphics.renderer.light.LightType;
import com.ten.graphics.renderer.light.PointLight;
import com.ten.graphics.renderer.light.SunLight;
import com.ten.graphics.shader.Shader;
import com.ten.graphics.shader.ShaderManager;
import com.ten.graphics.shader.uniform.UniformMatrix4;
import com.ten.graphics.shader.uniform.UniformNotFoundException;
import com.ten.graphics.shader.uniform.UniformVector2;
import com.ten.graphics.shader.uniform.UniformVector3;
import com.ten.graphics.shader.uniform.UniformVector4;
import com.ten.math.Matrix3;
import com.ten.math.Matrix4;
import com.ten.math.Vector3;
import com.ten.math.Vector4;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;

//TODO: This class isn't very scalable, along with the entire lighting system. 
//TODO: Settings table
//TODO: THIS CLASS NEEDS SOOOOOOOOO MUCH WORK
public class LightPass {
	private static final float MOD = 50f;

	private static Logger logger = Logger.getLogger(LightPass.class.getName());

	public void doPass(GBuffer gbuffer) throws UniformNotFoundException {
		
		Cube cube = Cube.getInstance();
		
		FrameBufferObject fbo = gbuffer.getFBO();
		FBOPoolManager fboPool = gbuffer.getFBOPool();

		FrameBufferObject wFBO = fboPool.getFbo();
		if (wFBO == null) {
			logger.fatal("Write fbo is null");
			System.exit(-1);
		}
		wFBO.useFBO();
		fboPool.setCurrentFBO(wFBO);
		fbo.bindTexture(FBOAttachment.COLOR1, GL_TEXTURE5);
		fbo.bindTexture(FBOAttachment.DEPTH, GL_TEXTURE6);
		fbo.bindTexture(FBOAttachment.COLOR0, GL_TEXTURE4);
		Light[] lights = LightRegistry.getLights();
		
		logger.info("Rending " + lights.length + " lights");
		
		Matrix4 viewMatrix = gbuffer.getViewMatrix();
		Vector4 viewTransformVector = viewMatrix.getTranslation();
		Matrix4 viewMatrixBillBoard = new Matrix4().setTranslation(viewTransformVector);
		Matrix4 projectionMatrix = gbuffer.getProjectionMatrix();
		
		glDisable(GL_CULL_FACE);
		glEnable(GL_STENCIL_TEST);
		glClear(GL_STENCIL_BUFFER_BIT);
		glStencilFunc(GL_ALWAYS, 0, 0);
		GL20.glStencilOpSeparate(GL_BACK, GL_KEEP, GL_INCR, GL_KEEP);
		GL20.glStencilOpSeparate(GL_FRONT, GL_KEEP, GL_DECR, GL_KEEP);
		
		Shader stencilShader = ShaderManager.getShader("PassThrough");
		stencilShader.useProgram();
		for (Light l : lights) {
			if (l.getType() == LightType.POINT_LIGHT) {
				Matrix4 lMat = new Matrix4().setTranslation(l.getLocation());
				
				float mScaler = getBoundingSize(l.getIntensity());
				lMat.scale(mScaler, mScaler, mScaler);
				
				Matrix4 mvMat = new Matrix4(viewMatrix).multiply(lMat);
				
				//float mScaler = l.getIntensity()*MOD;
				//mvMat.scale(mScaler, mScaler, mScaler);
				
				Matrix4 mvpMat = new Matrix4(projectionMatrix).multiply(mvMat);
				stencilShader.setUniform("lMat", new UniformMatrix4(mvpMat));
				cube.draw();
			}
		}
		
		
		
		
		
		glDisable(GL_DEPTH_TEST);
		
		glDisable(GL_STENCIL_TEST);
		glStencilFunc(GL_NOTEQUAL, 0, 0xFF);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);
		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE, GL_ONE);
		
		
		Shader shader = ShaderManager.getShader("PointLight");
		shader.useProgram();
		shader.setUniform("vMat", new UniformMatrix4(viewMatrix));
		shader.setSampler("gNormal", 5);
		shader.setSampler("gColor", 4);
		shader.setSampler("gDepth", 6);
		shader.setUniform("pMat", new UniformMatrix4(projectionMatrix));
		shader.setUniform("frustrumData", new UniformVector4(gbuffer.getFrustrumData()));
		shader.setUniform("buffersize", new UniformVector2(GraphicsEngine.DISPLAY_WIDTH, GraphicsEngine.DISPLAY_HEIGHT));
		
		for (Light l : lights) {
			if (l.getType() == LightType.POINT_LIGHT) {
				
				Vector3 location = l.getLocation();
			
				shader.setUniform("lightColor", new UniformVector4(l.getColor()));
				shader.setUniform("location", new UniformVector3(location));
				shader.setUniform("intensity", l.getIntensity());
				
				Matrix4 lightMatrix = new Matrix4().setIdentity();
				lightMatrix.translate(location.x, location.y, location.z);
				
				float mScaler = getBoundingSize(l.getIntensity());
				lightMatrix.scale(mScaler, mScaler, mScaler);
				
				Matrix4 mvMat = new Matrix4(viewMatrix).multiply(lightMatrix);
			//	mvMat.setMatrix3(new Matrix3().setIdentity());
				
				//float mScaler = l.getIntensity()*MOD;
				//mvMat.scale(mScaler, mScaler, mScaler);
				
				Matrix4 mvpMat = new Matrix4(projectionMatrix).multiply(mvMat);
				shader.setUniform("lMat", new UniformMatrix4(mvpMat));
				cube.draw();
			}
		}
		Shader shaderSun = ShaderManager.getShader("SunLight");
		shaderSun.useProgram();
		shaderSun.setUniform("vMat", new UniformMatrix4(viewMatrix));
		shaderSun.setSampler("gNormal", 5);
		shaderSun.setSampler("gColor", 4);
		shaderSun.setSampler("gDepth", 6);
		shaderSun.setUniform("pMat", new UniformMatrix4(projectionMatrix));
		shaderSun.setUniform("frustrumData", new UniformVector4(gbuffer.getFrustrumData()));
		shaderSun.setUniform("buffersize", new UniformVector2(GraphicsEngine.DISPLAY_WIDTH, GraphicsEngine.DISPLAY_HEIGHT));
		//glClear(GL_STENCIL_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glStencilFunc(GL_ALWAYS, 0, 0xFF);
		for (Light l : lights) {
			if (l.getClass() == SunLight.class) {
				Vector3 location = l.getLocation();
			
				shaderSun.setUniform("lightColor", new UniformVector4(l.getColor()));
				shaderSun.setUniform("lightLocation", new UniformVector3(location));
				shaderSun.setUniform("intensity", l.getIntensity());
				
				
				Cube.getInstance().draw();
			}
		}

		glDisable(GL_BLEND);
		glEnable(GL_DEPTH_TEST);
		GraphicsEngine.checkGLError(logger);
	//	glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		// TODO: Shadow drawing and other light types
		FrameBufferObject.unbindFBO();
	}
	
	private float getBoundingSize(float x) {
		return (-0.0462963f*(x*x))+(6.94444f*x)+29f;
	}

}
