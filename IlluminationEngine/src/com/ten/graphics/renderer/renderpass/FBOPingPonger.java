package com.ten.graphics.renderer.renderpass;

import com.ten.graphics.framebuffer.FBOAttachment;
import com.ten.graphics.framebuffer.FrameBufferObject;
import com.ten.graphics.framebuffer.TextureType;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL11.*;

public class FBOPingPonger {
	private FrameBufferObject[] fbos;
	private int writeFbo = -1;
	private int readFbo  = -1;
	
	/**
	 * Creates two identical FrameBufferObjects with a width of resX and a height of resY with a bitsPerTexel parameter of texelBits.
	 * Each FBO has 1 texture of type textureType bound to the COLOR0 location.
	 * @param textureType
	 * @param texelBits
	 * @param resX
	 * @param resY
	 */
	public FBOPingPonger(TextureType textureType, int texelBits, int resX, int resY) {
		fbos = new FrameBufferObject[2];
		fbos[0] = new FrameBufferObject(resX, resY, texelBits);
		fbos[0].addRenderTarget(textureType, FBOAttachment.COLOR0);
		
		fbos[1] = new FrameBufferObject(resX, resY, texelBits);
		fbos[1].addRenderTarget(textureType, FBOAttachment.COLOR0);
		
		writeFbo = 0;
		readFbo = 1;
	}
	
	public void drawTo() {
		fbos[0].useFBO();	}
	
	public void readFrom() {
		fbos[0].bindTexture(FBOAttachment.COLOR0, GL_TEXTURE12);
	}
	
	public void swapBuffers() {
		int tempRead = readFbo;
		readFbo = writeFbo;
		writeFbo = tempRead;
	}
	
	public void unbindAll() {
		FrameBufferObject.unbindFBO();
	}

	public void clearBuffers() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
	


}
