package com.ten.graphics.renderer.renderpass;

import java.util.List;

import org.apache.log4j.Logger;
import org.lwjgl.opengl.GL11;

import com.ten.graphics.GraphicsEngine;
import com.ten.graphics.RenderableObject;
import com.ten.graphics.framebuffer.FrameBufferObject;
import com.ten.graphics.renderer.Cube;
import com.ten.graphics.renderer.Quad;
import com.ten.graphics.shader.Shader;
import com.ten.graphics.shader.ShaderManager;
import com.ten.graphics.shader.uniform.UniformMatrix3;
import com.ten.graphics.shader.uniform.UniformMatrix4;
import com.ten.graphics.shader.uniform.UniformNotFoundException;
import com.ten.math.GameMath;
import com.ten.math.Matrix4;
import com.ten.math.Vector4;
import com.ten.physics.shape.BoundingBox;

public class GeometryPass {

	private static final Logger logger = Logger.getLogger(GeometryPass.class
			.getName());

	public void doPass(List<RenderableObject> objects, GBuffer gbuffer)
			throws UniformNotFoundException {
		FrameBufferObject fbo = gbuffer.getFBO();
		Matrix4 projectionMatrix = gbuffer.getProjectionMatrix();
		Matrix4 viewMatrix = gbuffer.getViewMatrix();
		fbo.useFBO();
		fbo.clearFBO();
		Shader shader = ShaderManager.getShader("GeometryPass");
		shader.useProgram();
		for (RenderableObject o : objects) {
			Matrix4 modelMatrix = o.getModelMatrix();
			Matrix4 mvMat = new Matrix4(viewMatrix).multiply(modelMatrix);
			shader.setUniform("mvMat3", new UniformMatrix3(mvMat.getMatrix3()));
			Matrix4 mvpMat = new Matrix4(projectionMatrix).multiply(mvMat);
			shader.setUniform("nMat",
					new UniformMatrix3(GameMath.normalMatrix(mvMat)));
			shader.setUniform("mvpMat", new UniformMatrix4(mvpMat));
			
			o.draw(shader);
			//drawBB(o, viewMatrix, projectionMatrix, shader);
		}
		FrameBufferObject.unbindFBO();
		gbuffer.getFBOPool().setCurrentFBO(fbo);
		GraphicsEngine.checkGLError(logger);
		logger.info("Geometry Pass Ran");
	}

	private void drawBB(RenderableObject o, Matrix4 viewMatrix,
			Matrix4 projectionMatrix, Shader shader)
			throws UniformNotFoundException {
		if (o.getBBs() != null) {
			GL11.glDisable(GL11.GL_CULL_FACE);
			GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);

			Matrix4 modelMatrix = new Matrix4(o.getModelMatrix());
			BoundingBox bb =(BoundingBox) o.getBBs()[0];
			if (bb != null) {
				Matrix4 vMat = new Matrix4(viewMatrix);

				// vMat.rotate(1, 0, 0, (float) Math.PI-1);
				// modelMatrix.rotate(1, 0, 0, (float) Math.PI/2);
				// modelMatrix.translate(0, 0, bb.extentZ - bb.originZ);
				logger.info("bb extent: " + bb.extentZ);
				modelMatrix.translate(bb.originX, bb.originY, bb.originZ);
				modelMatrix.scale(bb.halfExtentX, bb.halfExtentY,
						bb.halfExtentZ);

				logger.info("Bounding box:" + bb.originX + ", " + bb.originY
						+ ", " + bb.originZ);
				modelMatrix.setTranslation(modelMatrix.getTranslation()
						.multiply(new Vector4(1, 1, 1)));
				Matrix4 mvMat = new Matrix4(vMat).multiply(modelMatrix);

				Matrix4 mvpMat = new Matrix4(projectionMatrix).multiply(mvMat);

				shader.setUniform("mvpMat", new UniformMatrix4(mvpMat));
				Cube.getInstance().draw();

				GL11.glPolygonMode(GL11.GL_FRONT, GL11.GL_FILL);
			}
			GL11.glEnable(GL11.GL_CULL_FACE);
			GL11.glCullFace(GL11.GL_BACK);
		}
	}
}
