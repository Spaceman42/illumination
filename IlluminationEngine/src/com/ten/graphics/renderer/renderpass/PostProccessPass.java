package com.ten.graphics.renderer.renderpass;

import com.ten.graphics.renderer.Quad;

public interface PostProccessPass {
	public void doPass(GBuffer gbuffer);
}
