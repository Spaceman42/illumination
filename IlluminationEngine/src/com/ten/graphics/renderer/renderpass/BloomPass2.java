package com.ten.graphics.renderer.renderpass;

import static org.lwjgl.opengl.GL13.*;

import com.ten.SettingsTable;
import com.ten.graphics.Configeration;
import com.ten.graphics.GraphicsEngine;
import com.ten.graphics.framebuffer.FBOAttachment;
import com.ten.graphics.framebuffer.FrameBufferObject;
import com.ten.graphics.renderer.FBOPoolManager;
import com.ten.graphics.renderer.Quad;
import com.ten.graphics.shader.Shader;
import com.ten.graphics.shader.ShaderManager;
import com.ten.graphics.shader.uniform.UniformNotFoundException;
import com.ten.graphics.shader.uniform.UniformVector2;

public class BloomPass2 {
	private SettingsTable<Object> settings;

	public BloomPass2() {
		settings = new SettingsTable<Object>(Configeration.ENABLED);
		settings.setSetting("cutoff", 0.7f);
		settings.setSetting("blurSize", 32);
		GraphicsEngine.addSetting("Bloom", settings);
	}

	public void doPass(GBuffer gbuffer) throws UniformNotFoundException {
		if (settings.getState() == Configeration.ENABLED) {
			FBOPoolManager fboPool = gbuffer.getFBOPool();

			FrameBufferObject inFbo = fboPool.getCurrentFBO();
			FrameBufferObject tmpFbo = fboPool.getFbo();
			FrameBufferObject tmpFbo2 = fboPool.getFbo();
			FrameBufferObject writeFbo = fboPool.getFbo();

			Shader cutoffShader = ShaderManager.getShader("BloomCutoff");

			inFbo.bindTexture(FBOAttachment.COLOR0, GL_TEXTURE0);
			tmpFbo.useFBO();

			cutoffShader.useProgram();
			cutoffShader.setSampler("gColor", 0);
			cutoffShader.setUniform("cutoff",
					(float) settings.getSetting("cutoff"));

			Quad.getInstance().draw();

			FrameBufferObject.unbindFBO();
			
			tmpFbo.bindTexture(FBOAttachment.COLOR0, GL_TEXTURE1);
			tmpFbo2.useFBO();
			Shader horz = ShaderManager.getShader("horzBlur");
			horz.useProgram();
			horz.setSampler("uColor", 1);
			horz.setUniform("buffersize", new UniformVector2(GraphicsEngine.DISPLAY_WIDTH, GraphicsEngine.DISPLAY_HEIGHT));
			horz.setUniform("blurSize", (int) settings.getSetting("blurSize"));
			Quad.getInstance().draw();
			
			writeFbo.useFBO();
			tmpFbo2.bindTexture(FBOAttachment.COLOR0, GL_TEXTURE2);
			Shader vert = ShaderManager.getShader("vertBlur");
			vert.useProgram();
			vert.setSampler("uColor", 2);
			vert.setSampler("gColor", 0);
			vert.setUniform("buffersize", new UniformVector2(GraphicsEngine.DISPLAY_WIDTH, GraphicsEngine.DISPLAY_HEIGHT));
			vert.setUniform("blurSize", (int) settings.getSetting("blurSize"));
			Quad.getInstance().draw();
			
			FrameBufferObject.unbindFBO();
			fboPool.addAndClearFbo(tmpFbo);
			fboPool.addAndClearFbo(tmpFbo2);
			fboPool.addAndClearFbo(inFbo);
			fboPool.setCurrentFBO(writeFbo);
		}
	}
}
