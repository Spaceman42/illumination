package com.ten.graphics.renderer.renderpass;

import com.ten.graphics.framebuffer.FBOAttachment;
import com.ten.graphics.framebuffer.FrameBufferObject;
import com.ten.graphics.renderer.FBOPoolManager;
import com.ten.graphics.renderer.Quad;
import com.ten.graphics.shader.Shader;
import com.ten.graphics.shader.ShaderManager;
import com.ten.graphics.shader.uniform.UniformNotFoundException;

import static org.lwjgl.opengl.GL13.*;

public class FinalPass {
	public static void doPass(GBuffer gBuffer)
			throws UniformNotFoundException {
		FBOPoolManager fboPool = gBuffer.getFBOPool();
		FrameBufferObject rFbo = fboPool.getCurrentFBO();
		
		rFbo.bindTexture(FBOAttachment.COLOR0, GL_TEXTURE0);
		Shader shader = ShaderManager.getShader("final");
		shader.useProgram();
		shader.setSampler("gColor", 0);
		Quad.getInstance().draw();
		fboPool.setCurrentFBO(null);
		fboPool.addAndClearFbo(rFbo);
		FrameBufferObject.unbindFBO();
	}
}
