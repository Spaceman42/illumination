package com.ten.graphics.renderer.renderpass;

import org.apache.log4j.Logger;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.glu.Project;

import com.ten.game.DebugCamera;
import com.ten.graphics.framebuffer.FrameBufferObject;
import com.ten.math.Matrix4;
import com.ten.math.Vector4;
import com.ten.request.PhysicsPickRequest;
import com.ten.request.PhysicsPickRequest;

import static org.lwjgl.opengl.GL11.*;

public class MouseInputPass {
	
	private static final Logger logger = Logger.getLogger(MouseInputPass.class.getName());
	
	
	
	public void doPass(GBuffer gbuffer, DebugCamera camera) {
		FrameBufferObject gfbo = gbuffer.getFBO();
		gfbo.useFBO();
		if (Mouse.isButtonDown(0)) {
			logger.info("Sending physics pick request!");
			
			Matrix4 invPV = new Matrix4(camera.getProjectionMatrix()).multiply(camera.getViewMatrix());
			
			invPV.inverse();
			
			new PhysicsPickRequest(Mouse.getX(), Mouse.getY(), new Vector4(), camera).make();
		}
		FrameBufferObject.unbindFBO();
	}
}
