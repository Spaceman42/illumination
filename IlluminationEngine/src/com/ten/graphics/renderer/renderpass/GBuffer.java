package com.ten.graphics.renderer.renderpass;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Logger;

import com.ten.graphics.GraphicsEngine;
import com.ten.graphics.ShadowMap;
import com.ten.graphics.framebuffer.DepthTextureType;
import com.ten.graphics.framebuffer.FBOAttachment;
import com.ten.graphics.framebuffer.FrameBufferObject;
import com.ten.graphics.framebuffer.TextureType;
import com.ten.graphics.renderer.FBOPoolManager;
import com.ten.graphics.renderer.model.Texture;
import com.ten.graphics.shader.Shader;
import com.ten.graphics.shader.ShaderManager;
import com.ten.math.Matrix4;
import com.ten.math.Vector4;

public class GBuffer {
        
        private static Logger logger = Logger.getLogger(GBuffer.class.getName());
        
        private Matrix4 projectionMatrix;
        private Matrix4 viewMatrix;
        private Matrix4 invProjectionMatrix;
        private List<ShadowMap> shadowMaps;
        private FrameBufferObject fbo;
        private Vector4 frustrumData;
        private FBOPoolManager fboPool;
        private Texture noise;

        public GBuffer() {
               
                ShaderManager.loadShader("GeometryPass", "res/shaders/GeometryPass");
                ShaderManager.loadShader("PointLight", "res/shaders/PointLight");
                ShaderManager.loadShader("SSAO", "res/shaders/SSAO");
                ShaderManager.loadShader("AOBlur", "res/shaders/aoBlur");
                ShaderManager.loadShader("final", "res/shaders/Final");
                ShaderManager.loadShader("ShadowDraw", "res/shaders/ShadowDraw");
                ShaderManager.loadShader("Bloom", "res/shaders/Bloom");
                ShaderManager.loadShader("PassThrough", "res/shaders/passThrough");
                ShaderManager.loadShader("Bloom2", "res/shaders/Bloom2");
                ShaderManager.loadShader("BloomCutoff", "res/shaders/BloomCutoff");
                ShaderManager.loadShader("AmbientLight", "res/shaders/AmbientLight");
                ShaderManager.loadShader("SunLight", "res/shaders/SunLight");
                ShaderManager.loadShader("FXAA", "res/shaders/FXAA");
                ShaderManager.loadShader("horzBlur", "res/shaders/horzBlur");
                ShaderManager.loadShader("vertBlur", "res/shaders/vertBlur");
                ShaderManager.loadShader("HDRLuminance", "res/shaders/HDRLuminance");
                ShaderManager.loadShader("HDRExposure", "res/shaders/HDRExposure");
                shadowMaps = new ArrayList<ShadowMap>();
                fbo = new FrameBufferObject(GraphicsEngine.DISPLAY_WIDTH,
                                GraphicsEngine.DISPLAY_HEIGHT, 64);
                fbo.addDepthTexture(DepthTextureType.DEPTH32,
                                FBOAttachment.DEPTH);
                fbo.addRenderTarget(TextureType.RGBA16F, FBOAttachment.COLOR0);
                fbo.addRenderTarget(TextureType.RGBA16F, FBOAttachment.COLOR1);
                fbo.drawBuffers();

                
                fboPool = new FBOPoolManager(4);
                noise = Texture.loadTexture("res/textures/randomNoise.png");
        }

        public void frustrumData(Vector4 data) {
                this.frustrumData = data;
        }

        public Vector4 getFrustrumData() {
                return frustrumData;
        }

        public void matrixData(Matrix4 projectionMatrix, Matrix4 viewMatrix,
                        Matrix4 invProjectionMatrix) {
                this.projectionMatrix = projectionMatrix;
                this.viewMatrix = viewMatrix;
                this.invProjectionMatrix = invProjectionMatrix;
        }

        public void addShadowMatrix(ShadowMap map) {
                shadowMaps.add(map);
        }

        public Matrix4 getProjectionMatrix() {
                return new Matrix4(projectionMatrix);
        }

        public Matrix4 getViewMatrix() {
                return new Matrix4(viewMatrix);
        }

        public FrameBufferObject getFBO() {
                return fbo;
        }

        public Matrix4 getInvProjectionMatrix() {
                return invProjectionMatrix;
        }

        public FBOPoolManager getFBOPool() {
        	return fboPool;
        }

		public void finalizeLoop() {
			fboPool.clearAll();
			
		}
		
		public Texture getNoiseTexture() {
			return noise;
		}

		
}