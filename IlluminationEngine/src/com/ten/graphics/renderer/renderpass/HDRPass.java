package com.ten.graphics.renderer.renderpass;

import org.apache.log4j.Logger;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL21;
import org.lwjgl.opengl.GL31;
import org.lwjgl.opengl.GL32;

import com.ten.SettingsTable;
import com.ten.graphics.Configeration;
import com.ten.graphics.GraphicsEngine;
import com.ten.graphics.framebuffer.FBOAttachment;
import com.ten.graphics.framebuffer.FrameBufferObject;
import com.ten.graphics.framebuffer.TextureType;
import com.ten.graphics.renderer.FBOPoolManager;
import com.ten.graphics.renderer.Quad;
import com.ten.graphics.shader.Shader;
import com.ten.graphics.shader.ShaderManager;
import com.ten.graphics.shader.uniform.UniformInt;
import com.ten.graphics.shader.uniform.UniformVector2;

import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.*;
import static org.lwjgl.opengl.GL14.*;
import static org.lwjgl.opengl.GL30.*;

public class HDRPass {
	private static final Logger logger = Logger.getLogger(HDRPass.class
			.getName());
	private FrameBufferObject luminanceBuffer;
	private SettingsTable<Object> settings;
	private static final int FRAMES_TO_WAIT = 120;
	private int expPointerOld;
	private int frameCounter = 0;

	public HDRPass() {
		settings = new SettingsTable<Object>(Configeration.ENABLED);
		GraphicsEngine.addSetting("HDR", settings);
		luminanceBuffer = new FrameBufferObject(GraphicsEngine.DISPLAY_WIDTH, GraphicsEngine.DISPLAY_HEIGHT, 16);
		luminanceBuffer.addRenderTarget(TextureType.R16F, FBOAttachment.COLOR0);
	}

	public void doPass(GBuffer gbuffer) {
		if (settings.getState() == Configeration.ENABLED) {
			FBOPoolManager fboPool = gbuffer.getFBOPool();

			FrameBufferObject inFbo = fboPool.getCurrentFBO();
			
			luminanceBuffer.useFBO();
			inFbo.bindTexture(FBOAttachment.COLOR0, GL_TEXTURE0);
			Shader luminance = ShaderManager.getShader("HDRLuminance");
			luminance.useProgram();
			luminance.setSampler("inColor", 0);
			Quad.getInstance().draw();
			
			glReadBuffer(GL_COLOR_ATTACHMENT0);
			int expPointer = glGenTextures();
			glBindTexture(GL_TEXTURE_2D, expPointer);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_R16F, 0, 0, GraphicsEngine.DISPLAY_WIDTH, GraphicsEngine.DISPLAY_HEIGHT, 0);
				glGenerateMipmap(GL_TEXTURE_2D);
				double log = Math.log(Math.max(GraphicsEngine.DISPLAY_WIDTH, GraphicsEngine.DISPLAY_HEIGHT))/Math.log(2);
				int level = (int) Math.floor(log);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, level);
			glBindTexture(GL_TEXTURE_2D, 0);
			
			if (frameCounter == 0) {
		//		glDeleteTextures(expPointerOld);
				expPointerOld = expPointer;
			}
			
			FrameBufferObject outFbo = fboPool.getFbo();
			outFbo.useFBO();
			
			
			
			
			Shader exposure = ShaderManager.getShader("HDRExposure");
			exposure.useProgram();
			//luminanceBuffer.bindTexture(FBOAttachment.COLOR0, GL_TEXTURE1);
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, expPointer);
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, expPointerOld);
			inFbo.bindTexture(FBOAttachment.COLOR0, GL_TEXTURE0);
			
			
			exposure.setSampler("inColor", 0);
			exposure.setSampler("luminance", 1);
			exposure.setSampler("luminanceOld", 2);
			exposure.setUniform("frame", new UniformInt(frameCounter));
			exposure.setUniform("maxFrame", new UniformInt(FRAMES_TO_WAIT));
			Quad.getInstance().draw();

			fboPool.addAndClearFbo(inFbo);
			
			fboPool.setCurrentFBO(outFbo);
			if (frameCounter != 0) {
				glDeleteTextures(expPointer);
			}
			frameCounter++;
			if (frameCounter > FRAMES_TO_WAIT) {
				glDeleteTextures(expPointerOld);
				frameCounter = 0;
			}
		}
		GraphicsEngine.checkGLError(logger);
	}

}
