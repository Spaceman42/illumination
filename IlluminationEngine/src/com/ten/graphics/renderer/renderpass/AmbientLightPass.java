package com.ten.graphics.renderer.renderpass;

import com.ten.graphics.framebuffer.FBOAttachment;
import com.ten.graphics.framebuffer.FrameBufferObject;
import com.ten.graphics.renderer.FBOPoolManager;
import com.ten.graphics.renderer.Quad;
import com.ten.graphics.shader.Shader;
import com.ten.graphics.shader.ShaderManager;

import static org.lwjgl.opengl.GL13.*;

public class AmbientLightPass {
	
	public AmbientLightPass() {
		
	}
	
	public void doPass(GBuffer gbuffer) {
		FBOPoolManager fboPool = gbuffer.getFBOPool();
		
		FrameBufferObject gFbo = gbuffer.getFBO();
		FrameBufferObject rFbo = fboPool.getCurrentFBO();
		FrameBufferObject wFbo = fboPool.getFbo();
		
		gFbo.bindTexture(FBOAttachment.COLOR0, GL_TEXTURE1);
		gFbo.bindTexture(FBOAttachment.COLOR1, GL_TEXTURE2);
		rFbo.bindTexture(FBOAttachment.COLOR0, GL_TEXTURE3);
		wFbo.useFBO();
		
		Shader shader = ShaderManager.getShader("AmbientLight");
		shader.useProgram();
		shader.setSampler("gColor", 1);
		shader.setSampler("gNormal", 2);
		shader.setSampler("inColor", 3);
		Quad.getInstance().draw();
		
		fboPool.addAndClearFbo(rFbo);
		fboPool.setCurrentFBO(wFbo);
	}
}
