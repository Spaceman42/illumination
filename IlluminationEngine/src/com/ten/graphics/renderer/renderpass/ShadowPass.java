package com.ten.graphics.renderer.renderpass;

import java.util.List;
import java.util.Set;

import com.ten.graphics.RenderableObject;
import com.ten.graphics.framebuffer.FBOAttachment;
import com.ten.graphics.framebuffer.FrameBufferObject;
import com.ten.graphics.renderer.FBOPoolManager;
import com.ten.graphics.renderer.Quad;
import com.ten.graphics.renderer.light.LightRegistry;
import com.ten.graphics.renderer.light.ShadowLight;
import com.ten.graphics.shader.Shader;
import com.ten.graphics.shader.ShaderManager;
import com.ten.graphics.shader.uniform.UniformMatrix4;
import com.ten.graphics.shader.uniform.UniformNotFoundException;
import com.ten.graphics.shader.uniform.UniformVector3;
import com.ten.graphics.shader.uniform.UniformVector4;
import com.ten.io.keyboard.IOKeyboard;
import com.ten.io.keyboard.Key;
import com.ten.math.Matrix4;

import static org.lwjgl.opengl.GL13.*;

public class ShadowPass {
	public void doPass(GBuffer gbuffer, List<RenderableObject> objects)
			throws UniformNotFoundException {
		Set<ShadowLight> lights = LightRegistry.getShadowLights();
		if (lights.size() > 0) {
			FrameBufferObject gFBO = gbuffer.getFBO();
			FBOPoolManager fboPool = gbuffer.getFBOPool();
			FrameBufferObject rFBO = fboPool.getCurrentFBO();
			FrameBufferObject wFBO = fboPool.getFbo();

			for (ShadowLight l : lights) {
				if (IOKeyboard.isKeyDown(Key.KEY_G)) {
					l.setLMat(gbuffer.getViewMatrix());
				}
				l.mapShadows(objects);
				wFBO.useFBO();
				l.bindShadowTexture(GL_TEXTURE1);
				gFBO.bindTexture(FBOAttachment.DEPTH, GL_TEXTURE2);
				gFBO.bindTexture(FBOAttachment.COLOR1, GL_TEXTURE3);
				rFBO.bindTexture(FBOAttachment.COLOR0, GL_TEXTURE4);
				gFBO.bindTexture(FBOAttachment.COLOR0, GL_TEXTURE5);
				// System.out.println("lMat: " + l.getlMat());
				Shader shader = ShaderManager.getShader("ShadowDraw");
				shader.useProgram();
				shader.setSampler("uShadow", 1);
				shader.setSampler("gDepth", 2);
				shader.setSampler("gNormal", 3);
				shader.setSampler("uColor", 4);
				shader.setSampler("gColor", 5);
				shader.setUniform("uBias", 0f);
				shader.setUniform("frustrumData",
						new UniformVector4(gbuffer.getFrustrumData()));
				shader.setUniform("lMat", new UniformMatrix4(l.getlMat()));
				Matrix4 viewMatInv = new Matrix4(gbuffer.getViewMatrix())
						.inverse();
				shader.setUniform("invVMat", new UniformMatrix4(viewMatInv));
				shader.setUniform("vMat",
						new UniformMatrix4(gbuffer.getViewMatrix()));
				shader.setUniform("lightLocation",
						new UniformVector3(l.getLocation()));
				 shader.setUniform("lightIntensity", l.getIntensity());
				Quad.getInstance().draw();
				l.clearDepth();
			}
			FrameBufferObject.unbindFBO();
			fboPool.setCurrentFBO(wFBO);
			fboPool.addAndClearFbo(rFBO);
		}
	}
}
