package com.ten.graphics.renderer.renderpass;

import com.ten.SettingsTable;
import com.ten.graphics.Configeration;
import com.ten.graphics.GraphicsEngine;
import com.ten.graphics.framebuffer.FBOAttachment;
import com.ten.graphics.framebuffer.FrameBufferObject;
import com.ten.graphics.renderer.Cube;
import com.ten.graphics.renderer.FBOPoolManager;
import com.ten.graphics.renderer.Quad;
import com.ten.graphics.shader.Shader;
import com.ten.graphics.shader.ShaderManager;
import com.ten.graphics.shader.uniform.UniformVector2;

import static org.lwjgl.opengl.GL13.*;

public class FXAAPass {
	
	private SettingsTable<Object> settings;

	public FXAAPass() {
		settings = new SettingsTable<Object>(Configeration.ENABLED);
		settings.setSetting("samples", 4); //20
		GraphicsEngine.addSetting("FXAA", settings);
	}
	
	public void doPass(GBuffer gbuffer) {
		if (settings.getState() == Configeration.ENABLED) {
			FBOPoolManager fboPool = gbuffer.getFBOPool();
			FrameBufferObject rfbo = fboPool.getCurrentFBO();
			FrameBufferObject wfbo = fboPool.getFbo();
			
			wfbo.useFBO();
			
			Shader shader = ShaderManager.getShader("FXAA");
			shader.useProgram();
			rfbo.bindTexture(FBOAttachment.COLOR0, GL_TEXTURE15);
			shader.setSampler("gColor", 15);
			shader.setUniform("samples", (int)settings.getSetting("samples"));
			shader.setUniform("buffersize", new UniformVector2(GraphicsEngine.DISPLAY_WIDTH, GraphicsEngine.DISPLAY_HEIGHT));
			Quad.getInstance().draw();
			
			
			fboPool.addAndClearFbo(rfbo);
			fboPool.setCurrentFBO(wfbo);
		}
	}
}
