package com.ten.graphics.renderer.renderpass;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.ten.SettingsTable;
import com.ten.graphics.Configeration;
import com.ten.graphics.GraphicsEngine;
import com.ten.graphics.framebuffer.FBOAttachment;
import com.ten.graphics.framebuffer.FrameBufferObject;
import com.ten.graphics.renderer.FBOPoolManager;
import com.ten.graphics.renderer.Quad;
import com.ten.graphics.shader.Shader;
import com.ten.graphics.shader.ShaderManager;
import com.ten.graphics.shader.uniform.UniformMatrix4;
import com.ten.graphics.shader.uniform.UniformNotFoundException;
import com.ten.graphics.shader.uniform.UniformVector2;
import com.ten.graphics.shader.uniform.UniformVector4;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;

public class SSAOPass {
	private SettingsTable<Object> settings;
	
	private static final Logger logger = Logger.getLogger(SSAOPass.class
			.getName());
	
	public SSAOPass() {
		settings = new SettingsTable<Object>(Configeration.ENABLED);
		settings.setSetting("radius", 4f); //20
		settings.setSetting("intensity", 120f); //100
		settings.setSetting("scale", (1f/0.003f)); //0.1
		GraphicsEngine.addSetting("SSAO", settings);
	}

	public void doPass(GBuffer gBuffer)
			throws UniformNotFoundException {
		Quad quad = Quad.getInstance();
		
		glDisable(GL_DEPTH_TEST);
		FrameBufferObject gFbo = gBuffer.getFBO();
		FBOPoolManager fboPool = gBuffer.getFBOPool();
		
		FrameBufferObject readFbo = fboPool.getCurrentFBO();

		FrameBufferObject writeFbo = fboPool.getFbo();
		writeFbo.useFBO();
		
		
		gFbo.bindTexture(FBOAttachment.DEPTH, GL_TEXTURE6);
		gFbo.bindTexture(FBOAttachment.COLOR1, GL_TEXTURE5);
		gBuffer.getNoiseTexture().useTexture(GL_TEXTURE7);
		GraphicsEngine.checkGLError(logger);
		Shader aoShader = ShaderManager.getShader("SSAO");
		aoShader.useProgram();
		aoShader.setSampler("gDepth", 6);
		aoShader.setSampler("uRandom", 7);
		aoShader.setSampler("gNormal", 5);
		aoShader.setUniform("samples", 16);
		aoShader.setUniform("noiseScale", new UniformVector2(64f, 64f));
		aoShader.setUniform("frustrumData", new UniformVector4(gBuffer.getFrustrumData()));
		aoShader.setUniform("invPMat", new UniformMatrix4(gBuffer.getInvProjectionMatrix()));
		aoShader.setUniform("uRadius", (float) settings.getSetting("radius"));
		aoShader.setUniform("intensity",(float) settings.getSetting("intensity"));
		aoShader.setUniform("bias", -0.4f); //-0.7
		aoShader.setUniform("scale",(float) settings.getSetting("scale"));
		GraphicsEngine.checkGLError(logger);
		aoShader.setUniform("buffersize", new UniformVector2(GraphicsEngine.DISPLAY_WIDTH, GraphicsEngine.DISPLAY_HEIGHT));
		quad.draw();
		GraphicsEngine.checkGLError(logger);
		FrameBufferObject.unbindFBO();
		
		//readFbo.clearFBO();
		//readFbo.useFBO();
		FrameBufferObject finalFbo = fboPool.getFbo();
		finalFbo.useFBO();
		
		Shader blurShader = ShaderManager.getShader("AOBlur");
		blurShader.useProgram();
		blurShader.setUniform("buffersize", new UniformVector2(GraphicsEngine.DISPLAY_WIDTH, GraphicsEngine.DISPLAY_HEIGHT));
		readFbo.bindTexture(FBOAttachment.COLOR0, GL_TEXTURE12);
		blurShader.setSampler("gColor", 12);
		writeFbo.bindTexture(FBOAttachment.COLOR0, GL_TEXTURE8);
		blurShader.setSampler("ao", 8);
		quad.draw();
		GraphicsEngine.checkGLError(logger);  
		FrameBufferObject.unbindFBO();
		fboPool.addAndClearFbo(writeFbo);
		fboPool.addAndClearFbo(readFbo);
		fboPool.setCurrentFBO(finalFbo);
		glEnable(GL_DEPTH_TEST);
	}
}
