package com.ten.graphics.renderer;

import static org.lwjgl.opengl.GL11.*;

import java.util.List;

import org.apache.log4j.Logger;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;

import com.ten.game.DebugCamera;
import com.ten.graphics.Configeration;
import com.ten.graphics.GraphicsEngine;
import com.ten.graphics.RenderableObject;
import com.ten.graphics.renderer.light.PointLight;
import com.ten.graphics.renderer.light.ShadowLight;
import com.ten.graphics.renderer.renderpass.AmbientLightPass;
import com.ten.graphics.renderer.renderpass.BloomPass;
import com.ten.graphics.renderer.renderpass.BloomPass2;
import com.ten.graphics.renderer.renderpass.FXAAPass;
import com.ten.graphics.renderer.renderpass.FinalPass;
import com.ten.graphics.renderer.renderpass.GBuffer;
import com.ten.graphics.renderer.renderpass.GeometryPass;
import com.ten.graphics.renderer.renderpass.HDRPass;
import com.ten.graphics.renderer.renderpass.LightPass;
import com.ten.graphics.renderer.renderpass.MouseInputPass;
import com.ten.graphics.renderer.renderpass.SSAOPass;
import com.ten.graphics.renderer.renderpass.ShadowPass;
import com.ten.graphics.shader.ShaderManager;
import com.ten.graphics.shader.uniform.UniformNotFoundException;
import com.ten.math.Matrix4;
import com.ten.math.Vector3;
import com.ten.math.Vector4;

public class DeferredRenderer implements Renderer {

	private static final Logger logger = Logger
			.getLogger(DeferredRenderer.class.getName());

	private GBuffer gbuffer;

	
	private SSAOPass ssaoPass;
	private GeometryPass geometryPass;
	private LightPass lightPass;
	private ShadowPass shadowPass;
	private BloomPass2 bloomPass2;
	private AmbientLightPass ambientPass;
	private FXAAPass fxaaPass;
	private HDRPass hdrPass;
	
	private MouseInputPass mousePass;
	
	public DeferredRenderer() {
		ssaoPass = new SSAOPass();
		geometryPass = new GeometryPass();
		lightPass = new LightPass();
		shadowPass = new ShadowPass();
		bloomPass2 = new BloomPass2();
		mousePass = new MouseInputPass();
		ambientPass = new AmbientLightPass();
		fxaaPass = new FXAAPass();
		hdrPass = new HDRPass();
	}
	@Override
	public double render(List<RenderableObject> objects, DebugCamera camera, Matrix4 projectionMatrix) {
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		ShaderManager.getShader("GeometryPass").useProgram();
		if (camera != null) {
			gbuffer.matrixData(new Matrix4(projectionMatrix),
					camera.getViewMatrix(), camera.getInverseProjectionMatrix());
			gbuffer.frustrumData(camera.getFrustrumData());
			try {
				//glViewport(0, 0, GraphicsEngine.DISPLAY_WIDTH, GraphicsEngine.DISPLAY_HEIGHT);
				geometryPass.doPass(objects, gbuffer);
				mousePass.doPass(gbuffer, camera);
				lightPass.doPass(gbuffer);
				
				ambientPass.doPass(gbuffer);
				
				shadowPass.doPass(gbuffer, objects);
				
			
				//bloomPass.doPass(gbuffer);
				
				if (GraphicsEngine.readSetting("SSAO").getState() == Configeration.ENABLED) {
					ssaoPass.doPass(gbuffer);
				}
			//	fxaaPass.doPass(gbuffer);
				
				hdrPass.doPass(gbuffer);
				bloomPass2.doPass(gbuffer);
				FinalPass.doPass(gbuffer);
			} catch (UniformNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		gbuffer.finalizeLoop();
		return 0;
	}

	@Override
	public void init() {
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glEnable(GL_DITHER);
		glPolygonMode(GL_FRONT, GL_FILL);
		gbuffer = new GBuffer();
		//new ShadowLight(4000, new Vector3(1,1,1), 20f, new Vector3(0,0,100), new Vector3(3.14f/2,0,0f));
		logger.info("Deferred Renderer Initialized");
	}
	@Override
	public void disableDepthTest() {
		glDisable(GL_DEPTH_TEST);
		
	}
}
