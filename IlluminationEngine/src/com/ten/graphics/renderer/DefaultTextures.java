package com.ten.graphics.renderer;

import com.ten.graphics.renderer.model.Texture;

public class DefaultTextures {
	private static Texture noimage;
	private static Texture randomNoise;
	
	private DefaultTextures() {}
	
	public static Texture getNoImageTex() {
		if (noimage == null) {
			noimage = Texture.loadTexture("res/textures/noimage.png");
		}
		return noimage;
	}
	
	public static Texture getRandomNoiseTex() {
		if (randomNoise == null) {
			randomNoise = Texture.loadTexture("res/textures/randomNoise.png");
		}
		return randomNoise;
	}
}
