package com.ten.graphics.renderer.model;

import java.util.List;
import java.util.Map;

import com.ten.asset.Material;
import com.ten.graphics.renderer.light.Light;
import com.ten.graphics.shader.Shader;
import com.ten.physics.shape.BoundingShape;
import com.ten.physics.shape.ConvexShapeConstructionInfo;
import com.ten.physics.shape.ShapeConstructionInfo;
import com.ten.request.IOCompoundModelLoadRequst;

public class CompoundModel extends Model{
	private Model2[] models;
	private BoundingShape shape;
	
	private boolean loaded = false;
	
	
	@Override
	public void draw(Shader shader, int lod) {
		if (loaded) {
			for (Model2 m : models) {
				m.draw(shader);
			}
		}
	}

	public void update(Model2[] models, BoundingShape shape) {
		this.models = models;
		this.shape = shape;
		loaded = true;
	}
	@Override
	public BoundingShape[] getBBs() {
		return new BoundingShape[] {shape};
	}
	
	@Override
	public boolean loaded() {
		return loaded;
	}

	public static CompoundModel load(String modelLocation, String mtlLocation, ShapeConstructionInfo shape, float localScale) {
		CompoundModel model = new CompoundModel();
		new IOCompoundModelLoadRequst(modelLocation, mtlLocation, shape, localScale, model).make();
		return model;
	}

}
