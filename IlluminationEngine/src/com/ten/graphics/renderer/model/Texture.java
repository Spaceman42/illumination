package com.ten.graphics.renderer.model;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL13.glActiveTexture;

import java.io.File;
import java.nio.ByteBuffer;

import org.apache.log4j.Logger;

import com.ten.ThreadManager;
import com.ten.graphics.renderer.DefaultTextures;
import com.ten.request.GLTextureDeleteRequest;
import com.ten.request.GLTextureLoadRequest;
import com.ten.request.IOTextureLoadRequest;

public class Texture {
	private static final Logger logger = Logger.getLogger(Texture.class.getName());

	private int textureId = -1;
	private int width;
	private int height;
	private ByteBuffer data;

	private final Object lock;

	private Texture() {
		lock = new Object();
	}

	public static Texture loadTexture(String location) {
		Texture texture = null;
		
		File file = new File(location);
		
		if (file.exists()) {
			texture = new Texture();
			ThreadManager.getInstance().makeIoRequest(
					new IOTextureLoadRequest(texture, file));
		}else {
			logger.info("Couldn't find texture at: " + location);
			texture = DefaultTextures.getNoImageTex();
		}
		
		return texture;
	}
	
	public static Texture createTexture(int width, int height, ByteBuffer data) {
		Texture tex = new Texture();
		new GLTextureLoadRequest(tex, data, width, height).make();
		return tex;
	}

	public void update(int id, int width, int height, ByteBuffer data) {
		synchronized (lock) {
			this.textureId = id;
			this.width = width;
			this.height = height;
			this.data = data;
		}
	}

	public void delete() {
		synchronized (lock) {
			ThreadManager.getInstance().makeGLRequest(
					new GLTextureDeleteRequest(this));
			textureId = -1;
		}
	}

	public int getTextureId() {
		return textureId;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public ByteBuffer getData() {
		return data;
	}
	
	public void useTexture(int activeTexture) {
		if (textureId != 0) {
			glActiveTexture(activeTexture);
			glBindTexture(GL_TEXTURE_2D, textureId);
		}
	}
}
