package com.ten.graphics.renderer.model;

import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;

import java.io.File;

import org.apache.log4j.Logger;
import org.lwjgl.opengl.GL11;

import com.ten.asset.binaryasset.AssetFile;
import com.ten.asset.binaryasset.ModelChunk;
import com.ten.physics.shape.BoundingShape;
import com.ten.physics.shape.ShapeConstructionInfo;
import com.ten.request.GLModelMeshLoadRequest;

public class ModelMesh {
	private static final Logger logger = Logger.getLogger(ModelMesh.class
			.getName());

	private int vertexArrayId;
	private int indicesId = -1;
	private int verticesId;
	private int textureCoordsId;
	private int normalsId;
	private int indicesNum;
	
	private BoundingShape shape;

	private boolean loaded;

	private ModelMesh(BoundingShape shape) {
		this.shape = shape;
	}

	/**
	 * 
	 * @param location
	 * @param shapeInfo
	 *            shape info. If null no shape will be created
	 * @return
	 */
	public static ModelMesh loadBinary(String location,
			ShapeConstructionInfo shapeInfo) {
		AssetFile assetFile = AssetFile.load(new File(location));
		ModelChunk chunk = (ModelChunk) assetFile.findChunk("mesh");
		BoundingShape shape = null;
		if (shapeInfo != null) {
			shape = shapeInfo.constructShape(chunk.getVertices(), chunk.getIndices());
		}
		ModelMesh mesh = new ModelMesh(shape);
		new GLModelMeshLoadRequest(mesh, chunk.getVertices(), chunk.getTextureCoordinates(), chunk.getNormals(), chunk.getIndices()).make();
		return mesh;
	}
	
	public BoundingShape getShape() {
		return shape;
	}
	
	public void update(int vaoId, int indicesId, int verticesId, int textureCoordsId, int normalsId, int indicesAmount) {

		this.vertexArrayId = vaoId;
		this.indicesId = indicesId;
		this.verticesId = verticesId;
		this.textureCoordsId = textureCoordsId;
		this.normalsId = normalsId;
		this.indicesNum = indicesAmount;

		loaded = true;
		logger.info("Mesh data recived");
	}

	public void draw() {
		if (loaded) {
			glBindVertexArray(vertexArrayId);
			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);
			if (textureCoordsId != -1) {
				glEnableVertexAttribArray(2);
			}
			logger.info("IndicesID: " + indicesId);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesId);
			GL11.glDrawElements(GL11.GL_TRIANGLES, indicesNum,
					GL11.GL_UNSIGNED_INT, 0);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			glDisableVertexAttribArray(0);
			glDisableVertexAttribArray(1);
			if (textureCoordsId != -1) {
				glDisableVertexAttribArray(2);
			}
			glBindVertexArray(0);
		}
	}

	public void delete() {
	
	}
}
