package com.ten.graphics.renderer.model;

import com.ten.asset.Material;
import com.ten.graphics.shader.Shader;
import com.ten.physics.shape.BoundingShape;
import com.ten.physics.shape.ShapeConstructionInfo;
import com.ten.request.IOModelLoadRequest;

public class Model2 {
	
	private static final int GL_TEXTURE0 = 33984; //Should use LWJGL stuff, will make enum later
	private static final int GL_TEXTURE1 = 33985;
	private static final int GL_TEXTURE2 = 33986;
	
	private Mesh2 mesh;
	private Material material;
	
	public Model2(Mesh2 mesh, Material material) {
		this.mesh = mesh;
		this.material = material;
	}
	
	public void draw(Shader shader) {
		if (shader != null) {
			material.getColor().useTexture(GL_TEXTURE0);
			shader.setSampler("texture", 0);
			if (material.hasNormal()) {
				material.getNormal().useTexture(GL_TEXTURE1);
				shader.setSampler("normalMap", 1);
				shader.setUniform("hasNormalMap", 1);
			} else {
				shader.setUniform("hasNormalMap", 0);
			}
			if (material.hasSpec()) {
				material.getSpecular().useTexture(GL_TEXTURE2);
				shader.setUniform("specOverride", -1f);
				shader.setSampler("specMap", 2);
			} else {
				shader.setUniform("specOverride", material.getSpecValue());
			}
		}
		mesh.draw();
	}
}
