package com.ten.graphics.renderer.model;

import java.util.HashMap;
import java.util.Map;

public class ModelRegisty {
	private static Map<String, Model> registry = new HashMap<String, Model>();
	private ModelRegisty() {};
	
	public static Model getModel(String location) {
		Model model = registry.get(location);
		if (model == null) {
			model = Model.loadModel(location);
			registry.put(location, model);
		}
		return model;
	}
	
	public static void returnModel(String modelLocation) {
		registry.remove(modelLocation);
	}
}
