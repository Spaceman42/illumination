package com.ten.graphics.renderer.model;

import com.ten.graphics.shader.Shader;
import com.ten.physics.shape.BoundingBox;
import com.ten.physics.shape.BoundingShape;

public class LOD {
	
	private Mesh mesh;
	private Texture texture;
	private Texture normalMap;
	private Texture specularMap;
	
	public LOD(Mesh mesh, Texture texture, Texture normalMap, Texture specularMap) {
		this.mesh = mesh;
		this.texture = texture;
		this.normalMap = normalMap;
		this.specularMap = specularMap;
	}
		
	public LOD(LOD lod) {
		mesh = new Mesh(lod.mesh);
		//TODO: finish this and lower level copy methods (Maybe copying isn't needed)
	}

	public void draw(Shader shader) {
		//TODO: bind textures
		mesh.draw();
	}
	
	public void delete() {
		mesh.delete();
		texture.delete();
		normalMap.delete();
		specularMap.delete();
	}

	public BoundingShape getBB() {
		return mesh.getBB();
	}

	public Mesh getMesh() {
		return mesh;
	}
}
