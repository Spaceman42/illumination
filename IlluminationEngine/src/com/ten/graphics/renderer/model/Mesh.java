package com.ten.graphics.renderer.model;

import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;

import org.apache.log4j.Logger;
import org.lwjgl.opengl.GL11;

import com.ten.physics.shape.BoundingBox;
import com.ten.physics.shape.BoundingShape;
import com.ten.physics.shape.ConvexShapeConstructionInfo;
import com.ten.request.IOMeshLoadRequest;

public class Mesh {

	private static final Logger logger = Logger.getLogger(Mesh.class.getName());

	private int vertexArrayId;
	private int indicesId = -1;
	private int verticesId;
	private int textureCoordsId;
	private int normalsId;
	private int indicesNum;
	private BoundingShape bb;

	public boolean loaded = false;

	public Mesh() {
	}
	
	public static Mesh loadMesh(String meshLocation) {
		Mesh mesh = new Mesh();
		new IOMeshLoadRequest(meshLocation, mesh, new ConvexShapeConstructionInfo());
		return mesh;
	}

	public Mesh(Mesh mesh) {
		this.loaded = mesh.loaded;

		this.vertexArrayId = mesh.vertexArrayId;
		this.indicesId = mesh.indicesId;
		this.verticesId = mesh.verticesId;
		this.textureCoordsId = mesh.textureCoordsId;
		this.normalsId = mesh.normalsId;
		this.indicesNum = mesh.indicesNum;
		this.bb = mesh.bb;
		
	}

	public void update(int vaoId, int indicesId, int verticesId, int textureCoordsId, int normalsId, int indicesAmount, BoundingShape boundingBox) {

		this.vertexArrayId = vaoId;
		this.indicesId = indicesId;
		this.verticesId = verticesId;
		this.textureCoordsId = textureCoordsId;
		this.normalsId = normalsId;
		this.indicesNum = indicesAmount;
		this.bb = boundingBox;

		loaded = true;
		logger.info("Mesh data recived");
	}

	public void draw() {
		if (loaded) {
			glBindVertexArray(vertexArrayId);
			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);
			if (textureCoordsId != -1) {
				glEnableVertexAttribArray(2);
			}
			logger.info("IndicesID: " + indicesId);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesId);
			GL11.glDrawElements(GL11.GL_TRIANGLES, indicesNum, GL11.GL_UNSIGNED_INT, 0);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			glDisableVertexAttribArray(0);
			glDisableVertexAttribArray(1);
			if (textureCoordsId != -1) {
				glDisableVertexAttribArray(2);
			}
			glBindVertexArray(0);
		}
	}

	public void delete() {
		glDeleteVertexArrays(vertexArrayId);
		//Add more deletes to here
	}

	public BoundingShape getBB() {
		return bb;
	}
}
