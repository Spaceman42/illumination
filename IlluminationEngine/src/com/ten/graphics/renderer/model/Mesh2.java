package com.ten.graphics.renderer.model;

import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL30.glBindVertexArray;

import org.apache.log4j.Logger;
import org.lwjgl.opengl.GL11;

public class Mesh2 {

	private int vertexArrayId;
	private int indicesId;
	private int verticesId;
	private int normalsId;
	private int textureCoordsId;
	private int indicesNum;
	private int tansId;
	private int bitansId;
	
	private String id;
	private boolean loaded = false;
	private static final Logger logger = Logger.getLogger(Mesh2.class.getName());

	public Mesh2(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}

	public void update(int vao, int indicesID, int verticesID, int texCoordsID,
			int normalsID, int tansID, int bitansID, int indicesAmount) {

		this.vertexArrayId = vao;
		this.indicesId = indicesID;
		this.verticesId = verticesID;
		this.textureCoordsId = texCoordsID;
		this.normalsId = normalsID;
		this.indicesNum = indicesAmount;
		this.tansId = tansID;
		this.bitansId = bitansID;
		loaded  = true;
		
	}
	
	public void draw() {
		if (loaded) {
			glBindVertexArray(vertexArrayId);
			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);
			if (textureCoordsId != -1) {
				glEnableVertexAttribArray(2);
				glEnableVertexAttribArray(3);
				glEnableVertexAttribArray(4);
			}
			//logger.info("IndicesID: " + indicesId);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesId);
			GL11.glDrawElements(GL11.GL_TRIANGLES, indicesNum, GL11.GL_UNSIGNED_INT, 0);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			glDisableVertexAttribArray(0);
			glDisableVertexAttribArray(1);
			if (textureCoordsId != -1) {
				glDisableVertexAttribArray(2);
				glDisableVertexAttribArray(3);
				glDisableVertexAttribArray(4);
			}
			glBindVertexArray(0);
		}
	}

}
