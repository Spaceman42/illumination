package com.ten.graphics.renderer.model;

import org.apache.log4j.Logger;

import com.ten.ThreadManager;
import com.ten.graphics.shader.Shader;
import com.ten.physics.shape.BoundingBox;
import com.ten.physics.shape.BoundingShape;
import com.ten.physics.shape.ConvexShapeConstructionInfo;
import com.ten.request.IOBinaryMeshLoadRequest;
import com.ten.request.IOMeshLoadRequest;
/**
 * 
 * @author Ethan
 * Temporary model class for use testing the openGL pipeline. Drawing LOD should always be 0
 */
public class Model {
	
	private static final int GL_TEXTURE0 = 33984; //Should use LWJGL stuff, will make enum later
	private static final int GL_TEXTURE1 = 33985;
	private LOD[] lods;
	public final int MAX_LOD;
	public final int MIN_LOD = 0;
	
	private Texture texture;
	private Texture specTexture;
	
	Logger logger = Logger.getLogger(Model.class.getName());
	public Model() {
		MAX_LOD = 0;
	}
	private Model(LOD[] lods) {
		this.lods = lods;
		MAX_LOD = lods.length-1;
	}
	
	private Model(LOD[] lods, Texture texture) {
		this.lods = lods;
		MAX_LOD = lods.length-1;
		this.texture = texture;
	}
	
	private Model(LOD[] lods, Texture texture, Texture specTexture) {
		this.lods = lods;
		MAX_LOD = lods.length-1;
		this.texture = texture;
		this.specTexture = specTexture;
	}
	
	public static Model loadModel(String modelLocation) {
		Mesh mesh = new Mesh();
		new IOMeshLoadRequest(modelLocation, mesh, new ConvexShapeConstructionInfo()).make();
		LOD[] lods = new LOD[1];
		lods[0] = new LOD(mesh, null, null, null);
		return new Model(lods);
	}
	
	public static Model loadBinaryModel(String modelLocation) {
		Mesh mesh = new Mesh();
		new IOBinaryMeshLoadRequest(modelLocation, mesh, new ConvexShapeConstructionInfo()).make();
		LOD[] lods = new LOD[1];
		lods[0] = new LOD(mesh, null, null, null);
		return new Model(lods);
	}
	
	public static Model loadBinaryModel(String modelLocation, String textureLocation) {
		Mesh mesh = new Mesh();
		new IOBinaryMeshLoadRequest(modelLocation, mesh, new ConvexShapeConstructionInfo()).make();
		LOD[] lods = new LOD[1];
		lods[0] = new LOD(mesh, null, null, null);
		
		return new Model(lods, Texture.loadTexture(textureLocation));
	}
	
	public static Model loadBinaryModel(String modelLocation, String textureLocation, String specTextureLocation) {
		Mesh mesh = new Mesh();
		new IOBinaryMeshLoadRequest(modelLocation, mesh, new ConvexShapeConstructionInfo()).make();
		LOD[] lods = new LOD[1];
		lods[0] = new LOD(mesh, null, null, null);
		
		return new Model(lods, Texture.loadTexture(textureLocation), Texture.loadTexture(specTextureLocation));
	}
	
	
	public Model(Model model) {
		lods = new LOD[model.lods.length];
		for (int i = 0; i<lods.length; i++) {
			lods[i] = new LOD(model.lods[i]);
		}
		MAX_LOD = model.MAX_LOD;
	}
	
	public void draw(Shader shader, int lod) {
		if (texture != null) {
			texture.useTexture(GL_TEXTURE0);
			shader.setSampler("texure", 0);
		}
		if (specTexture != null) {
			specTexture.useTexture(GL_TEXTURE1);
			shader.setSampler("specular", 1);
			shader.setUniform("specOverride", -1f);
		} else {
			shader.setUniform("specOverride", 1f);
		}
		lods[lod].draw(shader);
	}
	
	public BoundingShape[] getBBs() {
		BoundingShape[] array = new BoundingShape[lods.length];
		for (int i = 0; i<array.length; i++) {
			array[i] = lods[i].getBB();
		}
		return array;
	}
	
	
	public void delete() {
		for (LOD lod : lods) {
			lod.delete();
		}
	}
	
	public boolean loaded() {
		boolean loaded = true;
		for (LOD lod : lods) {
			if (!lod.getMesh().loaded) {
				loaded =  false;
			}
		}
		return loaded;
	}

	public static Model loadModel(String modelLocation, String textureLocation) {
		Mesh mesh = new Mesh();
		new IOMeshLoadRequest(modelLocation, mesh, new ConvexShapeConstructionInfo()).make();
		LOD[] lods = new LOD[1];
		lods[0] = new LOD(mesh, null, null, null);
		
		return new Model(lods, Texture.loadTexture(textureLocation));
	}
}
