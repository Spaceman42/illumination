package com.ten.graphics.renderer.light;

import com.ten.math.Vector3;
import com.ten.math.Vector4;

public class PointLight implements Light {
	private Vector3 location;
	private Vector4 color;
	private float intensity;
	public PointLight(Vector3 location, Vector4 color) {
		this.location = location;
		this.color = color;
		this.intensity = color.w;
		LightRegistry.checkIn(this);
	}

	@Override
	public Vector3 getLocation() {
		return location;
	}

	@Override
	public Vector4 getColor() {
		return color;
	}

	@Override
	public void setLocation(Vector3 location) {
		this.location = location;
	}

	@Override
	public void setColor(Vector4 color) {
		this.color = color;
	}

	@Override
	public float getIntensity() {
		return intensity;
	}

	@Override
	public void setIntensity(float value) {
		this.intensity = value;
		this.color.w = value;
	}

	@Override
	public LightType getType() {
		return LightType.POINT_LIGHT;
	}

	@Override
	public void delete() {
		LightRegistry.removeLight(this);
		
	}
}
