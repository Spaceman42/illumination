package com.ten.graphics.renderer.light;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class LightRegistry {
	private static Set<Light> registry = new HashSet<Light>();
	private static Set<ShadowLight> shadowLights = new HashSet<ShadowLight>();
	
	protected synchronized static void checkIn(Light light) {
		registry.add(light);
	}
	
	protected synchronized static void checkOut(Light light) {
		registry.remove(light);
	}
	
	public synchronized static Light[] getLights() {
		return registry.toArray(new Light[registry.size()]);
	}
	
	public synchronized static void addShadowLight(ShadowLight light) {
		shadowLights.add(light);
	}
	public synchronized static void removeShadowLight(ShadowLight light) {
		shadowLights.remove(light);
	}
	public synchronized static Set<ShadowLight> getShadowLights() {
		return shadowLights;
	}

	public static void removeLight(Light l) {
		registry.remove(l);
		
	}
}
