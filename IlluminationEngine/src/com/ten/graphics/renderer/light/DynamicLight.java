package com.ten.graphics.renderer.light;

import com.ten.math.Matrix4;

public interface DynamicLight extends Light {
	public void updatePosition(Matrix4 transformMat);
}
