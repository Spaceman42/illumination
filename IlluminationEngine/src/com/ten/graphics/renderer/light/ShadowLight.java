package com.ten.graphics.renderer.light;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;

import com.ten.graphics.RenderableObject;
import com.ten.graphics.ShadowMap;
import com.ten.graphics.shader.Shader;
import com.ten.graphics.shader.ShaderManager;
import com.ten.graphics.shader.uniform.UniformNotFoundException;
import com.ten.math.Matrix4;
import com.ten.math.Vector3;
import com.ten.math.Vector4;

public class ShadowLight implements Light {
	private static final Logger logger = Logger.getLogger(Light.class.getName());
	private ShadowMap map;
	private Vector3 position;
	private Vector3 color;
	private Vector3 rotation;
	private float intensity;
	private Matrix4 lMat;
	private static Matrix4 biasMat = new Matrix4(0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 0.5f, 0.0f, 0.5f, 0.5f, 0.5f, 1.0f);

	public ShadowLight(int mapSize, Vector3 color, float intensity, Vector3 position, Vector3 rotation) {
		this.position = new Vector3(position);
		this.color = new Vector3(color);
		try {
			map = new ShadowMap(mapSize, new Shader("res/shaders/ShadowMap", "ShadowMap"));
		} catch (IOException e) {
			e.printStackTrace();
			logger.fatal(e);
			System.exit(-1);
		}
		LightRegistry.addShadowLight(this);
		this.rotation = new Vector3(rotation);
		lMat = new Matrix4();
		lMat.translate(-position.x, -position.y, -position.z);
		lMat.rotate(0, 1, 0, rotation.y);
		lMat.rotate(1, 0, 0, rotation.x);
		lMat.rotate(0, 0, 1, rotation.z);
		this.intensity = intensity;
	}

	@Override
	public Vector3 getLocation() {
		return new Vector3(position);
	}

	public int getSize() {
		return map.getSize();
	}
	
	public void setLMat(Matrix4 mat) {
		lMat = new Matrix4(mat);
		Vector4 temp = new Vector4(0,0,0,1);
		Matrix4 invVMat = new Matrix4(mat).inverse();
		temp.mutiplyByMatrix(invVMat);
		temp.y *= -1;
		position = new Vector3(temp);
		
	}

	public void mapShadows(List<RenderableObject> objects) throws UniformNotFoundException {
	
		
		
		map.mapShadows(lMat, objects);
	}

	public void bindShadowTexture(int glActiveTexture) {
		map.bindShadowMap(glActiveTexture);
	}
	
	public Matrix4 getlMat() {
		Matrix4 pMat = map.getPMat();
		
		Matrix4 mvpMat = new Matrix4(pMat).multiply(lMat);
		return new Matrix4(biasMat).multiply(mvpMat);
	}

	@Override
	public Vector4 getColor() {
		return new Vector4(color.x, color.y, color.z);
	}

	@Override
	public float getIntensity() {
		return intensity;
	}

	@Override
	public void setLocation(Vector3 location) {
		this.position = new Vector3(location);
	}

	@Override
	public void setColor(Vector4 color) {
		this.color = new Vector3(color.x, color.y, color.z);
	}

	@Override
	public void setIntensity(float value) {

	}

	public void clearDepth() {
		map.clear();
	}

	@Override
	public LightType getType() {
		return LightType.SHADOW_LIGHT;
	}
	
	@Override
	public void delete() {
		// TODO Auto-generated method stub
		
	}
}
