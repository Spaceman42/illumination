package com.ten.graphics.renderer.light;

import com.ten.math.Vector3;
import com.ten.math.Vector4;

public interface Light {
	public Vector3 getLocation();
	public Vector4 getColor();
	public float getIntensity();
	public void setLocation(Vector3 location);
	public void setColor(Vector4 color);
	public void setIntensity(float value);
	public void delete();
	public LightType getType();
}
