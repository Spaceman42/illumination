package com.ten.graphics.renderer.light;

import com.ten.math.Matrix4;
import com.ten.math.Vector3;
import com.ten.math.Vector4;

public class DynamicPointLight extends PointLight implements DynamicLight{
	private Vector4 initialLocation;

	public DynamicPointLight(Vector3 location, Vector4 color) {
		super(location, color);
		this.initialLocation = new Vector4(location);
		LightRegistry.checkIn(this);
	}

	@Override
	public void updatePosition(Matrix4 transformMat) {
		Vector4 transform = new Vector4(initialLocation);
		
		transform.mutiplyByMatrix(transformMat);
		setLocation(new Vector3(transform));
	}
	
	

}
