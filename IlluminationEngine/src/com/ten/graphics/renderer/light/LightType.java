package com.ten.graphics.renderer.light;

public enum LightType {
	POINT_LIGHT, SHADOW_LIGHT
}
