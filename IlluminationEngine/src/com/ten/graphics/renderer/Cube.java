package com.ten.graphics.renderer;

import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

public class Cube {
	private static Cube instance;
	
	private int indicesAmount;
	private int vao;
	private int vbo;
	private int indicesID;

	private Cube() {
		float[] vertices = { 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -0.999999f,
				1.0f, 0.999999f, 1.0f, 1.000001f, 1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -0.999999f,
				1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 0.999999f, 1.0f, 1.000001f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f, -1.0f, -1.0f, 1.0f,
				-1.0f, 1.0f, -1.0f, 1.0f };
		int[] indices = { 0, 1, 2, 4, 7, 6, 8, 9, 5, 10, 11, 12, 2, 13, 3, 4, 0, 14, 3, 0, 2, 5, 4, 6, 1, 0, 5, 11, 6, 12, 13, 15, 3, 7, 4, 14 };
		indicesAmount = indices.length;
		vao = glGenVertexArrays();
		vbo = createVertexBuffer(vao, vertices);
		indicesID = createIndicesBuffer(indices);
	}
	
	public static Cube getInstance() {
		if (instance == null) {
			instance = new Cube();
		}
		return instance;
	}

	private int createVertexBuffer(int vaoID, float[] vertices) {
		FloatBuffer buffer = BufferUtils.createFloatBuffer(vertices.length);
		buffer.put(vertices);
		buffer.flip();
		glBindVertexArray(vaoID);
		int vboID = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, vboID);
		glBufferData(GL_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 4, GL11.GL_FLOAT, false, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		buffer = null;

		return vboID;

	}

	private int createIndicesBuffer(int[] indices) {
		IntBuffer buffer = BufferUtils.createIntBuffer(indicesAmount);
		buffer.put(indices);
		buffer.flip();
		int id = glGenBuffers();
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		return id;
	}

	public void draw() {
		glBindVertexArray(vao);
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesID);

		GL11.glDrawElements(GL11.GL_TRIANGLES, indicesAmount, GL11.GL_UNSIGNED_INT, 0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);
	}

	public void delete() {
		// TODO: Make this do something useful
	}
}
