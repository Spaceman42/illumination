package com.ten.graphics.renderer;

import java.util.List;

import com.ten.game.DebugCamera;
import com.ten.graphics.GraphicsEngine;
import com.ten.graphics.RenderableObject;
import com.ten.math.Matrix4;

public interface Renderer {
	
	public double render(List<RenderableObject> objects, DebugCamera camera, Matrix4 projectionMatrix);
	
	public void init();

	public void disableDepthTest();

}
