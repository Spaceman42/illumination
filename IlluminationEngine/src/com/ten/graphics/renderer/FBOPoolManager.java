package com.ten.graphics.renderer;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import org.apache.log4j.Logger;
import org.lwjgl.opengl.GLContext;

import com.ten.graphics.GraphicsEngine;
import com.ten.graphics.framebuffer.FBOAttachment;
import com.ten.graphics.framebuffer.FrameBufferObject;
import com.ten.graphics.framebuffer.TextureType;

import static org.lwjgl.opengl.GL11.*;

public class FBOPoolManager {
	private FrameBufferObject currentFbo;
	private Queue<FrameBufferObject> pool;
	private int size;
	private static final Logger logger = Logger.getLogger(FBOPoolManager.class.getName());

	public FBOPoolManager(int size) {
		this.size = size;
		pool = new ArrayBlockingQueue<FrameBufferObject>(size);

		for (int i = 0; i < size; i++) {
			FrameBufferObject fbo = new FrameBufferObject(GraphicsEngine.DISPLAY_WIDTH, GraphicsEngine.DISPLAY_HEIGHT, 64);
			fbo.addRenderTarget(TextureType.RGBA16F, FBOAttachment.COLOR0);
			fbo.addDepthRenderBuffer();
			if (!pool.add(fbo)) {
				logger.fatal("Could not add a new FBO to the FBO pool");
				System.exit(-1);
			}
			logger.info("Added FBO " + i + " to the pool");
		}
		if (pool.isEmpty()) {
			logger.fatal("Error creating fbo pool");
			System.exit(-1);
		}
	}

	public FrameBufferObject getFbo() {
		return pool.poll();
	}

	public void addAndClearFbo(FrameBufferObject fbo) {
		if (pool.size() < size) {
			fbo.useFBO();
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			FrameBufferObject.unbindFBO();
			pool.add(fbo);
		}
	}

	public FrameBufferObject getCurrentFBO() {
		return currentFbo;
	}

	public void setCurrentFBO(FrameBufferObject fbo) {
		currentFbo = fbo;
	}

	public void clearAll() {
		for (int i = 0; i < pool.size(); i++) {
			FrameBufferObject fbo = pool.poll();
			fbo.useFBO();
			fbo.clearFBO();
			pool.add(fbo);
		}
		FrameBufferObject.unbindFBO();

	}
}
