package com.ten.graphics;

import java.io.IOException;
import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.util.glu.Project;

import com.ten.graphics.framebuffer.DepthTextureType;
import com.ten.graphics.framebuffer.FBOAttachment;
import com.ten.graphics.framebuffer.FrameBufferObject;
import com.ten.graphics.framebuffer.TextureType;
import com.ten.graphics.shader.Shader;
import com.ten.graphics.shader.uniform.UniformMatrix4;
import com.ten.graphics.shader.uniform.UniformNotFoundException;
import com.ten.math.GameMath;
import com.ten.math.Matrix4;
import com.ten.math.Vector3;

public class ShadowMap {
	private FrameBufferObject fbo;
	private Shader shader;
	private int size;
	private Matrix4 pMat;
	private static final float MAPVIEW = 200f;
	
	public ShadowMap(int size, Shader shader) throws IOException {
		fbo = new FrameBufferObject(size, size, 32);
		fbo.addDepthTexture(DepthTextureType.DEPTH32, FBOAttachment.DEPTH);
		//fbo.addRenderTarget(TextureType.RGBA8, FBOAttachment.COLOR0);
		fbo.drawBuffers();
		
		this.size = size;
		this.shader = shader;
		
		pMat = GameMath.orthoMatrix(-15f,1000f, -MAPVIEW, MAPVIEW, MAPVIEW, -MAPVIEW);
		//-26.666f, 26.666f, -15, 15, -0, 100
	}
	/** 
	 * Not fully implemented, woun't work
	 * @param rotation
	 * @param location
	 */
	public void mapShadows(Vector3 rotation, Vector3 location) {
		Matrix4 lMat = new Matrix4();
		
	}
	
	public void mapShadows(Matrix4 lMat, List<RenderableObject> objects) throws UniformNotFoundException {
		GL11.glViewport(0, 0, size, size);
		fbo.useFBO();
		shader.useProgram();
		
		for (RenderableObject o : objects) {
			shader.setUniform("lMat", new UniformMatrix4(lMat));
			shader.setUniform("pMat", new UniformMatrix4(pMat));
			shader.setUniform("mMat", new UniformMatrix4(o.getModelMatrix()));
			
			o.draw(null);
		}
		FrameBufferObject.unbindFBO();
		GL11.glViewport(0, 0, GraphicsEngine.DISPLAY_WIDTH, GraphicsEngine.DISPLAY_HEIGHT);
	}
	public Matrix4 getPMat() {
		return pMat;
	}
	public void bindShadowMap(int glActiveTexture) {
		fbo.bindTexture(FBOAttachment.DEPTH, glActiveTexture);
	}
	public int getSize() {
		return size;
	}
	public void clear() {
		fbo.useFBO();
		fbo.clearFBO();
		FrameBufferObject.unbindFBO();
	}
}
