package com.ten.graphics;

import com.ten.GameEngine;
import com.ten.gui.IlluminationGUI;

public class GLThread extends Thread {
	
	private GraphicsEngine engine;
	
	public GLThread() {
		super("GL-Thread");
		engine = new GraphicsEngine(GameEngine.getGui());
	}
	
	public void run() {
		try {
			engine.run();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		} finally {
		}
	}

	public GraphicsEngine getEngine() {
		return engine;
	}
}
