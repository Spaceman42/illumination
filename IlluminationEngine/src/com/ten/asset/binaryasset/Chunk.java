package com.ten.asset.binaryasset;

import java.nio.ByteBuffer;

//@author David
public abstract class Chunk {

    public static int TEXTURE_CHUNK = 0x11;
    public static int SHADER_CHUNK = 0x13;
    public static int MODEL_CHUNK = 0x12;
    public static int LIGHT_CHUNK = 0x14;
    protected String id;
    protected int type;

    public Chunk(String id, int type) {
        this.id = id;
        this.type = type;
    }

    public String getId() {
        return this.id;
    }

    public int getType() {
        return this.type;
    }

    public abstract void write(StreamOut s);

}
