/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ten.asset.binaryasset;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author David
 */
public class AssetFile {

    public int version = 1;
    public List<Chunk> chunks = new ArrayList();

    public static AssetFile load(File file) {
        AssetFile assets = new AssetFile();
        try {
            StreamIn s = new StreamIn(file);
            s.skip(4);
            assets.version = s.readInt();
            while (s.read() == 0x10) {
                int chunkType = s.read();
                int idLength = s.readInt();
                String id = s.readString(idLength);
                int chunkLength = s.readInt();
                if (chunkType == Chunk.TEXTURE_CHUNK) { //0x11
                    byte[] chunk = new byte[chunkLength];
                    s.read(chunk);
                    ByteBuffer buf = ByteBuffer.wrap(chunk);
                    assets.chunks.add(TextureChunk.load(buf, id));
                } else if (chunkType == Chunk.MODEL_CHUNK) { //0x12
                    byte[] chunk = new byte[chunkLength];
                    s.read(chunk);
                    ByteBuffer buf = ByteBuffer.wrap(chunk);
                    assets.chunks.add(ModelChunk.load(buf, id));
                } else if (chunkType == Chunk.SHADER_CHUNK) { //0x13
                    byte[] chunk = new byte[chunkLength];
                    s.read(chunk);
                    ByteBuffer buf = ByteBuffer.wrap(chunk);
                    assets.chunks.add(ShaderChunk.load(buf, id));
                }
            }
            s.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (StreamEndException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return assets;
    }

    public void save(File file) {
        try {
            StreamOut s = new StreamOut(file);
            s.writeInt(0x2a2a2a2a);
            s.writeInt(version);
            for (Chunk c : chunks) {
                c.write(s);
            }
            s.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AssetFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public AssetFile() {
    }

    private AssetFile(int version) {
        this.version = version;
    }

    public boolean addChunk(Chunk chunk) {
        String id = chunk.id;
        for (Chunk c : chunks) {
            if (c.id.equals(id)) {
                return false;
            }
        }
        chunks.add(chunk);
        return true;
    }

    public void removeChunk(String id) {
        for (int i = 0; i < chunks.size(); i++) {
            if (chunks.get(i).id.equals(id)) {
                chunks.remove(i);
            }
        }
    }

    public Chunk findChunk(String id) {
        for (int i = 0; i < chunks.size(); i++) {
            if (chunks.get(i).id.equals(id)) {
                return chunks.get(i);
            }
        }
        return null;
    }

    public Chunk[] getChunks() {
        Chunk[] array = new Chunk[this.chunks.size()];
        int i = 0;
        for (Chunk c : this.chunks) {
            array[i] = c;
            i++;
        }
        return array;
    }
}
