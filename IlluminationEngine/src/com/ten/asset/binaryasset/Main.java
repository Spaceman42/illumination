package com.ten.asset.binaryasset;

import java.io.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ten.asset.binaryasset.until.BinaryMesh;
import com.ten.asset.binaryasset.until.ModelLoader;
import com.ten.asset.binaryasset.until.ObjModelLoader;

/**
 *
 * @author David
 */
public class Main {

    private static final Logger mainLogger = Logger.getLogger(Main.class.getName());
    private static Scanner in;
    private static File current;
    private static AssetFile openFile;
    private static Thread gui;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean exit = false;
        in = new Scanner(System.in);
        System.out.println("Asset Manager started, ready for input...");
        while (!exit) {
            String command = in.nextLine();
            if ("create".equals(command)) {
                System.out.print("File location: ");
                File file = new File(in.nextLine());
                if (createFile(file)) {
                    current = file;
                    openFile = new AssetFile();
                } else {
                    if (ynDialog("That file already exists.\nUse anyways? (y/n): ")) {
                        clearFile(file);
                        current = file;
                        openFile = new AssetFile();
                    }
                }
            } else if ("open".equals(command)) {
                System.out.print("File location: ");
                File file = new File(in.nextLine());
                if (file.exists() && file.isFile()) {
                    if (openFile(file)) {
                        current = file;
                        openFile = AssetFile.load(file);
                    } else {
                        if (ynDialog("That file is of an invalid type.\nUse anyways? (y/n): ")) {
                            clearFile(file);
                            current = file;
                            openFile = new AssetFile();
                        }
                    }
                } else {
                    System.out.println("Sorry, that is not a valid file.");
                }
            } else if ("save".equals(command)) {
                if (current != null) {
                    saveFile(current);
                } else {
                    System.out.println("No valid file to save to.");
                }
            } else if ("list-chunks".equals(command)) {
                for (Chunk chunk : openFile.chunks) {
                    if (chunk.type == Chunk.SHADER_CHUNK) {
                        System.out.println("Shader Chunk, Id:" + chunk.id);
                    } else if (chunk.type == Chunk.TEXTURE_CHUNK) {
                        System.out.println("Texture Chunk, Id:" + chunk.id);
                    } else if (chunk.type == Chunk.MODEL_CHUNK) {
                        System.out.println("Model Chunk, Id:" + chunk.id);
                    }
                }
            } else if ("current-file".equals(command)) {
                System.out.println(current.getAbsolutePath());
            } else if ("file-version".equals(command)) {
                System.out.println(fileVersion(current));
            } else if ("add-obj".equals(command)) {
                System.out.print("Model location: ");
                String location = in.nextLine();
                System.out.print("Asset Id: ");
                String id = in.nextLine();
                BinaryMesh model = ModelLoader.loadModel(new File(location)).get("default");
                ModelChunk chunk = new ModelChunk(id, model);
                if (openFile.addChunk(chunk)) {
                } else {
                    if (ynDialog("A chunk with the same id already exists.\nReplace? (y/n): ")) {
                        openFile.removeChunk(id);
                        openFile.addChunk(chunk);
                    }
                }
            } else if ("add-texture".equals(command)) {
                System.out.print("Texture location: ");
                String location = in.nextLine();
                System.out.print("Asset Id: ");
                String id = in.nextLine();
                ImageTexture texture = ImageTexture.load(location);
                TextureChunk chunk = texture.createChunk(id);
                if (openFile.addChunk(chunk)) {
                } else {
                    if (ynDialog("A chunk with the same id already exists.\nReplace? (y/n): ")) {
                        openFile.removeChunk(id);
                        openFile.addChunk(chunk);
                    }
                }
            } else if ("add-shader".equals(command)) {
                System.out.print("Vertex Shader location: ");
                String vertLocation = in.nextLine();
                System.out.print("Fragment Shader location: ");
                String fragLocation = in.nextLine();
                System.out.print("Asset Id: ");
                String id = in.nextLine();
                GLSLShader shader = GLSLShader.load(vertLocation, fragLocation);
                ShaderChunk chunk = shader.createChunk(id);
                if (openFile.addChunk(chunk)) {
                } else {
                    if (ynDialog("A chunk with the same id already exists.\nReplace? (y/n): ")) {
                        openFile.removeChunk(id);
                        openFile.addChunk(chunk);
                    }
                }
            } else if ("print-chunk".equals(command)) {
                System.out.print("Asset Id: ");
                String id = in.nextLine();
                Chunk c = openFile.findChunk(id);
                System.out.println(c.toString());
            } else if ("exit".equals(command)) {
                exit = true;
            } else {
                System.out.println("Sorry, that is not a valid command.");
            }
        }
    }

    private static void saveFile(File file) {
        openFile.save(file);
    }

    private static boolean openFile(File file) {
        if (fileVersion(file) <= 0) {
            return false;
        } else {
            return true;
        }
    }

    private static boolean createFile(File file) {
        try {
            if (file.exists()) {
                return false;
            } else {
                file.createNewFile();
                return true;
            }
        } catch (IOException ex) {
            mainLogger.log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private static void clearFile(File file) {
        try {
            StreamOut stream = new StreamOut(file);
            stream.close();
        } catch (FileNotFoundException ex) {
            mainLogger.log(Level.WARNING, null, ex);
        } catch (IOException ex) {
            mainLogger.log(Level.SEVERE, null, ex);
        }
    }

    private static int fileVersion(File file) {
        try {
            StreamIn stream = new StreamIn(file);
            if (stream.readInt() == 0x2a2a2a2a) {
                int fileVersion = stream.readInt();
                if (fileVersion >= 1) {
                    stream.close();
                    return fileVersion;
                }
            }
            stream.close();
        } catch (FileNotFoundException ex) {
            mainLogger.log(Level.WARNING, null, ex);
        } catch (IOException ex) {
            mainLogger.log(Level.SEVERE, null, ex);
        } catch (StreamEndException ex) {
        }
        return -1;
    }

    private static boolean ynDialog(String question) {
        System.out.print(question);
        String answer = in.nextLine();
        if ("y".equals(answer)) {
            return true;
        } else {
            return false;
        }
    }
}
