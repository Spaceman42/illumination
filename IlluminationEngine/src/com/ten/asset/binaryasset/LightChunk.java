package com.ten.asset.binaryasset;

import java.io.IOException;
import java.nio.ByteBuffer;

import com.ten.asset.binaryasset.until.Utils;
import com.ten.math.Vector3;
import com.ten.math.Vector4;

public class LightChunk extends Chunk{
	
	private Vector3 position;
	private Vector4 color;
	
	private static final int POSITION = 0x01;
	private static final int COLOR = 0x02;

	public LightChunk(String id, Vector3 position, Vector4 color) {
		super(id, Chunk.LIGHT_CHUNK);
		this.color = color;
		this.position = position;
	}

	@Override
	public void write(StreamOut s) {
		try {
			s.write(0x10);
			s.write(Chunk.LIGHT_CHUNK);
			s.write(this.id.length()*2);
			s.writeString(id);
			s.write(1 + 1 + 4 + 4 + (3*4) + (4*4));
			
			s.write(POSITION);
			s.write(Utils.floatArrayToByteArray(position.toArray()));
			
			s.write(COLOR);
			s.write(Utils.floatArrayToByteArray(color.toArray()));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static LightChunk load(ByteBuffer b, String id) {
		Vector3 position = null;
		Vector4 color = null;
		
		while(b.hasRemaining()) {
			byte type = b.get();
			
			if (type == POSITION) {
				int length = 3*4;
				byte[] data = new byte[length];
				b.get(data);
				position.fromArray(Utils.byteArrayToFloatArray(data));
			} else if (type == COLOR){
				int length = 4*4;
				byte[] data = new byte[length];
				b.get(data);
				color.fromArray(Utils.byteArrayToFloatArray(data));
			}
		}
		
		return new LightChunk(id, position, color);
	}
	
}
