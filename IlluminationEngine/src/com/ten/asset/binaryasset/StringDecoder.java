package com.ten.asset.binaryasset;

//@author David
public class StringDecoder {

    public static int occurences(String string, String substring) {
        int occurs = 0;
        int index = string.indexOf(substring, 0);
        if (index == -1) {
            return 0;
        }
        while (index != -1) {
            index++;
            occurs++;
            index = string.indexOf(substring, index);
        }
        return occurs;
    }

    public static int occurences(String string, char character) {
        int occurs = 0;
        int index = string.indexOf(character, 0);
        if (index == -1) {
            return 0;
        }
        while (index != -1) {
            index++;
            occurs++;
            index = string.indexOf(character, index);
        }
        return occurs;
    }

    public static String removeNewLineCharacters(String string) {
        if (string.length() > 0) {
            int index = string.length() - 1;
            char temp = string.charAt(index);
            while (temp == '\n') {
                index--;
                temp = string.charAt(index);
                if (index == 0 && temp == '\n') {
                    return "";
                }
            }
            index++;
            return string.substring(0, index);
        } else {
            return string;
        }
    }

    public static String removeWhiteSpace(String string) {
        if (string.length() > 0) {
            int firstIndex = 0;
            int lastIndex = string.length() - 1;
            char temp = string.charAt(lastIndex);
            while (temp == ' ') {
                lastIndex--;
                temp = string.charAt(lastIndex);
                if (lastIndex == 0 && temp == ' ') {
                    return "";
                }
            }
            lastIndex++;
            temp = string.charAt(firstIndex);
            while (temp == ' ') {
                firstIndex++;
                temp = string.charAt(firstIndex);
            }
            return string.substring(firstIndex, lastIndex);
        } else {
            return string;
        }
    }
}
