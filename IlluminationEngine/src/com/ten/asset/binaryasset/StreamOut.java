/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ten.asset.binaryasset;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.ten.asset.binaryasset.until.Utils;

/**
 *
 * @author David
 */
public class StreamOut extends BufferedOutputStream {

    public StreamOut(File file) throws FileNotFoundException {
        super(new FileOutputStream(file));
    }

    public StreamOut(String url) throws FileNotFoundException {
        super(new FileOutputStream(new File(url)));
    }

    public void writeByte(byte value) throws IOException {
        this.write(Utils.unsign(value));
    }

    public void writeInt(int value) throws IOException {
        byte[] b = Utils.intToBytes(value);
        this.write(b);
    }

    public void writeFloat(float value) throws IOException {
        byte[] b = Utils.floatToBytes(value);
        this.write(b);
    }

    public void writeString(String s) throws IOException {
        byte[] b = Utils.stringToBytes(s);
        this.write(b);
    }
}
