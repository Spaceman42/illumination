package com.ten.asset.binaryasset;

//@author David
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TextureChunk extends Chunk {

    private int width;
    private int height;
    private byte[] data;

    public TextureChunk(String id, int width, int height, ByteBuffer data) {
        super(id, Chunk.TEXTURE_CHUNK);
        this.width = width;
        this.height = height;
        this.data = data.array();
    }

    public static TextureChunk load(ByteBuffer b, String id) {
        int width = b.getInt();
        int height = b.getInt();
        int length = b.getInt();
        byte[] data = new byte[length];
        b.get(data);

        ByteBuffer buf = ByteBuffer.wrap(data);
        return new TextureChunk(id, width, height, buf);
    }

    @Override
    public void write(StreamOut s) {
        try {
            s.write(0x10);
            s.write(0x11);
            s.writeInt(this.id.length() * 2);
            s.writeString(id);
            s.writeInt(4 + 4 + 4 + data.length);

            s.writeInt(width);
            s.writeInt(height);
            s.writeInt(data.length);
            s.write(data);
        } catch (IOException ex) {
            Logger.getLogger(ShaderChunk.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
