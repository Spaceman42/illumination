package com.ten.asset.binaryasset.until;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;





public class ObjModelLoader extends ModelLoader {

	public static class IndexBundle {

		public IntList vertexIndices;
		public IntList textureIndices;
		public IntList normalIndices;

		public IndexBundle(int faces) {
			vertexIndices = new IntList(faces * 3);
			textureIndices = new IntList(faces * 3);
			normalIndices = new IntList(faces * 3);
		}
	}

	public static Map<String, BinaryMesh> loadModel(String file) {
		ArrayList<IndexBundle> indicesList = new ArrayList<IndexBundle>();
		int currentObject = 0;
		Scanner scan = new Scanner(file);
		String[] objectStrings = file.split("\no ");
		ArrayList<String> namesList = new ArrayList<String>();
		if (objectStrings.length > 1) {
			for (int i = 1; i < objectStrings.length; i++) {
				indicesList.add(new IndexBundle(
						objectStrings[i].split("\nf ").length - 1));
			}
			currentObject = -1;
		} else {
			indicesList.add(new IndexBundle(file.split("\nf ").length - 1));
			currentObject = 0;
			namesList.add("default");
		}

		VectorList vertices = new VectorList(4);
		VectorList textures = new VectorList(2);
		VectorList normals = new VectorList(4);

		while (scan.hasNextLine()) {
			String line = scan.nextLine();
			line = line.trim();
			if (line.charAt(0) == '#') {
				// Handle Comment
			} else if (line.charAt(0) == 'v') {
				if (line.charAt(1) != 't' && line.charAt(1) != 'n'
						&& line.charAt(1) != 'p') {
					parseVertex(vertices, line.substring(1).trim());
				} else if (line.charAt(1) == 't') {
					parseTexture(textures, line.substring(2).trim());
				} else if (line.charAt(1) == 'n') {
					parseNormal(normals, line.substring(2).trim());
				}
			} else if (line.charAt(0) == 'f') {
				parseFace(line.substring(1).trim(), indicesList, currentObject);
			} else if (line.charAt(0) == 'g') {
				// Handle Group Tag
			} else if (line.charAt(0) == 'o') {
				currentObject++;
				namesList.add(line.substring(1).trim());
			} else if (line.charAt(0) == 'u') {
				// Handle Library Usage
			} else if (line.charAt(0) == 'm') {
				// Handle Library Import
			}
		}
		scan.close();

		Map<String, BinaryMesh> results = new HashMap<String, BinaryMesh>();
		for (int i = 0; i < indicesList.size(); i++) {
			if (indicesList.get(i).textureIndices.get(0) == -1) {
				IntList[] indices = new IntList[] {
						indicesList.get(i).vertexIndices,
						indicesList.get(i).normalIndices };
				String[] valueKeys = new String[] { BinaryMesh.VERTICES, BinaryMesh.NORMALS };
				VectorList[] values = new VectorList[] { vertices, normals };
				results.put(namesList.get(i),
						BinaryMesh.getMesh(indices, values, valueKeys));
			} else {
				IntList[] indices = new IntList[] {
						indicesList.get(i).vertexIndices,
						indicesList.get(i).textureIndices,
						indicesList.get(i).normalIndices };
				String[] valueKeys = new String[] { BinaryMesh.VERTICES,
						BinaryMesh.TEXTURES, BinaryMesh.NORMALS };
				VectorList[] values = new VectorList[] { vertices, textures,
						normals };
				results.put(namesList.get(i),
						BinaryMesh.getMesh(indices, values, valueKeys));
			}
		}
		return results;
	}

	private static void parseVertex(VectorList vertices, String value) {
		String[] s = value.split(" ");
		vertices.add(new float[] { Float.parseFloat(s[0]),
				Float.parseFloat(s[1]), Float.parseFloat(s[2]), 1 });
	}

	private static void parseTexture(VectorList textures, String value) {
		String[] s = value.split(" ");
		textures.add(new float[] { Float.parseFloat(s[0]),
				Float.parseFloat(s[1]) });
	}

	private static void parseNormal(VectorList normals, String value) {
		String[] s = value.split(" ");
		normals.add(new float[] { Float.parseFloat(s[0]),
				Float.parseFloat(s[1]), Float.parseFloat(s[2]), 1 });
	}

	private static void parseFace(String value,
			ArrayList<IndexBundle> indicesList, int currentObject) {
		String[] s = value.split(" ");
		if (s.length == 3) {
			parseFaceGroup(s[0], indicesList, currentObject);
			parseFaceGroup(s[1], indicesList, currentObject);
			parseFaceGroup(s[2], indicesList, currentObject);
		} else if (s.length == 4) {
			parseFaceGroup(s[0], indicesList, currentObject);
			parseFaceGroup(s[1], indicesList, currentObject);
			parseFaceGroup(s[2], indicesList, currentObject);

			parseFaceGroup(s[0], indicesList, currentObject);
			parseFaceGroup(s[2], indicesList, currentObject);
			parseFaceGroup(s[3], indicesList, currentObject);
		}
	}

	private static void parseFaceGroup(String value, 
			ArrayList<IndexBundle> indicesList, int currentObject) {
		String[] s = value.split("/");
		IndexBundle object = indicesList.get(currentObject);
		if (s.length == 1) { // Vertex
			object.vertexIndices.add(Integer.parseInt(s[0]) - 1);
			object.textureIndices.add(-1);
			object.normalIndices.add(-1);
		} else if (s.length == 2) { // Vertex, Texture
			object.vertexIndices.add(Integer.parseInt(s[0]) - 1);
			object.textureIndices.add(Integer.parseInt(s[1]) - 1);
			object.normalIndices.add(-1);
		} else if (s[1].length() == 0) { // Vertex, Normal
			object.vertexIndices.add(Integer.parseInt(s[0]) - 1);
			object.textureIndices.add(-1);
			object.normalIndices.add(Integer.parseInt(s[2]) - 1);
		} else { // Vertex, Normal, Texture
			object.vertexIndices.add(Integer.parseInt(s[0]) - 1);
			object.textureIndices.add(Integer.parseInt(s[1]) - 1);
			object.normalIndices.add(Integer.parseInt(s[2]) - 1);
		}
	}
}
