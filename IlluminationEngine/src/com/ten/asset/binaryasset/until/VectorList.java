package com.ten.asset.binaryasset.until;

import java.util.Arrays;

public class VectorList extends DynamicVector {

	private float[] data;
	private int size = 0;

	/**
	 * Constructor that creates a VectorFList with 16 entries, each as wide as
	 * vectorSize.
	 * 
	 * @param vectorSize
	 *            The width of each entry
	 */
	public VectorList(int vectorSize) {
		super(vectorSize);
		data = new float[vectorSize * 16];
	}

	/**
	 * Constructor that creates a VectorFList with a specified number of
	 * entries, each as wide as vectorSize.
	 * 
	 * @param length
	 *            The number of entries in the list
	 * @param vectorSize
	 *            The width of each entry
	 */
	public VectorList(int length, int vectorSize) {
		super(vectorSize);
		data = new float[vectorSize * length];
	}

	/**
	 * Constructor that creates a VectorFList based on an already existing
	 * array. The array is copied and is interpreted to have entries each as
	 * wide as vectorSize.
	 * 
	 * @param array
	 *            The array to copy
	 * @param vectorSize
	 *            The width of each entry
	 */
	public VectorList(float[] array, int vectorSize) {
		super(vectorSize);
		size = array.length / vectorSize;
		data = new float[array.length];
		System.arraycopy(array, 0, data, 0, array.length);
	}
	
	/**
	 * Constructor that copies another VectorList and duplicates its data.
	 * 
	 * @param list The VectorList to duplicate
	 */
	public VectorList(VectorList list) {
		super(list.getVectorWidth());
		size = list.size;
		data = new float[size * getVectorWidth()];
		System.arraycopy(list.data, 0, data, 0, data.length);
	}

	/**
	 * Returns an array which is a copy of the vector referenced.
	 * 
	 * @param index
	 *            The index of the vector to copy and return
	 * @return A copy of the vector at the specified index
	 */
	public float[] get(int index) {
		float[] temp = new float[getVectorWidth()];
		for (int i = 0; i < getVectorWidth(); i++) {
			temp[i] = data[(index * getVectorWidth()) + i];
		}
		return temp;
	}

	/**
	 * Adds the contents of the specified array to the end of the list. The
	 * array is presumed to have a size equal to the current vector width.
	 * 
	 * @param f
	 *            The vector to append to the list.
	 */
	public void add(float[] vector) {
		ensureCapacity(size + 1);
		set(size, vector);
		size++;
	}

	/**
	 * Sets the vector at the specified index to the vector specified as f. The
	 * size is left unchanged.
	 * 
	 * @param index
	 *            The index to place the vector at
	 * @param f
	 *            The vector to replace the current value with
	 */
	public void set(int index, float[] vector) {
		for (int i = 0; i < getVectorWidth(); i++) {
			data[(index * getVectorWidth()) + i] = vector[i];
		}
	}

	/**
	 * Returns the number of vectors stored in this list.
	 * 
	 * @return size
	 */
	public int size() {
		return size;
	}

	/**
	 * Returns the possible number of vectors that could be stored in this list.
	 * 
	 * @return The current vector capacity
	 */
	public int getCapacity() {
		return data.length / getVectorWidth();
	}

	/**
	 * Ensures that the current list is long enough to contain a certain amount
	 * of vectors.
	 * 
	 * @param capacity
	 *            The maximum number of vectors to store
	 */
	public void ensureCapacity(int capacity) {
		if (getCapacity() < capacity) {
			float[] temp = new float[capacity * getVectorWidth()];
			System.arraycopy(data, 0, temp, 0, data.length);
			data = temp;
		}
	}

	/**
	 * Checks if size is less than maxSize, and if so sets it to maxSize.
	 * 
	 * @param maxSize
	 *            The maximum size of the list
	 */
	private void updateSize(int maxSize) {
		if (maxSize > size) {
			size = maxSize;
		}
	}

	/**
	 * Returns a copy of the array that stores the vectors.
	 * 
	 * @return A copy of the backing array
	 */
	public float[] getArray() {
		float[] temp = new float[size * getVectorWidth()];
		System.arraycopy(data, 0, temp, 0, temp.length);
		return temp;
	}

	/**
	 * Returns the actual backing array of the vectors.
	 * 
	 * @return The backing array
	 */
	public float[] getRawArray() {
		return data;
	}

	/**
	 * Copies a set of adjacent vectors from one list to another. If
	 * copying is being done past the size of the list, the size will be updated
	 * to account for this.
	 * 
	 * @param dest
	 *			  The array to copy vectors to
	 * @param destPos
	 *            The position to begin writing vectors
	 * @param src
	 *            The array to copy vectors from
	 * @param srcPos
	 *            The position to begin copying vectors from
	 * @param length
	 *            The number of vectors to copy
	 */
	public static void copy(VectorList dest, int destPos, VectorList src,
			int srcPos, int length) {
		dest.ensureCapacity(destPos + length);
		dest.updateSize(destPos + length);
		for (int i = 0; i < length; i++) {
			for (int j = 0; j < dest.getVectorWidth(); j++) {
				dest.data[((destPos + i) * dest.getVectorWidth()) + j] = 
						src.data[((srcPos + i) * dest.getVectorWidth()) + j];
			}
		}
	}
	
	public static boolean vectorsEqual(VectorList list1, int index1,
			VectorList list2, int index2, int length) {
		for (int i = 0; i < length; i++) {
			for (int j = 0; j < list1.getVectorWidth(); j++) {
				if (list1.data[((index1 + i) * list1.getVectorWidth()) + j] != 
						list2.data[((index2 + i) * list1.getVectorWidth()) + j]) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public void changeVectorWidth(int vectorSize, boolean makeHomogenous) {
		float[] temp = new float[size * vectorSize];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < vectorSize; j++) {
				temp[(i * vectorSize) + j] = data[(i * getVectorWidth()) + j];
			}
		}
		if (makeHomogenous && vectorSize > getVectorWidth()) {
			for (int i = vectorSize - 1; i < data.length; i += vectorSize) {
				data[i] = 1;
			}
		}
		data = temp;
		setVectorWidth(vectorSize);
	}

	@Override
	public String toString() {
		return Arrays.toString(data);
	}
}
