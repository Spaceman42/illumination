package com.ten.asset.binaryasset;

//@author David
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ten.asset.binaryasset.until.BinaryMesh;
import com.ten.asset.binaryasset.until.Utils;

public class ModelChunk extends Chunk {

	private static final int INDEX_LIST = 0x03;
	private static final int VERTEX_LIST = 0x00;
	private static final int NOMRAL_LIST = 0x02;
	private static final int TEXTURE_LIST = 0x01;
	private static final byte TANGENT_LIST = 0x04;
	private static final byte BITANGENT_LIST = 0x05;

	private float[] vertices;
	private float[] textures;
	private float[] normals;
	private float[] tangents;
	private float[] bitangents;
	private int[] indices;

	private boolean hasTextures;

	private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger
			.getLogger(ModelChunk.class.getName());

	public ModelChunk(String id, BinaryMesh mesh) {
		super(id, Chunk.MODEL_CHUNK);
		this.indices = mesh.getIndices();
		this.vertices = mesh.getValue(BinaryMesh.VERTICES);
		this.textures = mesh.getValue(BinaryMesh.TEXTURES);
		this.normals = mesh.getValue(BinaryMesh.NORMALS);
		this.hasTextures = textures != null;
	}

	public ModelChunk(String id, boolean hasTextures, float[] vertices,
			float[] textureCoordinates, float[] normals, float[] tangents,
			float[] bitangents, int[] indices) {
		super(id, Chunk.MODEL_CHUNK);
		this.vertices = vertices;
		this.textures = textureCoordinates;
		this.normals = normals;
		this.tangents = tangents;
		this.bitangents = bitangents;
		this.indices = indices;
		this.hasTextures = hasTextures;
	}

	public static ModelChunk load(ByteBuffer b, String id) {
		boolean hasTextures;
		float[] vertices = null;
		float[] textures = null;
		float[] normals = null;
		float[] tangents = null;
		float[] bitangents = null;
		int[] indices = null;

		if (b.get() == 0x01) {
			hasTextures = true;
			logger.info("Has textures!");
		} else {
			hasTextures = false;
		}

		while (b.hasRemaining()) {
			byte type = b.get();
			if (type == VERTEX_LIST) { // Vertex List
				int length = b.getInt();
				byte[] data = new byte[length];
				b.get(data);
				vertices = Utils.byteArrayToFloatArray(data);
			} else if (type == TEXTURE_LIST) { // Texture List
				int length = b.getInt();
				byte[] data = new byte[length];
				b.get(data);
				textures = Utils.byteArrayToFloatArray(data);
			} else if (type == NOMRAL_LIST) { // Normal List
				int length = b.getInt();
				byte[] data = new byte[length];
				b.get(data);
				normals = Utils.byteArrayToFloatArray(data);
			} else if (type == INDEX_LIST) { // Index List
				int length = b.getInt();
				byte[] data = new byte[length];
				b.get(data);
				indices = Utils.byteArrayToIntArray(data);
			} else if (type == TANGENT_LIST) {
				int length = b.getInt();
				byte[] data = new byte[length];
				b.get(data);
				tangents = Utils.byteArrayToFloatArray(data);
			} else if (type == BITANGENT_LIST) {
				int length = b.getInt();
				byte[] data = new byte[length];
				b.get(data);
				bitangents = Utils.byteArrayToFloatArray(data);
			} 
		}

		return new ModelChunk(id, hasTextures, vertices, textures, normals,
				tangents, bitangents, indices);
	}

	@Override
	public void write(StreamOut s) {
		try {
			s.write(0x10);
			s.write(0x12);
			s.writeInt(this.id.length() * 2);
			s.writeString(id);

			if (hasTextures) {
				s.writeInt(1 + 1 + 4 + (vertices.length * 4) + 1 + 4
						+ (textures.length * 4) + 1 + 4 + (normals.length * 4)
						+ 1 + 4 + (indices.length * 4) + 1 + 4 + (tangents.length * 4)+ 1 + 4 + (bitangents.length * 4));
				s.write(0x01);
			} else {
				s.writeInt(1 + 1 + 4 + (vertices.length * 4) + 1 + 4
						+ (normals.length * 4) + 1 + 4 + (indices.length * 4));
				s.write(0x00);
			}

			s.write(0x00);
			s.writeInt(vertices.length * 4);
			s.write(Utils.floatArrayToByteArray(vertices));

			if (hasTextures) {
				logger.info("Has textures!");
				s.write(0x01);
				s.writeInt(textures.length * 4);
				s.write(Utils.floatArrayToByteArray(textures));
			}

			s.write(0x02);
			s.writeInt(normals.length * 4);
			s.write(Utils.floatArrayToByteArray(normals));

			s.write(0x03);
			s.writeInt(indices.length * 4);
			s.write(Utils.intArrayToByteArray(indices));
			if (hasTextures) {
				s.write(0x04);
				s.writeInt(tangents.length * 4);
				s.write(Utils.floatArrayToByteArray(tangents));

				s.write(0x05);
				s.writeInt(bitangents.length * 4);
				s.write(Utils.floatArrayToByteArray(bitangents));
			} 

		} catch (IOException ex) {
			Logger.getLogger(ShaderChunk.class.getName()).log(Level.SEVERE,
					null, ex);
		}
	}

	public float[] getVertices() {
		return vertices;
	}

	public void setVertices(float[] vertices) {
		this.vertices = vertices;
	}

	public float[] getTextureCoordinates() {
		return textures;
	}

	public void setTextureCoordinates(float[] textureCoordinates) {
		this.textures = textureCoordinates;
	}

	public float[] getNormals() {
		return normals;
	}

	public void setNormals(float[] normals) {
		this.normals = normals;
	}

	public int[] getIndices() {
		return indices;
	}
	
	public float[] getTangents() {
		return tangents;
	}
	
	public float[] getBitangents() {
		return bitangents;
	}

	public void setIndices(int[] indices) {
		this.indices = indices;
	}

	public boolean isHasTextures() {
		return hasTextures;
	}

	public void setHasTextures(boolean hasTextures) {
		this.hasTextures = hasTextures;
	}

	@Override
	public String toString() {
		return "ModelChunk: " + id;
	}
}
