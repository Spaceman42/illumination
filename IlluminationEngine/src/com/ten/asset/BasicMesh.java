package com.ten.asset;

public interface BasicMesh {
	public float[] getVertices();
	public float[] getNormals();
	public float[] getTextureCoordinates();
	public int[] getIndices();
	
	public String getId();
}
