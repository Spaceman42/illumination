package com.ten.asset;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.log4j.Logger;

import com.ten.GameEngine;



public class MaterialFileLoader {
	private static final Logger logger = Logger.getLogger(MaterialFileLoader.class.getName());
	
	public static Map<String, Material> loadMTLFile(File file, String textureFolder) throws IOException {
		Map<String, Material> materialMap = new HashMap<String, Material>();
		Scanner scan = new Scanner(new BufferedReader(new FileReader(file)));
		
		if (textureFolder == null) {
			textureFolder = "";
		}
		logger.info("Loading MTL! Mtl: " + file.getName() + "File exsists = " + file.exists());
		while(scan.hasNextLine()) {
			String line = scan.nextLine().trim();
			if (line.startsWith("newmtl ")) {
				String id = line.split(" ")[1];
				logger.info("new material: " + id);
				String spec = null;
				String color = null;
				String bump = null;
				String matLine;
				while (scan.hasNextLine() && !(matLine = scan.nextLine().trim()).isEmpty()) {
					if (matLine.startsWith("map_Ns ")) {
						spec = textureFolder + matLine.split(" ")[1];
					} else if (matLine.startsWith("map_Kd ")) {
						logger.info("new color texture");
						color = textureFolder + matLine.split(" ")[1];
					} else if (matLine.startsWith("map_d")){
						//TODO: Parse alpha map
					} else if (matLine.startsWith("map_bump ") || matLine.startsWith("map_Dist ")) {
						bump = textureFolder + matLine.split(" ")[1];
					}
				}
				if (color != null) {
					materialMap.put(id, new Material(id, color, spec, bump));
				}
			}
		}
		scan.close();
		
		return materialMap;
	}
}
