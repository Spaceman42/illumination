package com.ten.asset;

import java.util.HashMap;
import java.util.Map;

import com.ten.asset.util.IntList;
import com.ten.asset.util.VectorList;

public class AssetMesh {

	private static class RawMesh {

		public IntList indices;
		public VectorList[] values;

		public RawMesh(IntList indices, VectorList[] values) {
			this.indices = indices;
			this.values = values;
		}
	}

	private static class IndexMap {

		private int[][] map;

		public IndexMap(int size) {
			map = new int[size][];
		}

		public boolean hasIndex(int index) {
			return map[index] != null;
		}

		public void addIndex(int index, int newLoc) {
			if (map[index] == null) {
				map[index] = new int[1];
				map[index][0] = newLoc;
			} else {
				int[] temp = new int[map[index].length + 1];
				System.arraycopy(map[index], 0, temp, 0, map[index].length);
				temp[temp.length - 1] = newLoc;
				map[index] = temp;
			}
		}

		public int[] getIndex(int index) {
			return map[index];
		}
	}

	// Default keys for mesh
	public static String VERTICES = "VERTICES";
	public static String NORMALS = "NORMALS";
	public static String TEXTURES = "TEXTURES";

	private int[] indices;
	private Map<String, float[]> attributes;

	public AssetMesh(int[] indices, String[] keys, float[][] values) {
		this.indices = indices;
		attributes = new HashMap<String, float[]>();
		for (int i = 0; i < keys.length; i++) {
			attributes.put(keys[i], values[i]);
		}
	}

	public float[] getValue(String key) {
		return attributes.get(key);
	}

	public int[] getIndices() {
		return indices;
	}

	public static AssetMesh getMesh(IntList[] indices, VectorList[] values,
			String[] keys) {
		RawMesh raw = getRawMesh(indices, values);
		float[][] finalValues = new float[raw.values.length][];
		for (int i = 0; i < raw.values.length; i++) {
			finalValues[i] = raw.values[i].getArray();
		}
		return new AssetMesh(raw.indices.getArray(), keys, finalValues);

	}

	// This method is literal hell, fuck you <- this method doesn't work
	private static RawMesh getRawMesh(IntList[] _indices, VectorList[] _values) {
		// Copies the first list of indices to "pivot" around
		IntList indices = new IntList(_indices[0].getRawArray());
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < indices.size(); i++) {
			if (indices.get(i) < min) {
				min = indices.get(i);
			}
			if (indices.get(i) > max) {
				max = indices.get(i);
			}
		}
	//	System.out.println("Max: " + max + ", Min: " + min);
		VectorList[] values = new VectorList[_values.length];
		values[0] = new VectorList(_values[0]);
		for (int i = 1; i < _values.length; i++) {
			values[i] = new VectorList(_values[i].size(),
					_values[i].getVectorWidth());
		}

		// Creates an array to determine which values have been used
	//	System.out.println("hasValue size:" +_values[0].size() );
		boolean[] hasValue = new boolean[_values[0].size()];

		// Creates a map of where indices have been copied to
		IndexMap map = new IndexMap(_values[0].size());

		for (int i = 0; i < _indices[0].size(); i++) {
			int index = _indices[0].get(i);
		//	System.out.println("On indices " + i);
			if (!hasValue[index]) {
				for (int j = 1; j < values.length; j++) {
					VectorList.copy(values[j], index, _values[j],
							_indices[j].get(i), 1);
				}
				hasValue[index] = true;
			} 
			
			else if (!checkEqual(values, index, _values, _indices, i)) {
				if (map.hasIndex(index)) {
					int[] newLocs = map.getIndex(index);
					for (int j = 0; j < newLocs.length; j++) {
						if (checkEqual(values, newLocs[j], _values, _indices, i)) {
							indices.set(i, newLocs[j]);
						}
					}
				} else {
					int offset = values[0].size();
					indices.set(i, offset);
					for (int j = 0; j < values.length; j++) {
						VectorList.copy(values[j], offset, _values[j],
								_indices[j].get(i), 1);
					}
					map.addIndex(index, offset);
				}
			}
			
		}

		return new RawMesh(indices, values);
	}

	private static boolean checkEqual(VectorList[] values1, int i1,
			VectorList[] values2, IntList[] indices2, int i2) {
		for (int i = 0; i < values1.length; i++) {
			if (!VectorList.vectorsEqual(values1[i], i1, values2[i],
					indices2[i].get(i2), 1)) {
				return false;
			}
		}
		return true;
	}
}
