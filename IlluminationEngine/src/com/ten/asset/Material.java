package com.ten.asset;

import org.apache.log4j.Logger;

import com.ten.graphics.renderer.DefaultTextures;
import com.ten.graphics.renderer.model.Texture;
import com.ten.graphics.shader.uniform.Uniform;

public class Material {
	private static final Logger logger = Logger.getLogger(Material.class.getName());
	public String id;
	
	private Texture color;
	
	private boolean hasSpec;
	private Texture spec;
	
	private boolean hasNormal;
	private Texture normal;
	
	public Material(String id, String color, String spec, String bump) {
		this.id = id;
		hasSpec = spec != null;
		hasNormal = bump != null;
		if (hasSpec) {
			this.spec = Texture.loadTexture(spec);
		}
		if (hasNormal) {
			this.normal = Texture.loadTexture(bump);
		}
		if (color != null) {
			logger.info(id + " -- Loading color texture from " + color);
			this.color = Texture.loadTexture(color);
		} else {
			this.color = DefaultTextures.getNoImageTex();
		}
	}
	
	public boolean hasNormal() {
		return hasNormal;
	}
	
	public boolean hasSpec() {
		return hasSpec;
	}
	
	public Texture getNormal() {
		return normal;
	}
	
	public Texture getColor() {
		return color;
	}
	
	public Texture getSpecular() {
		return spec;
	}

	public float getSpecValue() {
		return 1.0f;
	}
}
