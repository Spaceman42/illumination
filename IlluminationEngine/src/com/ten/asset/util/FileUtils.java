package com.ten.asset.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class FileUtils {
	public static int fileLength(String data) {
		return data.split("\r\n|\r|\n").length;
	}
}
