package com.ten.asset.util;

/**
 * An abstract class that both FloatList and IntList extend. This class contains
 * just the width of the vectors that the list contains.
 * 
 * @author David
 * 
 */
public abstract class DynamicVector {

	private int vectorWidth;

	/**
	 * A constructor for initializing the vectorSize.
	 * 
	 * @param vectorSize
	 *            the size of the vectors
	 */
	public DynamicVector(int vectorWidth) {
		this.vectorWidth = vectorWidth;
	}

	/**
	 * Returns the size of the vectors the list contains
	 * 
	 * @return vectorSize
	 */
	public int getVectorWidth() {
		return vectorWidth;
	}

	/**
	 * Sets the size of the vectors the list contains, does not update the list.
	 * 
	 * @param vectorSize
	 */
	protected void setVectorWidth(int vectorWidth) {
		this.vectorWidth = vectorWidth;
	}

	/**
	 * Resizes each of the vectors in the list to vectorSize and sets the vector
	 * size for the list.
	 * 
	 * @param vectorWidth
	 *            The new size of each vector
	 * @param makeHomogenous
	 *            Specifies whether or not to make the vectors homogeneous
	 */
	public abstract void changeVectorWidth(int vectorWidth, boolean makeHomogeneous);
}
