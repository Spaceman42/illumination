package com.ten.asset.util;

import java.util.Arrays;

public class IntList {

    private int[] data;
    private int size = 0;

    public IntList() {
        data = new int[16];
    }

    public IntList(int length) {
        data = new int[length];
    }

    public IntList(int length, int init) {
        size = length;
        data = new int[length];
        Arrays.fill(data, init);
    }

    public IntList(int[] array) {
        size = array.length;
        data = new int[array.length];
        System.arraycopy(array, 0, data, 0, array.length);
    }

    public int get(int i) {
        return data[i];
    }

    public void add(int value) {
        if (size >= data.length) {
            int[] temp = new int[data.length * 2];
            System.arraycopy(data, 0, temp, 0, data.length);
            data = temp;
        }
        data[size] = value;
        size++;
    }
    
    public void add(int[] value) {
    	while (size + value.length >= data.length) {
            int[] temp = new int[data.length * 2];
            System.arraycopy(data, 0, temp, 0, data.length);
            data = temp;
        }
    	for (int i = 0; i < value.length; i++) {
    		data[size + i] = value[i];
    	}
        size += value.length;
    }

    public void set(int i, int f) {
        if (size < i + 1) {
            size = i + 1;
        }
        data[i] = f;
    }

    public int size() {
        return size;
    }

    public int getCapacity() {
        return data.length;
    }

    public void ensureCapacity(int capacity) {
        while (data.length < capacity) {
            int[] temp = new int[data.length * 2];
            System.arraycopy(data, 0, temp, 0, data.length);
            data = temp;
        }
    }

    public int[] getArray() {
        int[] temp = new int[size];
        System.arraycopy(data, 0, temp, 0, size);
        return temp;
    }

    public int[] getRawArray() {
        return data;
    }

    public void copy(int destPos, int[] src, int srcPos, int length) {
        if (size < destPos + length) {
            size = destPos + length;
            ensureCapacity(size);
        }
        for (int i = 0; i < length; i++) {
            data[destPos + i] = src[srcPos + i];
        }
    }

    public boolean sectionsEqual(int destPos, int[] src, int srcPos, int length) {
        for (int i = 0; i < length; i++) {
            if (data[destPos + i] != src[srcPos + i]) {
                return false;
            }
        }
        return true;
    }
    
    public int[] getMinMaxValue() {
    	int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		for (int i = 0; i<data.length; i++) {
			if (data[i] < min) {
				min = data[i];
			} 
			if (data[i] > max) {
				max = data[i];
			}
		}
		System.out.println("Max: " + max + ", Min: " + min);
		return new int[] {min, max};
    }
    
    public void adjustValues(int minValue) {
    	for (int i = 0; i < data.length; i++) {
    		data[i] = data[i] - minValue;
    	}
    }

    @Override
    public String toString() {
        return Arrays.toString(data);
    }
}