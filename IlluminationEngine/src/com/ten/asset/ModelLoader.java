package com.ten.asset;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public abstract class ModelLoader {

	public static Map<String, AssetMesh> loadModel(File file) {
		if (file.getName().endsWith(".obj")) {
			String value = null;
			try {
				value = loadFile(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return ObjModelLoader.loadModel(value);
		}
		return null;
	}

	public static String loadFile(File file) throws FileNotFoundException,
			IOException {
		InputStream in = new FileInputStream(file);
		byte[] b = new byte[in.available()];
		in.read(b);
		in.close();
		return new String(b);
	}
}
