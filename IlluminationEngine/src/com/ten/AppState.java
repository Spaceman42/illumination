package com.ten;

/**
 * 
 * @author David Gronlund, Ethan Hunter
 *
 */
public enum AppState {
	
	STARTING, RUNNING, SHUTTING_DOWN, CLOSING;

	private static AppState state = STARTING;
	protected static int stateThreadAmount = 0;
	protected static int stateChangeFinished = 0;
	
	public static boolean closeRequested = false;

	private AppState() {
	}

	public static AppState getState() {
		return state;
	}

	public static boolean setState(AppState newState) {
		System.err.println("State change to " + newState + " requested by thread " + Thread.currentThread().getName());
		stateChangeFinished--;
		if (state != newState && stateChangeFinished == 0) {
			stateChangeFinished = stateThreadAmount;
			state = newState;
			return true;
		} else {
			return false;
		}
	}
	
	
}
