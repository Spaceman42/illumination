package com.ten.game;

import com.ten.math.Vector4;

public class Scene extends Node{
	private final String name;
	
	public Scene(String name) {
		this.name = name;
	}
	
	public void add(GameObject o) {
		getChildren().add(o);
	}
	public void remove(GameObject o) {
		getChildren().remove(o);
	}
	
	@Override
	public void act() {
		for (GameObject n: getChildren()) {
			n.act();
		}
	}

	@Override
	public void delete() {
		for (GameObject n: getChildren()) {
			n.delete();
		}
		
	}

	@Override
	public void onClick(Vector4 worldLocation) {
		// TODO Auto-generated method stub
		
	}

}
