package com.ten.game;

import com.bulletphysics.dynamics.RigidBody;
import com.ten.graphics.RenderableObject;
import com.ten.graphics.renderer.model.CompoundModel;
import com.ten.graphics.renderer.model.Model;
import com.ten.graphics.shader.Shader;
import com.ten.math.Matrix4;
import com.ten.math.Vector3;
import com.ten.math.Vector4;
import com.ten.physics.PhysicalObject;
import com.ten.physics.PhysicsRigidBody;
import com.ten.physics.shape.BoundingShape;
import com.ten.physics.shape.ShapeConstructionInfo;

public abstract class BasicGameObject extends Node implements GameObject,
		RenderableObject, PhysicalObject {
	protected PhysicsRigidBody rigidBody;
	protected Model model;

	private TransformManager transform;

	private Vector3 initialInertia;
	private Vector3 scale;
	private float mass;
	private float angularDampening;
	private float restitution;

	public BasicGameObject(String modelLocation, String mtlLocation,
			ShapeConstructionInfo shape, Vector3 initialInertia, Vector3 scale,
			float mass, float angularDampening, float restitution) {
		
		model = CompoundModel.load(modelLocation, mtlLocation, shape, scale.x);
		this.initialInertia = initialInertia;
		this.scale = scale;
		this.mass = mass;
		this.angularDampening = angularDampening;
		this.restitution = restitution;
	}

	@Override
	public PhysicsRigidBody getRigidBody() {
		return rigidBody;
	}

	@Override
	public boolean loaded() {
		return (rigidBody != null) && model.loaded();
	}

	@Override
	public void draw(Shader shader) {
		model.draw(shader, 0);
	}

	/**
	 * Replaces {@code act()} which is overridden in {@code BasicGameObject}.
	 * {@code BasicGameObject} calls this method from {@code act}.
	 * {@code BasicGameObject} performs some operations in @code act()} that are
	 * required to abstract the underlying threading of the engine.
	 * 
	 */
	public abstract void basicAct();

	@Override
	public void act() {
		if (model.loaded() && rigidBody == null) {
			rigidBody = new PhysicsRigidBody(model.getBBs()[0], new Vector3(
					transform.getRawMatrix().getTranslation()), initialInertia,
					scale, mass, restitution, angularDampening, this);
		}
		if (loaded()) {
			basicAct();
		}
	}
	/**
	 * Translates the object in the direction given based on it's orientation
	 * @param vec
	 */
	public void translate(Vector3 vec) {
		Vector3 trans = transform.getRelativeTranslation(vec, 1);
		rigidBody.translate(trans);
		Vector3 endTranslation = rigidBody.getTranslation();
		transform.setTranslation(endTranslation);
	}
	/**
	 * Rotates the object based on it's orientation
	 * @param axis
	 * @param radians
	 */
	public void rotate(Vector3 axis, float radians) {
		Vector3 rotAxis = transform.getRelativeRotationAxis(axis);
		//rigidBody.rotate(rotAxis, radians);
	}
	
	public TransformManager getTransformManager() {
		return transform;
	}

	@Override
	public Matrix4 getModelMatrix() {
		return transform.getRawMatrix();
	}

	@Override
	public BoundingShape[] getBBs() {
		return model.getBBs();
	}

	@Override
	public void delete() {
		model.delete();
	}
}
