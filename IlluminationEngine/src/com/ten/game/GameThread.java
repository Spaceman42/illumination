package com.ten.game;

import org.apache.log4j.Logger;

import com.ten.AppState;
import com.ten.LoopSyncer;
import com.ten.ThreadManager;
import com.ten.Utils;
import com.ten.physics.PhysicsEngine;


public class GameThread extends Thread {

	private AbstractGame game;
	private PhysicsEngine physicsEngine;
	private static final Logger logger = Logger.getLogger(GameThread.class.getName());
	//Needed for maintaining framerate
	private LoopSyncer syncer = new LoopSyncer();

	
	public GameThread() {
		super("Game-Thread");
	//	physicsEngine = new PhysicsEngine();
	}
	
	@Override
	public void run() {
		
		try {
			//physicsEngine.init();
		
			AppState.setState(AppState.RUNNING);
			logger.info("Requested state change to running");
			//System.exit(-1);
			while (AppState.getState() != AppState.SHUTTING_DOWN) {
			//	physicsEngine.run(game.getPhysicalObjects());
				//game.setCollisions(physicsEngine.getCollisions());
				ThreadManager.getInstance().updatePhysicsSet(game.getPhysicalObjects());
				game.run();
				
				if (AppState.closeRequested) {
					AppState.setState(AppState.SHUTTING_DOWN);
				}
				
				syncer.sync(60);
			}
			AppState.setState(AppState.CLOSING);
		} catch (Exception e) {
			e.printStackTrace();
			logger.fatal(e);
			System.exit(-1);
		}
	}
	
	public AbstractGame getGame() {
		return game;
	}

	public void setGame(AbstractGame game) {
		this.game = game;
	}
	
	public PhysicsEngine getPhysicsEngine() {
		return physicsEngine;
	}
}
