package com.ten.game;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import com.ten.io.keyboard.Key;
import com.ten.math.GameMath;
import com.ten.math.Matrix4;
import com.ten.math.Vector4;

/**
 * @author David Gronlund
 */
public class DebugCamera {
	
	private float baseSpeed = 0.02f;

    private Vector4 translation = new Vector4(0, 0, 0, 1);
    private float rotateX;
    private float rotateY;
    protected Matrix4 viewMatrix;
    private Matrix4 projectionMatrix;
    private Matrix4 invProjectionMatrix;
    private float speed = baseSpeed;
    private float near;
    private float far;
    private float right;
    private float top;
    private float fov;

    public DebugCamera(float near, float far, float fov, float aspect) {
        viewMatrix = new Matrix4().setIdentity();
        projectionMatrix = GameMath.projectionMatrix(near, far, fov, aspect);
        invProjectionMatrix = new Matrix4(projectionMatrix).inverse();
        this.near = near;
        this.far = far;
        top = near * GameMath.tan((float) ((fov * GameMath.PI) / 360.0));
        right = -(-top * aspect);
        this.fov = fov;
    }
    
    public DebugCamera(DebugCamera camera) {
    	viewMatrix = camera.getViewMatrix();
    	projectionMatrix = camera.getProjectionMatrix();
    	invProjectionMatrix = camera.getInverseProjectionMatrix();
    	Vector4 frusData = camera.getFrustrumData();
    	near = frusData.x;
    	far = frusData.y;
    	right = frusData.z;
    	top = frusData.w;
    	fov = camera.getFov();
    }
    /**
     *
     * @return Vector4 packed with the nearclip, farclip, right, and top (in that order) values of the projection matrix
     */
    public Vector4 getFrustrumData() {
    	return new Vector4(near, far, right, top);
    }
    
    public float getNear() {
    	return near;
    }
    public float getFar() {
    	return far;
    }
    public float getFov() {
    	return fov;
    }
    
    public Vector4 getTranslation() {
    	return new Vector4(translation);
    }
    
    public Matrix4 getViewMatrix() {
        return new Matrix4(viewMatrix);
    }

    public Matrix4 getProjectionMatrix() {
        return new Matrix4(projectionMatrix);
    }
    
    public Matrix4 getInverseProjectionMatrix() {
    	return new Matrix4(invProjectionMatrix);
    }

    public Matrix4 getViewProjectionMatrix() {
        return new Matrix4(projectionMatrix).multiply(viewMatrix);
    }
    
    public void adjustSpeed(int deltaTime) {
    	speed = baseSpeed * deltaTime;
    	baseSpeed += Mouse.getDWheel() * 0.001;
    	if (baseSpeed < 0) {
    		baseSpeed = 0;
    	}
    }
    
    public void handleInput(int deltaTime) {
    	adjustSpeed(deltaTime);
    	handleInput();
    }
    
    
    public void handleInput() {
        Vector4 keysPressed = new Vector4();
        int dx = 0;
        int dy = 0;
        if (Mouse.isGrabbed()) {
        	dx = Mouse.getDX();
        	dy = Mouse.getDY();
        }
        if (Key.KEY_W.isDown()) {
            keysPressed.z += speed;
        }
        if (Key.KEY_S.isDown()) {
            keysPressed.z -= speed;
        }
        if (Key.KEY_A.isDown()) {
            keysPressed.x -= speed;
        }
        if (Key.KEY_D.isDown()) {
            keysPressed.x += speed;
        }
        if (Key.KEY_Q.isDown()) {
            keysPressed.y -= speed;
        }
        if (Key.KEY_E.isDown()) {
            keysPressed.y += speed;
        }
        keysPressed.rotate(new Vector4(0, 1, 0), -rotateY);
        translation.translate(keysPressed);
        rotateY -= GameMath.toRadians(dx / 16.0f);
        rotateY = GameMath.normalize(rotateY);
        rotateX -= GameMath.toRadians(dy / 16.0f);
        rotateX = GameMath.normalize(rotateX);
        viewMatrix.setIdentity();
        viewMatrix.translate(translation);
        viewMatrix.rotate(0, 1, 0, rotateY);
        viewMatrix.rotate(1, 0, 0, -rotateX);
    //    viewMatrix.rotate(0, 0, 1, (float) Math.PI);
        viewMatrix = GameMath.findCameraMatrix(viewMatrix, false);
        if (Keyboard.isKeyDown(Keyboard.KEY_K)) {
        	System.out.println("Camera translation: " + translation);
        	System.out.println("Camera rotation X:" + -rotateX + " Y: " + rotateY);
        }
    }
}
