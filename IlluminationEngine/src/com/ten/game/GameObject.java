package com.ten.game;

import com.ten.math.Vector4;


public interface GameObject {
	
	public void act();
	
	public void delete();
	
	public void onClick(Vector4 worldLocation);
}
