package com.ten.game;

import com.ten.math.Matrix4;
import com.ten.math.Vector4;

public interface Camera {
	public Matrix4 getViewMatirx();
	public Vector4 getTranslation();
	
	public Vector4 getFrustrumData();
    
    public float getNear();
    public float getFar();
    public float getFov();
}
