package com.ten.game;

import com.ten.math.Vector4;

public class ClickEvent {
	public Object objectId;
	public int screenX;
	public int screenY ;
	public Vector4 worldPosition;
	
	public ClickEvent(Object objectId, int screenX, int screenY, Vector4 worldPosition) {
		this.objectId = objectId;
		this.screenX = screenX;
		this.screenY = screenY;
		this.worldPosition = new Vector4(worldPosition);
	}
}
