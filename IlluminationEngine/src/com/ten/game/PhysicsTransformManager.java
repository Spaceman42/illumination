package com.ten.game;



import com.ten.math.GameMath;
import com.ten.math.Matrix4;
import com.ten.math.Vector3;
import com.ten.math.Vector4;
import com.ten.physics.PhysicsRigidBody;
import com.ten.physics.shape.BoundingShape;

public class PhysicsTransformManager {
	private Matrix4 mat;
	private Vector3 localAxes;
	private Vector3 scale;
	private PhysicsRigidBody shape;
	private float angularMomentum = 0;
	
	public PhysicsTransformManager(Vector3 initalTranslation, Vector3 scale, PhysicsRigidBody shape) {
		this.scale = scale;
		this.shape = shape;
		mat = new Matrix4().translate(new Vector4(initalTranslation));
		localAxes = new Vector3(1,1,1);
	}
	
	public void setAngularMomentum(PhysicsRigidBody shape, Vector3 axis, float value) {
		angularMomentum += value;
		shape.setAngularVelocity(axis, angularMomentum);
	}
	
	public void transformAbs(PhysicsRigidBody shape, Vector3 transform) {
		shape.translate(transform);
	}
	
	public Matrix4 getRawMatrix(PhysicsRigidBody shape) {
		if (shape != null) {
			return shape.getMatrix();
		} else {
			return new Matrix4();
		}
	}
}
