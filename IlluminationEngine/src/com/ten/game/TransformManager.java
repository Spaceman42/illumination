package com.ten.game;

import com.ten.math.GameMath;
import com.ten.math.Matrix3;
import com.ten.math.Matrix4;
import com.ten.math.Quaternion4;
import com.ten.math.Vector3;
import com.ten.math.Vector4;

public class TransformManager {
	private Vector3 localAxes;
	private Matrix4 matrix;
	private Vector3 scale;
	
	public TransformManager(Vector3 initalTranslation, Vector3 scale) {
		matrix = new Matrix4();
		localAxes = new Vector3(1,1,1);
		this.scale = scale;
	}
	
	public void rotateX(float radians) {
		rotate(new Vector3(1,0,0), radians);
	}
	public void rotateY(float radians) {
		rotate(new Vector3(0,1,0), radians);
	}
	public void rotateZ(float radians) {
		rotate(new Vector3(0,0,1), radians);
	}
	
	public void rotate(Vector3 axis, float radians) {
		axis.normalise();
		Vector3 rotAxis = new Vector3(localAxes).multiply(axis);
		matrix.rotate(new Vector4(rotAxis), GameMath.normalize(radians));
		localAxes.mutiplyByMatrix(matrix.getMatrix3());
		localAxes.normalise();
		localAxes.x = Math.abs(localAxes.x);
		localAxes.y = Math.abs(localAxes.y);
		localAxes.z = Math.abs(localAxes.z);
	}
	
	public Vector3 getRelativeRotationAxis(Vector3 initialAxis) {
		initialAxis.normalise();
		Vector3 rotAxis = new Vector3(localAxes).multiply(initialAxis);
		return rotAxis.normalise();
	}
	
	public void rotateAbsolute(Vector3 axis, float radians) {
		matrix.rotate(new Vector4(axis), radians);
		localAxes.mutiplyByMatrix(matrix.getMatrix3());
		localAxes.normalise();
	}
	
	public void setRotation(Matrix3 mat) {
		
	}
	/**
	 * 
	 * @param vec
	 * @param mul
	 * @return the absolute transformation vector. Can be used to transform in the physics engine
	 */
	public Vector3 translate(Vector3 vec, float mul) {
		Matrix3 mat = matrix.getMatrix3();
		Vector3 transVec = new Vector3(vec).normalise();
		//transVec.mutiplyByMatrix(mat);
		//transVec.multiply(vec.magnitude());
		transVec.multiply(localAxes);
		transVec.normalise();
		transVec.multiply(vec.magnitude());
		translateAbsolute(transVec.multiply(mul));
		return transVec;
	}
	
	public Vector3 getRelativeTranslation(Vector3 vec, float mul) {
		Matrix3 mat = matrix.getMatrix3();
		Vector3 transVec = new Vector3(vec).normalise();
		//transVec.mutiplyByMatrix(mat);
		//transVec.multiply(vec.magnitude());
		transVec.multiply(localAxes);
		transVec.normalise();
		transVec.multiply(vec.magnitude());
		transVec.multiply(mul);
		return transVec;
	}
	
	public void translateAbsolute(Vector3 vec) {
		matrix.translate(vec.x, vec.y, vec.z);
	}
	
	public Matrix4 getRawMatrix() {
		return new Matrix4(matrix).scale(scale.x, scale.y, scale.z);
	}

	public void setTranslation(Vector3 endTranslation) {
		matrix.setTranslation(endTranslation);
	}
}
