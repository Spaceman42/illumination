package com.ten.game;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.ten.AppState;
import com.ten.GameEngine;
import com.ten.ThreadManager;
import com.ten.Utils;
import com.ten.audio.AudibleObject;
import com.ten.audio.AudioEngine;
import com.ten.graphics.RenderableObject;
import com.ten.io.keyboard.IOKeyboard;
import com.ten.io.mouse.IOMouse;
import com.ten.math.Vector4;
import com.ten.physics.PhysicalObject;

public abstract class AbstractGame {
	private static final Logger logger = Logger.getLogger(AbstractGame.class);
	private long lastFrameTime;
	private DebugCamera cameraToSend;
	private Set<PhysicalObject> physicalObjects;
	private List<CollisionObject> collisions;
	private Scene scene;

	private boolean click = false;
	private ClickEvent clickEvent;

	public abstract void onPause();
	
	
	public abstract void onClick(Object objectId, int screenX, int screenY, Vector4 worldPosition);

	public void onClickExternal(Object objectID, int screenX, int screenY, Vector4 worldPosition) {
		clickEvent = new ClickEvent(objectID, screenX, screenY, worldPosition);
		click = true;
		// Use a click event queue, sortof like requests, but different, instead
		// of this
	}

	/**
	 * This method will be called a maximum of 60 times per second or 16.66ms although it may be called a at slower rate.
	 * 
	 * @param deltaTime - Milliseconds scince the last time onLoop() was called
	 * @param collisions 
	 * @return List of all game objects that need to be rendered, need to play audio, or need to have physics applied. It is suggested
	 * that a HashSet is used.
	 */
	public abstract void onLoop(int deltaTime);
	
	public void setScene(Scene scene) {
		this.scene = scene;
	}

	protected void run() {
		logger.info(AppState.getState());
		if (AppState.getState() == AppState.RUNNING) {
			GameEngine.getGui().handleInput();
			if (click) {
				onClick(clickEvent.objectId, clickEvent.screenX, clickEvent.screenY, new Vector4(clickEvent.worldPosition));
				click = false;
			}
			IOKeyboard.updateKeys();
			IOMouse.updateMouse(cameraToSend);
			onLoop(getDeltaTime());
			Collection<GameObject> objects = scene.getChildren();

			List<RenderableObject> renerableObjects = new ArrayList<RenderableObject>();
			physicalObjects = new HashSet<PhysicalObject>();
			// More type lists here ex: physics object
			for (GameObject o : objects) {
				if (o instanceof RenderableObject) {
					renerableObjects.add((RenderableObject) o);
				}
				if (o instanceof PhysicalObject) {
					physicalObjects.add((PhysicalObject) o);
				}
				// Check for other types here
			}
			ThreadManager.getInstance().updateGameData(cameraToSend, renerableObjects);
		}
		//updateFPS();
		//sync(60);
	}
	
	public Set<PhysicalObject> getPhysicalObjects() {
		return physicalObjects;
	}
	

	/**
	 * Sets the camera that will be used when rendering. Doesn't need to be
	 * called every loop, but it dosn't matter if it is
	 * 
	 * @param camera
	 */
	public void setCamera(DebugCamera camera) {
		cameraToSend = camera;
	}

	private int getDeltaTime() {
		long time = Utils.getTime();
		int delta = (int) (time - lastFrameTime);
		lastFrameTime = time;
		return delta;
	}

	public void setCollisions(List<CollisionObject> list) {
		this.collisions = list;
	}

}
