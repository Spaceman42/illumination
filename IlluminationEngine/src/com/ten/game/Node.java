package com.ten.game;

import java.util.ArrayList;
import java.util.List;

import com.ten.math.Vector4;

public abstract class Node implements GameObject {

	private List<GameObject> children;
	
	public Node() {
		children = new ArrayList<GameObject>();
	}
	
	public List<GameObject> getChildren() {
		return children;
	}
	
}
