package com.ten;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.ten.graphics.Configeration;

/**
 * Thread safe configurable setting table. 
 * @author Ethan Hunter
 *
 * @param <T>
 */
public class SettingsTable<T> {
	private Configeration state;
	private Map<String, T> settings; 
	
	public SettingsTable(Configeration state) {
		this.state = state;
		settings = new ConcurrentHashMap<String, T>();
	}
	
	public void setToEnabled() {
		state = Configeration.ENABLED;
	}
	public void setToDisabled() {
		state = Configeration.DISABLED;
	}
	public Configeration getState() {
		return state;
	}
	
	public void setSetting(String setting, T value) {
		settings.put(setting, value);
	}
	
	public T getSetting(String setting) {
		return settings.get(setting);
	}
}
