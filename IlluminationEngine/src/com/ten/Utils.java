package com.ten;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.vecmath.Vector3f;

import org.apache.log4j.Logger;

import com.ten.math.Vector3;

public class Utils {

	private static final int ONE_MILLION = 1_000_000;
	private static final int ONE_THOUSAND = 1_000;
	
	private static final Logger logger = Logger.getLogger(Utils.class.getName());

	private Utils() {
	}

	/**
	 * Returns the time in milliseconds synced with System.nanoTime().
	 * 
	 * @return The program time in milliseconds
	 */
	public static long getTime() {
		return Math.abs(System.nanoTime() / ONE_MILLION);
	}

	public static float toSeconds(int milliseconds) {
		return (float) ((double) milliseconds / (double) ONE_THOUSAND);
	}

	public static float randomFloat() {
		return (float) Math.random();
	}

	public static Vector3[] floatsToVectors(float[] array, int size) {
		if (size >= 3) {
			Vector3[] vectors = new Vector3[array.length / size];
			for (int i = 0; i < array.length / size; i += size) {
				vectors[i] = new Vector3(array[i], array[i+1], array[i+2]);
			}
			return vectors;
		} else {
			return null;
		}
	}
	
	public static Vector3f[] floatsToVector3s(float[] array, int stride) {
		Vector3f[] vecs = new Vector3f[array.length/stride];
		int vecsIndex = 0;
		logger.info("Converting vectors:" + (float) array.length/ (float) stride);
		for (int i = 0; i<array.length; i+=stride) {
			vecs[vecsIndex] = new Vector3f(array[i], array[i+1], array[i+2]);
			vecsIndex++;
		}
		return vecs;
	}

	/**
	 * Reads the contents of a file into a string.
	 * 
	 * @param file
	 *            The file to read and return
	 * @return A string containing the contents of the file
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static String loadFile(File file) throws FileNotFoundException, IOException {
		InputStream in = new FileInputStream(file);
		byte[] b = new byte[in.available()];
		in.read(b);
		in.close();
		return new String(b);
	}
}
