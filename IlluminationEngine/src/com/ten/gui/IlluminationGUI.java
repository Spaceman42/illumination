package com.ten.gui;

import static org.lwjgl.opengl.GL11.*;

import java.awt.FontFormatException;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JComboBox.KeySelectionManager;

import org.lwjgl.input.Mouse;

import com.ten.graphics.GraphicsEngine;
import com.ten.graphics.shader.ShaderManager;
import com.ten.gui.font.AngelCodeFont;
import com.ten.gui.font.Font;
import com.ten.io.Event.EventType;
import com.ten.io.mouse.IOMouse;
import com.ten.io.mouse.MouseEvent;
import com.ten.io.mouse.MouseEventListener;

public class IlluminationGUI {
	private Map<String, Screen> screens;
	private String currentScreen;
	private boolean shadersLoaded;

	public IlluminationGUI() {
		screens = new ConcurrentHashMap<String, Screen>();

		new MouseEventListener(0) {

			@Override
			public void mouseEventPerformed(MouseEvent e) {
				if (e.getType() == EventType.PRESSED) {
					Screen screen = null;
					if (currentScreen != null
							&& (screen = getScreen(currentScreen)) != null) {
						float mouseX = ((float) Mouse.getX() / (float) GraphicsEngine.DISPLAY_WIDTH);
						float mouseY = ((float) Mouse.getY() / (float) GraphicsEngine.DISPLAY_HEIGHT);
						screen.mouseClick(mouseX, mouseY, e.getButton());
					}
				}
			}
		};

	}

	public void handleInput() {
		float mouseX = ((float) Mouse.getX() / (float) GraphicsEngine.DISPLAY_WIDTH);
		float mouseY = ((float) Mouse.getY() / (float) GraphicsEngine.DISPLAY_HEIGHT);
		Screen screen = null;
		if (currentScreen != null
				&& (screen = getScreen(currentScreen)) != null) {
			screen.handleInput(mouseX, mouseY);
		}
	}

	public void render() {
		if (!shadersLoaded) {
			ShaderManager.loadShader("GuiQuad", "res/shaders/gui/GuiQuad");
			ShaderManager.loadShader("ImageShader",
					"res/shaders/gui/ImageShader");
			ShaderManager.loadShader("Text", "res/shaders/gui/Text");
			glDisable(GL_TEXTURE_2D);
			shadersLoaded = true;
		}
		Screen screen = null;
		if (currentScreen != null
				&& (screen = getScreen(currentScreen)) != null) {
			glEnable(GL_BLEND);
			glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
			glDisable(GL_DEPTH_TEST);
			screen.draw();
			glEnable(GL_DEPTH_TEST);
			glDisable(GL_BLEND);
		}

	}

	public void addScreen(Screen screen) {
		screens.put(screen.getName(), screen);
	}

	public void removeScreen(Screen screen) {
		screens.remove(screen.getName());
	}

	public Screen getScreen(String name) {
		return screens.get(name);
	}

	public boolean setCurrentScreen(String name) {
		if (screens.containsKey(name)) {
			this.currentScreen = name;
			return true;
		} else {
			return false;
		}
	}

	public void addScreens(Screen[] screens) {
		for (Screen s : screens) {
			addScreen(s);
		}
	}

	public void addScreens(Collection<Screen> screens) {
		for (Screen s : screens) {
			addScreen(s);
		}
	}
	// TODO: Transition to screen
}
