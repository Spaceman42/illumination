package com.ten.gui.font;

import com.ten.gui.Color;

public interface Font {
	public void drawText(float x, float y, float size, Color color, String text);
}
