package com.ten.gui.font;

import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL11.*;

import java.awt.Rectangle;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import com.ten.graphics.GraphicsEngine;
import com.ten.graphics.renderer.Quad;
import com.ten.graphics.renderer.model.Texture;
import com.ten.graphics.shader.Shader;
import com.ten.graphics.shader.ShaderManager;
import com.ten.graphics.shader.uniform.UniformMatrix4;
import com.ten.graphics.shader.uniform.UniformVector2;
import com.ten.graphics.shader.uniform.UniformVector4;
import com.ten.gui.Color;
import com.ten.gui.shape.RectangleShape;
import com.ten.math.Matrix4;
import com.ten.math.Vector4;
import static org.lwjgl.opengl.GL11.*;

public class AngelCodeFont implements Font {

	private class Glyph {
		private int id;
		private int x;
		private int y;
		private int xoffset;
		private int yoffset;
		private int xadvance;
		private int width;
		private int height;
		private int page;
		private HashMap<Character, Integer> kerning;
		
		public Glyph() {
			kerning = new HashMap<Character, Integer>();
		}

		public int getKerning(Character c) {
			Integer value = kerning.get(c);
			if (value == null) {
				return 0;
			} else {
				return value.intValue();
			}
		}
		
		public void addKerningFor(Character c, int amount) {
			kerning.put(c, amount);
		}
	}

	private int lineHeight;
	private int width;
	private int height;

	private HashMap<Character, Glyph> glyphs;

	private Texture fontTexture;

	public AngelCodeFont(String fntFile, String fontBitmap) {
		glyphs = new HashMap<Character, Glyph>();
		fontTexture = Texture.loadTexture(fontBitmap);
		try {
			parseFont(new File(fntFile));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	@Override
	public void drawText(float x, float y, float size, Color color, String text) {
		float addX = 0;
		Glyph lastGlyph = null;
		for (int i = 0; i < text.length(); i++) {
			Glyph g = glyphs.get(text.charAt(i));
			if (lastGlyph != null) {
				int kerning = lastGlyph.getKerning(Character.valueOf((char)g.id));
				addX += ((float)kerning/(float)GraphicsEngine.DISPLAY_WIDTH)*size*5;
			}
			
			Matrix4 quadMatrix = getQuadMatrix(x+addX, y,
					((float) g.width / GraphicsEngine.DISPLAY_WIDTH),
					((float) g.height / GraphicsEngine.DISPLAY_HEIGHT));
			quadMatrix.scale(size, size, size);
			quadMatrix.translate(addX, 0, 0);
			float sizeX = (float) g.width / (float) this.width;
			float sizeY = (float) g.height / (float) this.height;

			float texLocationX = (float) g.x / (float) this.width;
			float texLocationY = (float) g.y / (float) this.height;
			texLocationY -= 1;
			texLocationY *= -1;

			fontTexture.useTexture(GL_TEXTURE0);

			Shader shader = ShaderManager.getShader("Text");
			shader.useProgram();
			shader.setSampler("image", 0);
			shader.setUniform("mMat", new UniformMatrix4(quadMatrix));
			shader.setUniform("color", new UniformVector4(new Vector4(color.r,
					color.g, color.b, color.a)));
			shader.setUniform("texSize", new UniformVector2(sizeX, sizeY));
			shader.setUniform("texLocation", new UniformVector2(texLocationX,
					texLocationY));
			Quad.getInstance().draw();
			addX += (((float)g.xadvance)/(float)GraphicsEngine.DISPLAY_WIDTH)*size;
			lastGlyph = g;
		}

	}
	
	private float normalizeX(float x) {
		return x/width;
	}
	
	private float normalizeY(float y) {
		return y/height;
	}

	private Matrix4 getQuadMatrix(float x, float y, float width, float height) {
		Matrix4 quadMatrix = new Matrix4();
		float halfExtentX = width;
		float halfExtentY = height;
		quadMatrix.translate(x * 2 - 1, y * 2 - 1, 0);
		quadMatrix.scale(halfExtentX, halfExtentY, 0);
		return quadMatrix;
	}

	private void parseFont(File fntFile) throws IOException {
		String file = loadFile(fntFile);
		Scanner scan = new Scanner(file);
		List<Glyph> glyphList = new ArrayList<Glyph>();
		while (scan.hasNextLine()) {
			String line = scan.nextLine().trim();

			if (line.startsWith("info")) {

			} else if (line.startsWith("common")) {
				HashMap<String, Integer> values = splitToIntTokens(line);
				parseCommon(values);
			} else if (line.startsWith("page")) {

			} else if (line.startsWith("chars")) {

			} else if (line.startsWith("char")) {
				HashMap<String, Integer> values = splitToIntTokens(line);
				parseChar(values);
			} else if (line.startsWith("kernings")) {

			} else if (line.startsWith("kerning")) {
				HashMap<String, Integer> values = splitToIntTokens(line);
				parseKerning(values);
			}
		}
	}

	private void parseKerning(HashMap<String, Integer> values) {
		int first = values.get("first");
		int second = values.get("second");
		int amount = values.get("amount");
		Glyph g = glyphs.get(Character.valueOf((char) first));
		g.addKerningFor(Character.valueOf((char) second), amount);
		
	}

	private void parseCommon(HashMap<String, Integer> values) {
		width = values.get("scaleW");
		height = values.get("scaleH");
		lineHeight = values.get("lineHeight");
	}

	private void parseChar(HashMap<String, Integer> values) {
		Glyph g = new Glyph();
		g.id = values.get("id");
		g.x = values.get("x");
		g.y = values.get("y");
		g.width = values.get("width");
		g.height = values.get("height");
		g.xoffset = values.get("xoffset");
		g.yoffset = values.get("yoffset");
		g.xadvance = values.get("xadvance");
		g.page = values.get("page");
		glyphs.put(Character.valueOf((char) g.id), g);
	}

	private HashMap<String, Integer> splitToIntTokens(String line) {
		HashMap<String, Integer> splitLine = new HashMap<String, Integer>();

		String[] tokens = line.split(" ");
		for (String token : tokens) {
			String[] values = token.split("=");
			if (values.length > 1) {
				splitLine.put(values[0], Integer.valueOf(values[1]));
			} else {
				splitLine.put(values[0], null);
			}
		}
		return splitLine;
	}

	private String loadFile(File file) throws FileNotFoundException,
			IOException {
		BufferedReader br = new BufferedReader(new FileReader(file));
		StringBuilder data = new StringBuilder();
		try {
			String line = null;
			while ((line = br.readLine()) != null) {
				data.append(line).append('\n');
			}
		} finally {
			br.close();
		}
		return data.toString();
	}

}
