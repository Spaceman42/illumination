package com.ten.gui;

public abstract class GuiComponent extends GuiNode{
	private float x;
	private float y;
	private float width;
	private float height;
	private boolean clickable;
	
	
	public abstract void draw();
	public abstract boolean isMouseOver(float mouseX, float mouseY);
	
	public void setLocation(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public void setSize(float width, float height) {
		this.width = width;
		this.height = height;
	}
	
	public void setX(float x) {
		setLocation(x, this.y);
	}
	public void setY(float y) {
		setLocation(this.x, y);
	}
	public void setWidth(float width) {
		setSize(width, this.height);
	}
	public void setHeight(float height){
		setSize(this.width, height);
	}
	
	public float getX() {
		return x;
	}
	public float getY() {
		return y;
	}
	public float getWidth() {
		return width;
	}
	public float getHeight() {
		return height;
	}
	
	public void setClickable(boolean value) {
		clickable = value;
	}
	public boolean isClickable() {
		return clickable;
	}
}

