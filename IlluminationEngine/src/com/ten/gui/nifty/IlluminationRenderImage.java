package com.ten.gui.nifty;

import com.ten.graphics.renderer.model.Texture;

import de.lessvoid.nifty.spi.render.RenderImage;

public class IlluminationRenderImage implements RenderImage{
	Texture texture;
	
	public IlluminationRenderImage(String name) {
		texture = Texture.loadTexture(name);
	}
	
	@Override
	public void dispose() {
		texture.delete();
	}

	@Override
	public int getHeight() {
		return texture.getHeight();
	}

	@Override
	public int getWidth() {
		return texture.getWidth();
	}
	
	public void use(int activeTexture) {
		texture.useTexture(activeTexture);
	}

}
