package com.ten.gui.nifty;

import java.io.IOException;

import com.ten.graphics.GraphicsEngine;
import com.ten.graphics.renderer.Quad;
import com.ten.graphics.shader.Shader;
import com.ten.graphics.shader.ShaderManager;
import com.ten.graphics.shader.uniform.UniformMatrix4;
import com.ten.graphics.shader.uniform.UniformVector3;
import com.ten.math.Matrix4;
import com.ten.math.Vector3;
import com.ten.math.Vector4;

import de.lessvoid.nifty.render.BlendMode;
import de.lessvoid.nifty.renderer.lwjgl.render.LwjglRenderFont;
import de.lessvoid.nifty.spi.render.MouseCursor;
import de.lessvoid.nifty.spi.render.RenderDevice;
import de.lessvoid.nifty.spi.render.RenderFont;
import de.lessvoid.nifty.spi.render.RenderImage;
import de.lessvoid.nifty.tools.Color;
import de.lessvoid.nifty.tools.resourceloader.NiftyResourceLoader;

import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL11.*;

public class IlluminationRenderDevice implements RenderDevice {
	private NiftyResourceLoader loader;

	public IlluminationRenderDevice() {
		ShaderManager.loadShader("ImageShader", "res/shaders/gui/ImageShader");
		ShaderManager.loadShader("GuiQuad", "res/shaders/gui/GuiQuad");
	}

	@Override
	public void beginFrame() {
		glDisable(GL_DEPTH_TEST);

	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub

	}

	@Override
	public RenderFont createFont(String arg0) {
		return new LwjglRenderFont(arg0, this, loader);
	}

	@Override
	public RenderImage createImage(String name, boolean filterLinear) {
		return new IlluminationRenderImage(name);
	}

	@Override
	public MouseCursor createMouseCursor(String arg0, int arg1, int arg2)
			throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void disableClip() {
		// TODO Auto-generated method stub

	}

	@Override
	public void disableMouseCursor() {
		// TODO Auto-generated method stub

	}

	@Override
	public void enableClip(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void enableMouseCursor(MouseCursor arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void endFrame() {
		glEnable(GL_DEPTH_TEST);

	}

	@Override
	public int getHeight() {
		return GraphicsEngine.DISPLAY_HEIGHT;
	}

	@Override
	public int getWidth() {
		return GraphicsEngine.DISPLAY_WIDTH;
	}

	@Override
	public void renderFont(RenderFont arg0, String arg1, int arg2, int arg3,
			Color arg4, float arg5, float arg6) {
		// TODO Auto-generated method stub

	}

	@Override
	public void renderImage(final RenderImage image, final int x, final int y,
			final int width, final int height, final Color color,
			final float scale) {
		IlluminationRenderImage renderImage = (IlluminationRenderImage) image;
		renderImage.use(GL_TEXTURE0);
		Vector4 xyNDC = new Vector4((float) x / (float) getWidth(), (float) y
				/ (float) getHeight(), 0, 1);
		Matrix4 quadMatrix = new Matrix4();

		quadMatrix.translate(xyNDC);
		float xScale = (float) width / (float) getWidth();
		float yScale = (float) height / (float) getHeight();
		quadMatrix.scale(xScale, yScale, 1);
		quadMatrix.scale(scale, scale, 1);

		Shader shader = ShaderManager.getShader("ImageShader");
		shader.useProgram();
		shader.setSampler("image", 0);
		shader.setUniform("mMat", new UniformMatrix4(quadMatrix));
		shader.setUniform("color",
				new UniformVector3(new Vector3(color.getRed(),
						color.getGreen(), color.getBlue())));
		Quad.getInstance().draw();
	}

	@Override
	public void renderImage(final RenderImage image, final int x, final int y,
			final int w, final int h, final int srcX, final int srcY,
			final int srcW, final int srcH, final Color color,
			final float scale, final int centerX, final int centerY) {
		float xNDC = (float)x/(float)getWidth();
		float yNDC = (float)y/(float)getHeight();
		
		Matrix4 quadMatrix = new Matrix4();
		quadMatrix.translate(1, 1, 0);
		quadMatrix.translate(xNDC, yNDC, 0);
		quadMatrix.scale((float)w/(float)getWidth(), (float)h/(float)getHeight(), 1);
		quadMatrix.scale(scale, scale, 1);
		
		IlluminationRenderImage renderImage = (IlluminationRenderImage) image;
		renderImage.use(GL_TEXTURE0);
		
		Shader shader = ShaderManager.getShader("ImageShader");
		shader.useProgram();
		shader.setSampler("image", 0);
		shader.setUniform("mMat", new UniformMatrix4(quadMatrix));
		shader.setUniform("color",
				new UniformVector3(new Vector3(color.getRed(),
						color.getGreen(), color.getBlue())));
		Quad.getInstance().draw();

	}

	@Override
	public void renderQuad(final int x, final int y, final int width, final int height, final Color color) {
		float xNDC = (float)x/(float)getWidth();
		float yNDC = (float)y/(float)getHeight();
		
		Matrix4 quadMatrix = new Matrix4();
		//quadMatrix.translate(0.25f, 0.25f, 0);
		quadMatrix.translate(xNDC/4f, yNDC/4f, 0);
		quadMatrix.scale((float)width/(float)getWidth(), (float)height/(float)getHeight(), 1);
		
		Shader shader = ShaderManager.getShader("GuiQuad");
		shader.useProgram();
		shader.setUniform("mMat", new UniformMatrix4(quadMatrix));
		shader.setUniform("color", new UniformVector3(new Vector3(color.getRed(), color.getGreen(), color.getBlue())));
		Quad.getInstance().draw();
	}

	@Override
	public void renderQuad(int arg0, int arg1, int arg2, int arg3, Color arg4,
			Color arg5, Color arg6, Color arg7) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setBlendMode(BlendMode arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setResourceLoader(NiftyResourceLoader arg0) {
		this.loader = arg0;
	}

}
