package com.ten.gui.nifty;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.batch.BatchRenderDevice;
import de.lessvoid.nifty.batch.spi.BatchRenderBackend;
import de.lessvoid.nifty.nulldevice.NullSoundDevice;
import de.lessvoid.nifty.renderer.lwjgl.input.LwjglInputSystem;
import de.lessvoid.nifty.renderer.lwjgl.render.LwjglRenderDevice;
import de.lessvoid.nifty.renderer.lwjgl.render.batch.LwjglBatchRenderBackend;
import de.lessvoid.nifty.renderer.lwjgl.render.batch.LwjglBatchRenderBackendCoreProfile;
import de.lessvoid.nifty.spi.render.RenderDevice;
import de.lessvoid.nifty.spi.time.impl.AccurateTimeProvider;
@Deprecated
public class NiftyGUIManager {
	private final Nifty nifty;
	
	public NiftyGUIManager() throws Exception {
	
		
		LwjglInputSystem inputSys = new LwjglInputSystem();
		inputSys.startup();
		RenderDevice renderDevice = new BatchRenderDevice(new LwjglBatchRenderBackend(), 2048, 2048);
		nifty = new Nifty(renderDevice, new NullSoundDevice(), inputSys, new AccurateTimeProvider());
	}
	
	public void render() {
		nifty.render(false);
	}
	
	public Nifty getNifty() {
		return nifty;
	}
}
