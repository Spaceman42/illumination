package com.ten.gui.shape;

import static org.lwjgl.opengl.GL13.GL_TEXTURE0;

import com.ten.graphics.renderer.Quad;
import com.ten.graphics.renderer.model.Texture;
import com.ten.graphics.shader.Shader;
import com.ten.graphics.shader.ShaderManager;
import com.ten.graphics.shader.uniform.UniformMatrix4;
import com.ten.graphics.shader.uniform.UniformVector3;
import com.ten.graphics.shader.uniform.UniformVector4;
import com.ten.gui.Color;
import com.ten.gui.nifty.IlluminationRenderImage;
import com.ten.math.Matrix4;
import com.ten.math.Vector3;
import com.ten.math.Vector4;

public class RectangleShape {
	private RectangleShape() {}
	
	/**
	 * Draws a rectangle at the given location of the given size with the given image on it
	 * @param tex - Image to draw
	 * @param color	- The image color is multiplied by this value
	 * @param x - x location of the center of the rectangle
	 * @param y - y location of the center of the rectangle 
	 * @param width - width of the rectangle
	 * @param height - height of the rectangle
	 */
	public static void draw(Texture tex, Color color, float x, float y, float width, float height) {
		Matrix4 quadMatrix = getQuadMatrix(x, y, width, height);
		

		tex.useTexture(GL_TEXTURE0);
		
		Shader shader = ShaderManager.getShader("ImageShader");
		shader.useProgram();
		shader.setSampler("image", 0);
		shader.setUniform("mMat", new UniformMatrix4(quadMatrix));
		shader.setUniform("color", new UniformVector4(new Vector4(color.r, color.g, color.b, color.a)));
		Quad.getInstance().draw();
	}
	/**
	 * Draws a rectangle at the given location of the given size of the given color
	 * @param color - The color of the rectangle
	 * @param x - x location of the center of the rectangle
	 * @param y - y location of the center of the rectangle
	 * @param width - width of the rectangle
	 * @param height - height of the rectangle
	 */
	public static void draw(Color color, float x, float y, float width, float height) {
		Matrix4 quadMatrix = getQuadMatrix(x, y, width, height);
		
		Shader shader = ShaderManager.getShader("GuiQuad");
		shader.useProgram();
		shader.setUniform("mMat", new UniformMatrix4(quadMatrix));
		shader.setUniform("color", new UniformVector4(new Vector4(color.r, color.g, color.b, color.a)));
		Quad.getInstance().draw();
	}
	
	public static boolean pointInside(float pointX, float pointY, float rectangleX, float rectangleY, float rectangleWidth, float rectangleHeight) {
		float x  = rectangleX;
		float y = rectangleY;
		float height = rectangleHeight;
		float width = rectangleWidth;
		
		boolean xInside = false;
		boolean yInside = false;
		
		xInside = (pointX < x+(width/2f)) && (pointX > x - (width/2f)); 
		yInside = (pointY < y+(height/2f)) && (pointY > y - (height/2f)); 
		
		return xInside && yInside;
		
	}
	
	private static Matrix4 getQuadMatrix(float x, float y, float width, float height) {
		Matrix4 quadMatrix = new Matrix4();
		float halfExtentX = width;
		float halfExtentY = height;
		quadMatrix.translate(x*2-1, y*2-1, 0);
		quadMatrix.scale(halfExtentX, halfExtentY, 0);
		return quadMatrix;
	}
}
