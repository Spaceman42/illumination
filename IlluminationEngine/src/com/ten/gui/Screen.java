package com.ten.gui;

public class Screen extends GuiComponent {
	private final String name;
	
	public Screen(String name) {
		this.name = name;
	}
	
	@Override
	public void draw() {
		for (GuiComponent c : this.getChildren()) {
			c.draw();
		}
	}
	
	public void add(GuiComponent c) {
		this.getChildren().add(c);
	}
	
	public void remove(GuiComponent c) {
		this.getChildren().remove(c);
	}

	@Override
	public boolean isMouseOver(float mouseX, float mouseY) {
		return false;
	}
	
	public String getName() {
		return name;
	}

	public void handleInput(float mouseX, float mouseY) {
		for (GuiComponent c : getChildren()) {
			boolean isOver = c.isMouseOver(mouseX, mouseY);
			if(c instanceof ClickableComponent) {
				ClickableComponent cc = (ClickableComponent) c;
				if (isOver) {
					cc.mouseOver(true);
				} else {
					cc.mouseOver(false);
				}
			}
		}
	}
	
	public void mouseClick(float mouseX, float mouseY, int button) {
		for (GuiComponent c : getChildren()) {
			if(c.isMouseOver(mouseX, mouseY) && c instanceof ClickableComponent) {
				ClickableComponent cc = (ClickableComponent) c;
				cc.clickedWithButton(button);
			}
		}
	}

}
