package com.ten.gui;

public class Color {
	public final float r;
	public final float g;
	public final float b;
	public final float a;
	
	public Color(float r, float g, float b, float a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
	
	public Color(float r, float g, float b) {
		this(r,g,b, 1.0f);
	}
	
	public Color(int r, int g, int b, int a) {
		this.r = (float)r/255f;
		this.g = (float)g/255f;
		this.b = (float)b/255f;
		this.a = (float)a/255f;
	}
	
	public Color(int r, int g, int b) {
		this(r,g,b,255);
	}
	
	public Color() {
		this(1.0f, 1.0f, 1.0f, 1.0f);
	}
}
