package com.ten.gui;



import com.ten.graphics.renderer.model.Texture;
import com.ten.gui.shape.RectangleShape;

public abstract class ClickableImageRectangle extends ClickableRectangle{
	
	private Texture image;
	
	public ClickableImageRectangle(Texture image, Color color, float x, float y, float width,
			float height) {
		super(color, x, y, width, height);
		this.image = image;
	}
	
	@Override
	public void draw() {
		RectangleShape.draw(image, getColor(), getX(), getY(), getWidth(), getHeight());
	}
	
	public void setImage(Texture image) {
		this.image = image;
	}

}
