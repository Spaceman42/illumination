package com.ten.gui;

import java.util.ArrayList;
import java.util.List;

public abstract class GuiNode {
	private List<GuiComponent> children;
	
	public GuiNode() {
		children = new ArrayList<GuiComponent>();
	}
	
	public List<GuiComponent> getChildren() {
		return children;
	}
}
