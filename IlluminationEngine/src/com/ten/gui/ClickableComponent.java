package com.ten.gui;

public abstract class ClickableComponent extends GuiComponent{
	private boolean mouseOver;

	
	public ClickableComponent() {
		setClickable(true);
	}
	
	public abstract void mouseOver(boolean value);
	public abstract void clickedWithButton(int button);
}
