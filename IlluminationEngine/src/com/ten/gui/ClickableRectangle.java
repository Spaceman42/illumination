package com.ten.gui;

import com.ten.gui.shape.RectangleShape;

public abstract class ClickableRectangle extends ClickableComponent{
	private Color color;
	
	public ClickableRectangle(Color color, float x, float y, float width, float height) {
		super();
		this.color = color;
		setLocation(x, y);
		setSize(width, height);
	}
	
	@Override
	public void draw() {
		RectangleShape.draw(color, getX(), getY(), getWidth(), getHeight());
	}
	
	public boolean isMouseOver(float mouseX, float mouseY) {
		return RectangleShape.pointInside(mouseX, mouseY, getX(), getY(), getWidth(), getHeight());
	}
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}

}
