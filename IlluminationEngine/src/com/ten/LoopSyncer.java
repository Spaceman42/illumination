package com.ten;

import org.apache.log4j.Logger;

public class LoopSyncer {
	private static final Logger logger = Logger.getLogger(LoopSyncer.class.getName());
	
	private long initTime = 0;
	private long exeTime = 0;
	
	
	public void sync(int fps) {
		exeTime = Math.abs(Utils.getTime() - initTime);
		int timeToWaitFor = 1000 / fps;

		int deltaTime = (int) (timeToWaitFor - exeTime);
		if (deltaTime < 0) {
			System.err.println("Sleeping for: " + deltaTime);
			logger.error("Sleeping for: " + deltaTime);
		}
		if (deltaTime > 0) {
			try {
				Thread.sleep(deltaTime);
			} catch (InterruptedException e) {
			}
		}
		initTime = Utils.getTime(); 
	}

}
