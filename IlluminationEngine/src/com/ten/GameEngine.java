package com.ten;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.RollingFileAppender;
import org.apache.log4j.TTCCLayout;
import org.apache.log4j.WriterAppender;

import com.ten.game.AbstractGame;
import com.ten.gui.IlluminationGUI;

public class GameEngine {

	public static String RESOURCES = "res/";

	private static IlluminationGUI gui = new IlluminationGUI();
	
	private static final String LOG_FILE = "logs/log.txt";
	private AbstractGame game;

	
	public GameEngine(AbstractGame game) {
		this.game = game;
	}
	
	public static void configureLogging() throws IOException {
		File logFile = new File(LOG_FILE);
		if (logFile.exists()) {
			logFile.delete();
		}
		Logger.getRootLogger().addAppender(
				new RollingFileAppender(new TTCCLayout(), LOG_FILE));
		Logger.getRootLogger().addAppender(
				new WriterAppender(new TTCCLayout(), System.out));
	}

	public void run() throws IOException {
		GameEngine.configureLogging();
		Logger.getRootLogger().info("Application Starting");
		AppState.setState(AppState.STARTING);
		
		ThreadManager manager = ThreadManager.getInstance();
		manager.setGame(game);
		manager.startThreads();
	}
	
	public static IlluminationGUI getGui() {
		return gui;
	}
	
}
