package com.ten.physics;

import com.bulletphysics.dynamics.RigidBody;

public interface PhysicalObject {

	public PhysicsRigidBody getRigidBody();
	
	public boolean loaded();
}
