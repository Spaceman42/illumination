package com.ten.physics;

import java.util.Set;

import com.ten.AppState;
import com.ten.LoopSyncer;
import com.ten.request.PhysicsRequest;

public class PhysicsThread extends Thread{
	private PhysicsEngine engine;
	private Set<PhysicalObject> objects;
	private LoopSyncer syncer;
	
	public PhysicsThread() {
		engine = new PhysicsEngine();
		syncer = new LoopSyncer();
		setName("Physics-Thread");
	}
	
	@Override
	public void run() {
		try {
			engine.init();
			AppState.setState(AppState.RUNNING);
			while (AppState.getState() != AppState.SHUTTING_DOWN) {
				engine.run(objects);
				if (AppState.closeRequested) {
					AppState.setState(AppState.SHUTTING_DOWN);
				}
				syncer.sync(60);
			} 
			AppState.setState(AppState.CLOSING);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public void updateObjects(Set<PhysicalObject> objects) {
		this.objects = objects;
	}

	public void makeRequest(PhysicsRequest physicsRequest) {
		engine.makeRequest(physicsRequest);
		
	}
}
