package com.ten.physics;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.vecmath.Vector3f;

import org.apache.log4j.Logger;

import com.bulletphysics.collision.broadphase.BroadphaseInterface;
import com.bulletphysics.collision.broadphase.DbvtBroadphase;
import com.bulletphysics.collision.dispatch.CollisionConfiguration;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.DefaultCollisionConfiguration;
import com.bulletphysics.dynamics.DiscreteDynamicsWorld;
import com.bulletphysics.dynamics.DynamicsWorld;
import com.bulletphysics.dynamics.constraintsolver.ConstraintSolver;
import com.bulletphysics.dynamics.constraintsolver.SequentialImpulseConstraintSolver;
import com.ten.AppState;
import com.ten.Utils;
import com.ten.request.PhysicsRequest;

public class PhysicsEngine {
	private DynamicsWorld world;

	private Set<PhysicalObject> objectRegistry;

	private BlockingQueue<PhysicsRequest> requests;

	private long lastTime;
	
	private static final Logger logger = Logger.getLogger(PhysicsEngine.class.getName());
	
	public PhysicsEngine() {
		requests = new LinkedBlockingQueue<PhysicsRequest>();
	}

	public void init() {
		BroadphaseInterface broadphase = new DbvtBroadphase();
		CollisionConfiguration collisionConfig = new DefaultCollisionConfiguration();
		CollisionDispatcher dispatcher = new CollisionDispatcher(collisionConfig);
		ConstraintSolver solver = new SequentialImpulseConstraintSolver();

		world = new DiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfig);
		world.setGravity(new Vector3f(0f, -9f, 0f));
		objectRegistry = new HashSet<PhysicalObject>();
		
		logger.info("Physics engine loaded!");
	}

	public void run(Set<PhysicalObject> objects) {
		if (AppState.getState() == AppState.RUNNING) {
			if (objects != null) {
				updateRegistry(objects);
			}
			for (CollisionObject o : world.getCollisionObjectArray()) {
				logger.info("Local Scalling: " + o.getCollisionShape().getLocalScaling(new Vector3f()));
			}
			world.stepSimulation(Utils.toSeconds(getDeltaTime()));
			doRequests();
		}
	}

	private void doRequests() {
		PhysicsRequest r;
		while ((r = requests.poll()) != null) {
			logger.info("Doing a request");
			r.doRequest(world);
		}
	}
	
	public void makeRequest(PhysicsRequest r) {
		requests.add(r);
	}

	private void updateRegistry(Set<PhysicalObject> objects) {
		//logger.info("updating physics registry");
		for (PhysicalObject o : objects) {
			if (!objectRegistry.contains(o)) {
				if (o.loaded()) {
					world.addRigidBody(o.getRigidBody().getBody());
					objectRegistry.add(o);
					logger.info("RigidBody " + o.getRigidBody().getBody().getUserPointer() + " loaded into the physics engine");
				}
			}
		}

		for (PhysicalObject o : objectRegistry) {
			if (!objects.contains(o)) {
				world.removeRigidBody(o.getRigidBody().getBody());
				objectRegistry.remove(o);
			}
		}
	}

	private int getDeltaTime() {
		long time = Utils.getTime();
		int delta = (int) (time - lastTime);
		lastTime = time;
		return delta;
	}

	public List<CollisionObject> getCollisions() {
		return world.getCollisionObjectArray();
		
	}
}
