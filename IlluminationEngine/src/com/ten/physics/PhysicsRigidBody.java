package com.ten.physics;

import javax.vecmath.Matrix4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

import org.apache.log4j.Logger;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.MotionState;
import com.bulletphysics.linearmath.Transform;
import com.ten.math.Matrix4;
import com.ten.math.Vector3;
import com.ten.physics.shape.BoundingBox;
import com.ten.physics.shape.BoundingShape;

public class PhysicsRigidBody {
	private static final Logger logger = Logger.getLogger(PhysicsRigidBody.class.getName());
	
	
	private RigidBody body;

	private final Vector3f origin;
	private final Vector3 scale;
	private CollisionShape cShape;
	
	public static final float MASS_INFINITE = 0.0f;

	public PhysicsRigidBody(BoundingShape boundingShape, Vector3 startTranslation,
			Vector3 inertia, Vector3 scale, float mass, float restitution,
			float angularDanpening, PhysicalObject userPointer) {
		
		origin = boundingShape.getOrigin();
		
		cShape = boundingShape.getNewShape();
		this.scale = scale;
		Vector3f scalef = scale.toJavaX();
		//scalef.x /= 1/scalef.x;
		//scalef.y /= 1/scalef.y;
		//scalef.z /= 1/scalef.z;
		///shape.setMargin(1);
		
		logger.info("r-body scalling = " + cShape.getLocalScaling(new Vector3f(5f,5f,5f)));
		Vector3f transVec = startTranslation.toJavaX();
		
		transVec.add(origin);
		
		Matrix4f mat = new Matrix4f(new Quat4f(), transVec, 1);

		Transform transform = new Transform(mat);

		MotionState motionState = new DefaultMotionState(transform);

		Vector3f intertia3f = new Vector3(inertia).toJavaX();

		cShape.calculateLocalInertia(mass, intertia3f);

		RigidBodyConstructionInfo constructionInfo = new RigidBodyConstructionInfo(
				mass, motionState, cShape, intertia3f);
		constructionInfo.restitution = restitution;
		constructionInfo.angularDamping = angularDanpening;

		body = new RigidBody(constructionInfo);
		//body.setActivationState(CollisionObject.DISABLE_DEACTIVATION);
		body.setUserPointer(userPointer);
		//shape.setLocalScaling(scalef);
		cShape.setLocalScaling(scalef);
		logger.info("r-body scalling (2)= " + cShape.getLocalScaling(new Vector3f(5f,5f,5f)));
		
	}

	public RigidBody getBody() {
		return body;
	}

	public Vector3 getTranslation() {
		Transform transform = body.getMotionState().getWorldTransform(
				new Transform());
		Vector3f translation = new Vector3f(transform.origin);
		return new Vector3(translation.x - origin.x, translation.y - origin.y,
				translation.z - origin.z);
	}

	public Vector3 getRotation() {
		return null;
	}

	public void translate(Vector3 translate) {
		body.translate(translate.toJavaX());
	}

	public void setVelocity(Vector3 newVolocity) {
		body.setLinearVelocity(newVolocity.toJavaX());
	}
	
	public boolean isInWorld() {
		return body.isInWorld();
	}

	public void setAngularVelocity(Vector3 axis, float value) {
		body.setAngularVelocity(new Vector3(axis).multiply(value).toJavaX());
	}
	
	public Matrix4 getMatrix() {
		Transform transform = body.getMotionState().getWorldTransform(
				new Transform());
		float[] f = new float[16];
		transform.getOpenGLMatrix(f);
		return new Matrix4(f);
	}
	
}
