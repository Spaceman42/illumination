package com.ten.physics;

import javax.vecmath.Vector3f;

import org.apache.log4j.Logger;

import static org.lwjgl.opengl.GL11.*;

import com.bulletphysics.linearmath.IDebugDraw;
//TODO: Make this work
public class DebugRenderer extends IDebugDraw {

	private static final Logger logger = Logger.getLogger(DebugRenderer.class.getName());
	private int debugMode;

	@Override
	public void draw3dText(Vector3f arg0, String arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void drawContactPoint(Vector3f pointOnB, Vector3f normalOnB, float distance, int lifeTime, Vector3f color) {
		/*
		 * if ((debugMode & DebugDrawModes.DRAW_CONTACT_POINTS) != 0) { Vector3f
		 * to = tmpVec; to.scaleAdd(distance*100f, normalOnB, pointOnB);
		 * Vector3f from = pointOnB;
		 * 
		 * // JAVA NOTE: added if (DEBUG_NORMALS) { to.normalize(normalOnB);
		 * to.scale(10f); to.add(pointOnB); glLineWidth(3f); glPointSize(6f);
		 * glBegin(GL_POINTS); glColor3f(color.x, color.y, color.z);
		 * glVertex3f(from.x, from.y, from.z); glEnd(); }
		 * 
		 * glBegin(GL_LINES); glColor3f(color.x, color.y, color.z);
		 * glVertex3f(from.x, from.y, from.z); glVertex3f(to.x, to.y, to.z);
		 * glEnd();
		 * 
		 * // JAVA NOTE: added if (DEBUG_NORMALS) { gl.glLineWidth(1f);
		 * gl.glPointSize(1f); }
		 */
	}

	@Override
	public void drawLine(Vector3f from, Vector3f to, Vector3f color) {
		if (debugMode > 0) {
			glBegin(GL_LINES);
			glColor3f(color.x, color.y, color.z);
			glVertex3f(from.x, from.y, from.z);
			glVertex3f(to.x, to.y, to.z);
			glEnd();
		}

	}

	@Override
	public int getDebugMode() {
		return debugMode;
	}

	@Override
	public void reportErrorWarning(String arg0) {
		logger.error(arg0);
	}

	@Override
	public void setDebugMode(int debugMode) {
		this.debugMode = debugMode;
	}

}
