package com.ten.physics.shape;

import javax.vecmath.Vector3f;

import org.apache.log4j.Logger;

import com.bulletphysics.collision.shapes.CollisionShape;

public class BoundingMesh {
	private static final Logger logger = Logger.getLogger(BoundingMesh.class.getName());
	
	
	private final float[] vertices;
	private final int[] indices;
	private final int stride;
	private final int numVertices;
	
	public BoundingMesh(float[] vertices, int[] indices, int vertexSize) {
		this.vertices = vertices;
		this.indices = indices;
		stride = vertexSize;
		numVertices = vertices.length/stride;
		logger.info("there are " + numVertices + " vertices");
	}
	
	public float[] getVertices() {
		return vertices;
	}
	
	public int[] getIndices() {
		return indices;
	}
	
	public int getStride() {
		return stride;
	}
	
	public int getVertNumber() {
		return numVertices;
	}
	
	public Vector3f[] asVectorArray() {
		
		Vector3f[] array = new Vector3f[indices.length];
		Vector3f[] tmpArray = new Vector3f[numVertices];
		logger.info("new array created with length of: " + indices.length);
		logger.info("temp array created with length of: " + numVertices);
		int tmpArrayLoc = 0;
		for (int i = 0; i<numVertices*stride; i+=stride) {
			Vector3f vec = new Vector3f();
			vec.x = vertices[i];
			vec.y = vertices[i+1];
			vec.z = vertices[i+2];
			tmpArray[tmpArrayLoc] = vec;
			if (vec == null || tmpArray[tmpArrayLoc] == null) {
				System.err.println("null vector was just put in array");
				System.exit(-1);
			}
			tmpArrayLoc++;
		}
		
		for (int i = 0; i<indices.length; i++) {
			if (tmpArray[indices[i]] == null) {
				System.err.println("null vector was found while sorting arrays at index: " + i + " and vertex index: " + indices[i]);
				System.exit(-1);
			}
			array[i] = tmpArray[indices[i]];
		}
		
		for (Vector3f v : array) {
			if (v == null) {
				System.err.println("null vector found in array");
				System.exit(-1);
			}
		}
		return array;
	}
}
