package com.ten.physics.shape;

import java.util.Arrays;


import javax.vecmath.Vector3f;

import org.apache.log4j.Logger;

import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.ConvexHullShape;
import com.bulletphysics.collision.shapes.ShapeHull;
import com.bulletphysics.util.ObjectArrayList;

public class ConvexBoundingShape implements BoundingShape{
	private static final Logger logger = Logger.getLogger(ConvexBoundingShape.class.getName());
	
	private static final int VERTEX_SIZE = 4;
	
	private Vector3f[] mesh;
	
	public ConvexBoundingShape(Vector3f[] mesh) {
		this.mesh = mesh;
	}
	
	@Override
	public CollisionShape getNewShape() {
		Vector3f[] verts = mesh;
		logger.info("Vertex array for bb is " + verts);
		ObjectArrayList<Vector3f> list = new ObjectArrayList<Vector3f>(verts.length);
		list.addAll(Arrays.asList(verts));
		ConvexHullShape oConvexHull =  new ConvexHullShape(list);
		ShapeHull hull = new ShapeHull(oConvexHull);
		hull.buildHull(0);
		return new ConvexHullShape(hull.getVertexPointer());
	}

	@Override
	public Vector3f getOrigin() {
		return new Vector3f();
	}

}
