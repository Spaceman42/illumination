package com.ten.physics.shape;

import javax.vecmath.Vector3f;

import com.bulletphysics.collision.shapes.CollisionShape;

public interface BoundingShape {
	public CollisionShape getNewShape();
	public Vector3f getOrigin();
	
	
}
