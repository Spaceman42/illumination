package com.ten.physics.shape;

public class BoxConstructionInfo implements ShapeConstructionInfo{

	@Override
	public BoundingShape constructShape(float[] vertices, int[] indices) {
		
		return BoundingBox.generateAABB(vertices);
	}
	
}
