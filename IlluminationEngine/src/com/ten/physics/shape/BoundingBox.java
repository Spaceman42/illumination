package com.ten.physics.shape;

import javax.vecmath.Vector3f;

import org.apache.log4j.Logger;

import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.ten.graphics.renderer.Quad;
import com.ten.math.Matrix4;
import com.ten.math.Vector3;

public class BoundingBox implements BoundingShape{
	public final float extentX;
	public final float extentY;
	public float extentZ;
	
	public final float halfExtentX;
	public final float halfExtentY;
	public final float halfExtentZ;
	
	public final float originX;
	public final float originY;
	public final float originZ;
	
	private static final Logger logger = Logger.getLogger(BoundingBox.class.getName());
	
	private BoundingBox(float extentX, float extentY, float extentZ, float originX, float originY, float originZ) {
		this.extentX = extentX;
		this.extentY = extentY;
		this.extentZ = extentZ;
		
		halfExtentX = extentX/2f;
		halfExtentY = extentY/2f;
		halfExtentZ = extentZ/2f;
		
		this.originX = originX;
		this.originY = originY;
		this.originZ = originZ;
		logger.info("new BB created with origin: " + this.originX + ", " + this.originY + ", " + this.originZ + '\n' +
				"and half extents: " + this.halfExtentX + ", " + this.halfExtentY + ", " + this.halfExtentZ);
		
	}
	
	
	public BoundingBox(BoundingBox bb) {
		extentX = bb.extentX;
		extentY = bb.extentY;
		extentZ = bb.extentZ;
		
		halfExtentX = bb.halfExtentX;
		halfExtentY = bb.halfExtentY;
		halfExtentZ = bb.halfExtentZ;
		
		originX = bb.originX;
		originY = bb.originY;
		originZ = bb.originZ;
	}
	
	public String toString() {
		return "Bounding Box: " + '\n' 
				+ "Extents: " + extentX + ", " + extentY + ", " + extentZ + '\n' 
				+ "Origin: " + originX + ", " + originY + ", " + + originZ; 
	}

	/**
	 * Assumes vertices are stored with 4 floats
	 * @param vertices
	 * @return
	 */
	public static BoundingBox generateAABB(float[] vertices) {
		float extentX = 0f;
		float extentY = 0f;
		float extentZ = 0f;
		
		float maxX = 0f;
		float minX = 0f;
		
		float maxY = 0f;
		float minY = 0f;
		
		float maxZ = 0f;
		float minZ = 0f;
		
		for (int i = 0; i<vertices.length; i+=4) {
			float x = vertices[i];
			float y = vertices[i+1];
			float z = vertices[i+2];
			
			if (x > maxX) {
				maxX = x;
			} else if (x<minX) {
				minX = x;
			}
			
			if (y > maxY) {
				maxY = y;
			} else if (y<minY) {
				minY = y;
			}
			
			if (z > maxZ) {
				maxZ = z;
			} else if (z < minZ) {
				minZ = z;
			}
		}
		
		extentX = maxX - minX;
		extentY = maxY - minY;
		extentZ = maxZ - minZ;
		
		float originX = (maxX + minX)/2f;
		float originY = (maxY + minY)/2f;
		float originZ = (maxZ + minZ)/2f;
		logger.info("bounding box origin is: " + originX + ", " + originY + ", " + originZ);
		
		return new BoundingBox(extentX, extentY, extentZ, originX, originY, originZ);
	}


	@Override
	public CollisionShape getNewShape() {
		return new BoxShape(new Vector3f(halfExtentX, halfExtentY, halfExtentZ));
	}


	@Override
	public Vector3f getOrigin() {
		return new Vector3f(originX, originY, originZ);
	}

}
