package com.ten.physics.shape;
import java.nio.Buffer;
import java.nio.ByteBuffer;

import javax.vecmath.Vector3f;

import com.bulletphysics.collision.shapes.BvhTriangleMeshShape;
import com.bulletphysics.collision.shapes.ByteBufferVertexData;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.StridingMeshInterface;
import com.bulletphysics.collision.shapes.TriangleIndexVertexArray;
import com.ten.FloatByteBuffer;
import com.ten.IntByteBuffer;
public class StaticConcaveShape implements BoundingShape{
	private float[] vertices;
	private int[] indices;
	
	public StaticConcaveShape(float[] vertices, int[] indices) {
		this.vertices = vertices;
		this.indices = indices;
	}

	@Override
	public CollisionShape getNewShape() {
		FloatByteBuffer verticesBuffer = new FloatByteBuffer(vertices);
		IntByteBuffer indicesBuffer = new IntByteBuffer(indices);
		
		StridingMeshInterface stridingMesh = new TriangleIndexVertexArray(indices.length/3, indicesBuffer.getRawBuffer(), 4*3, 
				vertices.length/4, verticesBuffer.getRawBuffer(), 4*4);
		return new BvhTriangleMeshShape(stridingMesh, true);
	}

	@Override
	public Vector3f getOrigin() {
		return new Vector3f();
	}
	
	
	
}
