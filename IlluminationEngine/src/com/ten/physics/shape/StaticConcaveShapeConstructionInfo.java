package com.ten.physics.shape;

public class StaticConcaveShapeConstructionInfo implements ShapeConstructionInfo{

	@Override
	public BoundingShape constructShape(float[] vertices, int[] indices) {
		return new StaticConcaveShape(vertices, indices);
	}

}
