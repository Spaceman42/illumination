package com.ten.physics.shape;

public interface ShapeConstructionInfo {
	public BoundingShape constructShape(float[] vertices, int[] indices);
}
