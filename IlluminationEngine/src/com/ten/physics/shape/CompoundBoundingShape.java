package com.ten.physics.shape;

import javax.vecmath.Matrix4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

import org.apache.log4j.Logger;

import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.CompoundShape;
import com.bulletphysics.linearmath.Transform;

public class CompoundBoundingShape implements BoundingShape{
	private static final Logger logger = Logger.getLogger(CompoundBoundingShape.class.getName());
	
	private CollisionShape[] shapes;
	private Vector3f[] locations;
	
	public CompoundBoundingShape(CollisionShape[] shapes, Vector3f[] locations) {
		this.locations = locations;
		this.shapes = shapes;
	}
	
	@Override
	public CollisionShape getNewShape() {
		CompoundShape shape = new CompoundShape();
		for (int i = 0; i<shapes.length; i++) {
			shape.addChildShape(new Transform(new Matrix4f(new Quat4f(), locations[i], 1f)), shapes[i]);
		}
		return shape;
	}

	@Override
	public Vector3f getOrigin() {
		return new Vector3f();
	}
	
	public static CompoundBoundingShape generateShape(BoundingShape[] shapes, Vector3f[] locations) {
		CollisionShape[] collisionShapes = new CollisionShape[shapes.length];
		for (int i = 0; i<shapes.length; i++) {
			collisionShapes[i] = shapes[i].getNewShape();
		}
		logger.info("There are: " + collisionShapes.length + "collision shapes");
		return new CompoundBoundingShape(collisionShapes, locations);
	}
	
	public static Vector3f findCenter(Vector3f[] mesh) {
		Vector3f min = new Vector3f(0,0,0);
		Vector3f max = new Vector3f(0,0,0);
		
		for (Vector3f v : mesh) {
			if (v.x > max.x) {
				max.x = v.x;
			} 
			if (v.x < min.x) {
				min.x = v.x;
			}
			
			if (v.y > max.y) {
				max.y = v.y;
			} 
			if (v.y < min.y) {
				min.y = v.y;
			}
			
			if (v.z > max.z) {
				max.z = v.z;
			} 
			if (v.z < min.z) {
				min.z = v.z;
			}
		}
		Vector3f center = new Vector3f(0,0,0);
		center.add(min);
		center.add(max);
		center.scale(0.5f);
		return center;
	}
}
