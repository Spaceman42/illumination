package com.ten.physics.shape;

import javax.vecmath.Vector3f;

public class Utils {
	private Utils() {}
	
	public static Vector3f[] expandVertices(float[] vertices, int[] indices, int vertexSize) {
		int numVertices = vertices.length/vertexSize;
		
		
		Vector3f[] array = new Vector3f[indices.length];
		Vector3f[] tmpArray = new Vector3f[numVertices];
	
		int tmpArrayLoc = 0;
		for (int i = 0; i<numVertices*vertexSize; i+=vertexSize) {
			Vector3f vec = new Vector3f();
			vec.x = vertices[i];
			vec.y = vertices[i+1];
			vec.z = vertices[i+2];
			tmpArray[tmpArrayLoc] = vec;
			if (vec == null || tmpArray[tmpArrayLoc] == null) {
				System.err.println("null vector was just put in array");
				System.exit(-1);
			}
			tmpArrayLoc++;
		}
		
		for (int i = 0; i<indices.length; i++) {
			if (tmpArray[indices[i]] == null) {
				System.err.println("null vector was found while sorting arrays at index: " + i + " and vertex index: " + indices[i]);
				System.exit(-1);
			}
			array[i] = tmpArray[indices[i]];
		}
		
		for (Vector3f v : array) {
			if (v == null) {
				System.err.println("null vector found in array");
				System.exit(-1);
			}
		}
		return array;
	}
}
