package com.ten.physics.shape;

import javax.vecmath.Vector3f;

public class ConvexShapeConstructionInfo implements ShapeConstructionInfo{

	private static final int VERTEX_SIZE = 4;

	@Override
	public BoundingShape constructShape(float[] vertices, int[] indices) {
		Vector3f[] mesh = Utils.expandVertices(vertices, indices, VERTEX_SIZE);
		return new ConvexBoundingShape(mesh);
	}

}
