package physics;

public class PointChecker {
	
	/*
	 * My Theory
	 * 
	 * Check if the point is enclosed by using a ray and determining if its intersection triangle is facing away or towards.
	 * 
	 * 1. Project every triangle in the mesh onto the xy plane.
	 * 2. Iterate through dense-packing points
	 * 3. If the point lies inside a triangle (use the cross product on the distances to the triangle points)
	 * 	  then it lies on the line
	 * 4. Check if the cross product of the point and the 3d triangles works out and determine the point's status
	 */

}
