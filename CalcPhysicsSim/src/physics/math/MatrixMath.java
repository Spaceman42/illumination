package physics.math;

/**
 * @author David Gronlund
 */
public class MatrixMath {

    public static void reducedRowEchelonForm(float[] m, float[] v) {
        if (!isSquare(m)) {
            return;
        }
        int s = (int) Math.sqrt(m.length);
        for (int c = 0; c < s - 1; c++) {
            for (int r = s - 1; r > c; r--) {
                float scalar = -get(m, r - 1, c) / get(m, r, c);
                rowMultiply(m, scalar, r);
                v[r] *= scalar;
                rowAdd(m, r - 1, r);
                v[r] += v[r - 1];
            }
        }
        for (int c = s - 1; c > 0; c--) {
            for (int r = c - 1; r >= 0; r--) {
                float scalar = -get(m, c, c) / get(m, r, c);
                rowMultiply(m, scalar, r);
                v[r] *= scalar;
                rowAdd(m, c, r);
                v[r] += v[c];
            }
        }
        for (int i = 0; i < s; i++) {
            if (get(m, i, i) != 1.0f) {
                float scalar = 1.0f / get(m, i, i);
                rowMultiply(m, scalar, i);
                v[i] *= scalar;
                
            }
        }
    }

    public static float[] inverse(float[] m) {
        if (!isSquare(m)) {
            return null;
        }
        int s = (int) Math.sqrt(m.length);
        float[] mi = new float[m.length];
        for (int i = 0; i < s; i++) {
            mi[(i * s) + i] = 1.0f;
        }

        for (int c = 0; c < s - 1; c++) {
            for (int r = s - 1; r > c; r--) {
                float scalar = -get(m, r - 1, c) / get(m, r, c);
                rowMultiply(m, scalar, r);
                rowMultiply(mi, scalar, r);
                rowAdd(m, r - 1, r);
                rowAdd(mi, r - 1, r);
            }
        }
        for (int c = s - 1; c > 0; c--) {
            for (int r = c - 1; r >= 0; r--) {
                float scalar = -get(m, c, c) / get(m, r, c);
                rowMultiply(m, scalar, r);
                rowMultiply(mi, scalar, r);
                rowAdd(m, c, r);
                rowAdd(mi, c, r);
            }
        }
        for (int i = 0; i < s; i++) {
            if (get(m, i, i) != 1.0f) {
                float scalar = 1.0f / get(m, i, i);
                rowMultiply(m, scalar, i);
                rowMultiply(mi, scalar, i);
                
            }
        }
        return mi;
    }

    public static boolean isSquare(float[] m) {
        return Math.sqrt(m.length) % 1.0 == 0.0;
    }

    public static void rowMultiply(float[] m, float f, int r) {
        if (!isSquare(m)) {
            return;
        }
        int s = (int) Math.sqrt(m.length);
        for (int i = 0; i < s; i++) {
            m[(i * s) + r] *= f;
        }
    }

    public static void rowAdd(float[] m, int operand, int result) {
        if (!isSquare(m)) {
            return;
        }
        int s = (int) Math.sqrt(m.length);
        for (int i = 0; i < s; i++) {
            m[(i * s) + result] += m[(i * s) + operand];
        }
    }

    public static float get(float[] m, int r, int c) {
        if (!isSquare(m)) {
            return 0.0f;
        }
        int s = (int) Math.sqrt(m.length);
        return m[(s * c) + r];
    }

    private static void print(float[] m) {
        if (!isSquare(m)) {
            return;
        }
        int s = (int) Math.sqrt(m.length);
        for (int r = 0; r < s; r++) {
            for (int c = 0; c < s; c++) {
                System.out.print(m[(c * s) + r] + " ");
            }
            System.out.println();
        }
    }
}
