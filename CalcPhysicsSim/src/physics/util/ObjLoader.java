package physics.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class ObjLoader {
	
	private static ArrayList<Float> vertices = new ArrayList<Float>();
	private static ArrayList<Float> textures = new ArrayList<Float>();
	private static ArrayList<Float> normals = new ArrayList<Float>();
	private static ArrayList<Integer> indices = new ArrayList<Integer>();
	
	public static void main(String[] args) throws FileNotFoundException {
		Scanner s = new Scanner(new FileInputStream("res/trussed_support.obj"));
		while (s.hasNextLine()) { // Loops through all lines in the file
			String l = s.nextLine();
			if (l.contains("v ")) { // If the line represents a vertex
				int index1 = l.indexOf(' '); // Finds the first space
				while (l.charAt(++index1) == ' ') { // Moves forward until the first number
				}
				int index2 = l.indexOf(' ', index1); // Finds the next space after the number
				vertices.add(Float.parseFloat(l.substring(index1, index2))); // Adds entry
				index1 = index2; // Repeat
				while (l.charAt(++index1) == ' ') {
				}
				index2 = l.indexOf(' ', index1);
				vertices.add(Float.parseFloat(l.substring(index1, index2)));
				index1 = index2;
				while (l.charAt(++index1) == ' ') {
				}
				vertices.add(Float.parseFloat(l.substring(index1).trim()));
			} else if (l.contains("vt ")) {
				int index1 = l.indexOf(' '); // Finds the first space
				while (l.charAt(++index1) == ' ') { // Moves forward until the first number
				}
				int index2 = l.indexOf(' ', index1); // Finds the next space after the number
				textures.add(Float.parseFloat(l.substring(index1, index2))); // Adds entry
				index1 = index2; // Repeat
				while (l.charAt(++index1) == ' ') {
				}
				index2 = l.indexOf(' ', index1);
				textures.add(Float.parseFloat(l.substring(index1, index2)));
			} else if (l.contains("vn ")) {
				int index1 = l.indexOf(' '); // Finds the first space
				while (l.charAt(++index1) == ' ') { // Moves forward until the first number
				}
				int index2 = l.indexOf(' ', index1); // Finds the next space after the number
				normals.add(Float.parseFloat(l.substring(index1, index2))); // Adds entry
				index1 = index2; // Repeat
				while (l.charAt(++index1) == ' ') {
				}
				index2 = l.indexOf(' ', index1);
				normals.add(Float.parseFloat(l.substring(index1, index2)));
				index1 = index2;
				while (l.charAt(++index1) == ' ') {
				}
				normals.add(Float.parseFloat(l.substring(index1).trim()));
			} else if (l.contains("f ")) {
				int index1 = l.indexOf(' '); // Finds the first space
				while (l.charAt(++index1) == ' ') { // Moves forward until the first number
				}
				int index2 = l.indexOf(' ', index1); // Finds the next space after the number
				String[] temp = l.substring(index1, index2).split("/");
				indices.add(Integer.parseInt(temp[0])); // Adds entry
				indices.add(Integer.parseInt(temp[1]));
				indices.add(Integer.parseInt(temp[2]));
				index1 = index2; // Repeat
				while (l.charAt(++index1) == ' ') {
				}
				index2 = l.indexOf(' ', index1);
				temp = l.substring(index1, index2).split("/");
				indices.add(Integer.parseInt(temp[0])); // Adds entry
				indices.add(Integer.parseInt(temp[1]));
				indices.add(Integer.parseInt(temp[2]));
				index1 = index2;
				while (l.charAt(++index1) == ' ') {
				}
				temp = l.substring(index1).trim().split("/");
				indices.add(Integer.parseInt(temp[0])); // Adds entry
				indices.add(Integer.parseInt(temp[1]));
				indices.add(Integer.parseInt(temp[2]));
			} else {
			}
			
		}
		s.close();
		System.out.println(indices.size() / 9.0);
		System.out.println(vertices.size() / 3.0);
		System.out.println(textures.size() / 2.0);
		System.out.println(normals.size() / 3.0);
	}
}
