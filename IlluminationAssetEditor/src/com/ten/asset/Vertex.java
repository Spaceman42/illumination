package com.ten.asset;

import com.ten.math.Vector3;

public class Vertex {
	private Vector3 pos;
	private Vector2 uv;
	
	public Vertex(Vector3 pos, Vector2 uv) {
		this.pos = new Vector3(pos);
		this.uv = new Vector2(uv);
	}
	
	public Vector3 getPos() {
		return new Vector3(pos);
	}
	
	public Vector2 getUv() {
		return new Vector2(uv);
	}
}
