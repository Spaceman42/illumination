package com.ten.asset;



import com.ten.math.Vector3;

public class Utils {
	public static Vector3index[] expandVector3Array(float[] vectors, int vectorSize, int[] indices) {
		int numVectors = vectors.length/vectorSize;
		
		
		Vector3index[] array = new Vector3index[indices.length];
		Vector3index[] tmpArray = new Vector3index[numVectors];
	
		int tmpArrayLoc = 0;
		for (int i = 0; i<numVectors*vectorSize; i+=vectorSize) {
			Vector3index vec = new Vector3index();
			vec.x = vectors[i];
			vec.y = vectors[i+1];
			vec.z = vectors[i+2];
			tmpArray[tmpArrayLoc] = vec;
			vec.setIndex(tmpArrayLoc);
			if (vec == null || tmpArray[tmpArrayLoc] == null) {
				System.err.println("null vector was just put in array");
				System.exit(-1);
			}
			tmpArrayLoc++;
		}
		
		for (int i = 0; i<indices.length; i++) {
			if (tmpArray[indices[i]] == null) {
				System.err.println("null vector was found while sorting arrays at index: " + i + " and vertex index: " + indices[i]);
				System.exit(-1);
			}
			array[i] = tmpArray[indices[i]];
		}
		
		for (Vector3 v : array) {
			if (v == null) {
				System.err.println("null vector found in array");
				System.exit(-1);
			}
		}
		return array;
	}
	
	public static Vector2[] expandVector2Array(float[] vectors, int vectorSize, int[] indices) {
	int numVectors = vectors.length/vectorSize;
		
		
		Vector2[] array = new Vector2[indices.length];
		Vector2[] tmpArray = new Vector2[numVectors];
	
		int tmpArrayLoc = 0;
		for (int i = 0; i<numVectors*vectorSize; i+=vectorSize) {
			Vector2 vec = new Vector2();
			vec.x = vectors[i];
			vec.y = vectors[i+1];
			tmpArray[tmpArrayLoc] = vec;
			if (vec == null || tmpArray[tmpArrayLoc] == null) {
				System.err.println("null vector was just put in array");
				System.exit(-1);
			}
			tmpArrayLoc++;
		}
		
		for (int i = 0; i<indices.length; i++) {
			if (tmpArray[indices[i]] == null) {
				System.err.println("null vector was found while sorting arrays at index: " + i + " and vertex index: " + indices[i]);
				System.exit(-1);
			}
			array[i] = tmpArray[indices[i]];
		}
		
		for (Vector2 v : array) {
			if (v == null) {
				System.err.println("null vector found in array");
				System.exit(-1);
			}
		}
		return array;
	}
}
