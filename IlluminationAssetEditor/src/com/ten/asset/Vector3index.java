package com.ten.asset;

import com.ten.math.Vector3;

public class Vector3index extends Vector3{
	private int index;
	
	public void setIndex(int i) {
		index = i;
	}
	
	public int getIndex() {
		return index;
	}
}
