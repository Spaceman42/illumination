package com.ten.asset;

import com.ten.math.Vector3;

public class Vector2 {
	public float x;
	public float y;
	
	public Vector2() {
		
	}
	public Vector2(float[] fs) {
		x = fs[0];
		y = fs[1];
	}

	public Vector2(Vector2 uv1) {
		x = uv1.x;
		y = uv1.y;
	}
	public Vector2 sub(Vector2 uv1) {
		x -= uv1.x;
		y -= uv1.y;
		return this;
	}
	
	public Vector2 add(Vector2 vec) {
		x += vec.x;
		y += vec.y;
		return this;
	}
	
	public Vector2 mul(Vector2 vec) {
		x *= vec.x;
		y *= vec.y;
		return this;
	}
	
	@Override
	public String toString() {
		return "Vector2: [" + x + ", " + y + "]";
	}
}
