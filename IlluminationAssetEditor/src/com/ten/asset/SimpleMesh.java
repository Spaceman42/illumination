package com.ten.asset;

public class SimpleMesh {
	public float[] vertices;
	public float[] textures;
	public float[] normals;
	public float[] tangents;
	public float[] bitangents;
	public int[] indices;

	public boolean hasTextures;
	public String id;

	public SimpleMesh(float[] vertices, float[] textures, float[] normals,
			float[] tangents, float[] bitangents, int[] indices,
			boolean hasTextures, String id) {
		this.vertices = vertices;
		this.textures = textures;
		this.normals = normals;
		this.tangents = tangents;
		this.bitangents = bitangents;
		this.indices = indices;
		
		this.hasTextures = hasTextures;
		this.id = id;

	}
}
