package com.ten.inteface;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.ten.Utils;
import com.ten.asset.AssetMesh;
import com.ten.asset.ModelLoader;
import com.ten.game.AbstractGame;
import com.ten.game.DebugCamera;
import com.ten.game.GameObject;
import com.ten.game.Scene;
import com.ten.graphics.GraphicsEngine;
import com.ten.graphics.renderer.light.PointLight;
import com.ten.io.keyboard.IOKeyboard;
import com.ten.io.keyboard.Key;
import com.ten.math.Vector3;
import com.ten.math.Vector4;
import com.ten.physics.PhysicalObject;

public class Interface extends AbstractGame {
	private DebugCamera camera;
	private Scene scene;
	private String objectLocation;
	private long lastClick = 0;

	public Interface(String objectLocation) {
		camera = new DebugCamera(1.0f, 500f, 45,
				(float) GraphicsEngine.DISPLAY_WIDTH
						/ (float) GraphicsEngine.DISPLAY_HEIGHT);
		scene = new Scene("scene");
		setCamera(camera);
		setScene(scene);

		this.objectLocation = objectLocation;
		scene.add(new EditorObject(objectLocation, new Vector4(0, 0,
				0, 1), "EditorObject"));

		new PointLight(new Vector3(0, 0, 8), new Vector4(1, 1, 1, 20));
	}
	
	public Interface() {
		new SwingWindow(this);
		camera = new DebugCamera(1.0f, 500f, 45,
				(float) GraphicsEngine.DISPLAY_WIDTH
						/ (float) GraphicsEngine.DISPLAY_HEIGHT);
		setCamera(camera);
		scene = new Scene("scene");
		setScene(scene);
	}
	
	@Override
	public void onPause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(Object objectId, int screenX, int screenY,
			Vector4 worldPosition) {
		if (Math.abs(Utils.getTime() - lastClick) > 100) {
			GameObject o = (GameObject) objectId;
			o.onClick(worldPosition);
			scene.add(new TestbedBox(0.01f, worldPosition));
		}
		lastClick = Utils.getTime();

	}

	@Override
	public void onLoop(int deltaTime) {
		GraphicsEngine.readSetting("SSAO").setToDisabled();
		camera.handleInput(deltaTime);
		scene.act();
		if (IOKeyboard.isKeyDown(Key.KEY_F)) {
			File file = new File(objectLocation + ".ilm_a");
			if (!file.exists()) {
				for (GameObject object : scene.getChildren()) {
					if (object instanceof EditorObject) {
						EditorObject editorObject = (EditorObject) object;
						editorObject.save(file);
					}
				}
			}
		}
		
		System.out.println("inLoop called");
	}
	
	public void loadOBJ(File file) {
		Map<String, AssetMesh> meshs = ModelLoader.loadModel(file);
		File saveTo = new File("temp/model.ilm");
		try {
			saveTo.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		CompundEditorObjectUtil.saveCompoundILM(saveTo, meshs);
		scene.add(new CompoundEditorObject("temp/model.ilm"));
	}
	
	public void loadILM(File file) {
		
	}
	
	public void saveILM(File file) {
		
	}

}
