package com.ten.inteface;

import com.ten.game.GameObject;
import com.ten.graphics.RenderableObject;
import com.ten.graphics.renderer.model.CompoundModel;
import com.ten.graphics.renderer.model.Model;
import com.ten.graphics.shader.Shader;
import com.ten.math.Matrix4;
import com.ten.math.Vector3;
import com.ten.math.Vector4;
import com.ten.physics.PhysicalObject;
import com.ten.physics.PhysicsRigidBody;
import com.ten.physics.shape.BoundingShape;
import com.ten.physics.shape.StaticConcaveShapeConstructionInfo;

public class CompoundEditorObject implements GameObject, PhysicalObject, RenderableObject{
	Model model;
	PhysicsRigidBody rigidBody;
	public CompoundEditorObject(String location) {
		model = CompoundModel.load(location, "res/material.mtl", new StaticConcaveShapeConstructionInfo());
	}

	@Override
	public void act() {
		if (model.loaded()) {
			rigidBody = new PhysicsRigidBody(model.getBBs()[0], new Vector3(), new Vector3(), new Vector3(), 0f,0f,0f,this);
		}
		
	}

	@Override
	public void delete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(Vector4 worldLocation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public PhysicsRigidBody getRigidBody() {
		return rigidBody;
	}

	@Override
	public boolean loaded() {
		return model.loaded() && rigidBody != null;
	}

	@Override
	public void draw(Shader shader) {
		model.draw(shader, 0);
		
	}

	@Override
	public Matrix4 getModelMatrix() {
		return new Matrix4();
	}

	@Override
	public BoundingShape[] getBBs() {
		return model.getBBs();
	}
	
}
