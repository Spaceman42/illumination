package com.ten.inteface;

import com.ten.game.GameObject;
import com.ten.graphics.RenderableObject;
import com.ten.graphics.renderer.Cube;
import com.ten.graphics.shader.Shader;
import com.ten.graphics.shader.uniform.UniformNotFoundException;
import com.ten.graphics.shader.uniform.UniformVector3;
import com.ten.math.Matrix4;
import com.ten.math.Vector3;
import com.ten.math.Vector4;
import com.ten.physics.shape.BoundingBox;

public class TestbedBox implements GameObject, RenderableObject {

	private Vector4 translation;
	private float scale;
	
	public TestbedBox(float scale, Vector4 translation) {
		this.translation = new Vector4(translation);
		this.scale = scale;
	}
	
	
	@Override
	public void draw(Shader shader) {
		if (shader != null) {
			shader.setUniform("colorIN", new UniformVector3(new Vector3(1,1,1)));
		}
		Cube.getInstance().draw();
	}

	@Override
	public Matrix4 getModelMatrix() {
		return new Matrix4().translate(translation).scale(scale, scale, scale);
	}

	@Override
	public BoundingBox[] getBBs() {
		return null;
	}

	@Override
	public void act() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(Vector4 worldLocation) {
		// TODO Auto-generated method stub
		
	}

	

}
