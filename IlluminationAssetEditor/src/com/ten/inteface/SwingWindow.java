package com.ten.inteface;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;

import com.ten.graphics.renderer.light.Light;
import com.ten.graphics.renderer.light.PointLight;
import com.ten.io.Event.EventType;
import com.ten.io.mouse.MouseEvent;
import com.ten.io.mouse.MouseEventListener;
import com.ten.math.Vector3;
import com.ten.math.Vector4;

public class SwingWindow {
	private final JFrame frame;
	JFileChooser openChooser = new JFileChooser();
	JFileChooser saveChooser = new JFileChooser();
	
	public static void main(String[] args) {
		SwingWindow w = new SwingWindow(null);
	}
	
	public SwingWindow(final Interface parent) {
		saveChooser.setFileFilter(new FileFilter() {
			
			@Override
			public String getDescription() {
				return "Illumination Engine Model (.ilm)";
			}
			
			@Override
			public boolean accept(File f) {
				return f.getName().endsWith(".ilm") || !f.getName().contains(".");
			}
		});
		frame = new JFrame("Editor Pane");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel(new BorderLayout());
		frame.add(panel);
		
		panel.add(createLoadButton(parent), BorderLayout.SOUTH);
		panel.add(createSaveButton(parent), BorderLayout.NORTH);	
		
		JButton addLight = new JButton("Add Light");
		addLight.addActionListener(new ActionListener() {		
			@Override
			public void actionPerformed(ActionEvent arg0) {
				createLightEditorWindow(new PointLight(new Vector3(0,0,0), new Vector4(1,1,1,10)));
			}
		});
		panel.add(addLight, BorderLayout.CENTER);
		
		new MouseEventListener(0) {
			
			@Override
			public void mouseEventPerformed(MouseEvent e) {
				if (e.getType() == EventType.PRESSED) {
					if (e.getWorldLocation() != null) {
						createLightEditorWindow(new PointLight(e.getWorldLocation(), new Vector4(1,1,1,15)));
					}
				}
			}
		};
		
		
		frame.pack();
		frame.setVisible(true);
	}
	
	
	private void createLightEditorWindow(Light light) {
		new LightEditorWindow(light);
	}
	
	private JButton createSaveButton(final Interface parent) {
		JButton button = new JButton("Save");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int returnValue = saveChooser.showSaveDialog(frame);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File file = saveChooser.getSelectedFile();
					System.out.println("Saving to: " + file);
				}
			}
		});
		return button;
	}
	
	private JButton createLoadButton(final Interface parent) {
		JButton button = new JButton("Load");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int returnValue = openChooser.showOpenDialog(frame);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File file = openChooser.getSelectedFile();
					parent.loadOBJ(file);
				}
			}
		});
		return button;
	}
}
