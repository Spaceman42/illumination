package com.ten.inteface;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.ten.asset.AssetMesh;
import com.ten.asset.ModelLoader;
import com.ten.asset.SimpleMesh;
import com.ten.asset.Utils;
import com.ten.asset.Vector2;
import com.ten.asset.Vector3index;
import com.ten.asset.Vertex;
import com.ten.asset.binaryasset.AssetFile;
import com.ten.asset.binaryasset.Chunk;
import com.ten.asset.binaryasset.ModelChunk;
import com.ten.asset.util.VectorList;
import com.ten.math.Vector3;
import com.ten.math.Vector4;

public class CompundEditorObjectUtil extends EditorObject{
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String location = in.nextLine();
		System.out.println("loading model...");
		SaveCompoundModel(location);
		System.out.println("Finished");
	}
	
	public static void saveCompoundILM(File saveTo, Map<String, AssetMesh> meshs) {
		System.out.println("Saving model...");
		Object[] obids =  meshs.keySet().toArray();
		String[] ids = new String[obids.length];
		
		for (int i = 0; i<obids.length; i++) {
			ids[i] = (String)obids[i];
		}
		
		AssetFile assetFile = new AssetFile();
		for (int i = 0; i < ids.length; i++) {
			System.out.println("Converting mesh " + i);
			SimpleMesh m = convertAssetMesh(meshs.get(ids[i]), ids[i]);
			ModelChunk chunk = new ModelChunk(m.id, m.hasTextures, m.vertices, m.textures, m.normals, m.tangents, m.bitangents, m.indices);
			System.out.println(m.id);
			assetFile.addChunk(chunk);
		}
		meshs=null;
		System.gc();
		System.out.println("Chunks in model are:");
		Chunk[] savedChunks = assetFile.getChunks();
		for (Chunk c : assetFile.getChunks()) {
			System.out.println(c.getId());
		}
		System.out.println(assetFile.getChunks().length);
		assetFile.save(saveTo);
		
		AssetFile reTest = AssetFile.load(saveTo);
		System.out.println("Saving and reloading.... Testing chunks:");
		Chunk[] loadedChunks = reTest.getChunks();
		for (Chunk c : reTest.getChunks()) {
			System.out.println(c.getId());
		}
		System.out.println(reTest.getChunks().length);
		
		System.out.println("Testing for consistancy...");
		boolean fileOK = checkConsistancy(savedChunks, loadedChunks);
		if (fileOK) {
			System.out.println("File passed consistancy check");
		} else {
			System.out.println("File failed consitancy check");
		}
		System.out.println("Done converting model!");
	}

	public static void SaveCompoundModel(String location) {
		File loadFrom = new File(location + ".obj");
		File saveTo = new File(location + ".ilm");
		Map<String, AssetMesh> meshs = ModelLoader.loadModel(loadFrom);
		saveCompoundILM(saveTo, meshs);
		
	}
	
	private static boolean checkConsistancy(Chunk[] savedChunks, Chunk[] loadedChunks) {
		HashMap<String, Chunk> chunks = new HashMap<String, Chunk>();
		for (Chunk toAdd : loadedChunks) {
			chunks.put(toAdd.getId(), toAdd);
		}
		for (Chunk chunk : savedChunks) {
			if (chunks.containsKey(chunk.getId())) {
				Chunk chunk2 = chunks.get(chunk.getId());
				if (chunk2.getType() == Chunk.MODEL_CHUNK && chunk.getType() == Chunk.MODEL_CHUNK) {
					if (!modelChunksEqual((ModelChunk) chunk, (ModelChunk) chunk2)) {
						return false;
					}
				} else {
					System.err.println("Chunk type mismatch! id:" + chunk.getId());
					return false;
				}
			} else {
				System.err.println("Missing chunk! id: " + chunk.getId());
				return false;
			}
		}
		return true;
		
	}
	
	private static boolean modelChunksEqual(ModelChunk chunk, ModelChunk chunk2) {
		boolean indices = Arrays.equals(chunk.getIndices(), chunk2.getIndices());
		boolean vertices = Arrays.equals(chunk.getVertices(), chunk2.getVertices());
		boolean normals = Arrays.equals(chunk.getNormals(), chunk2.getNormals());
		boolean textures = Arrays.equals(chunk.getTextureCoordinates(), chunk2.getTextureCoordinates());
		boolean tangents = Arrays.equals(chunk.getTangents(), chunk2.getTangents());
		boolean bitangents = Arrays.equals(chunk.getBitangents(), chunk2.getBitangents());
		
		boolean[] comparasions = new boolean[] {indices, vertices, normals, textures, tangents, bitangents};
		String[] names = new String[] {"indices", "vertices", "normals", "textures", "tangents", "bitangents"};
		for (int i = 0; i<comparasions.length; i++) {
			if (!comparasions[i]) {
				System.err.println("Values in " + names[i] + " do not match!");
				return false;
			}
		}
		return true;
			
		
	}

	private static SimpleMesh convertAssetMesh(AssetMesh mesh, String id) {
		float[] vertices = mesh.getValue(AssetMesh.VERTICES);
		float[] normals = mesh.getValue(AssetMesh.NORMALS);
		float[] textures = mesh.getValue(AssetMesh.TEXTURES);
		int[] indices = mesh.getIndices();
		float[] tangents = null;
		float[] bitangents = null;
		boolean hasTextures = false;
		mesh = null;
		if (textures != null) {
			Vector3index[] verticesVec = Utils.expandVector3Array(vertices, 4, indices);
			Vector2[] texturesVec = Utils.expandVector2Array(textures, 2, indices);
			hasTextures = true;
			float[][] arrays = calculateTangentsAndBitangents(vertices, textures, indices);
			tangents = arrays[0];
			bitangents = arrays[1];
			
			
		
		}
		return new SimpleMesh(vertices, textures, normals, tangents, bitangents, indices, hasTextures, id);
	}
	
	private static float[][] calculateTangents(float[] vertices, float[] textures, int[] indices) {
		VectorList verts = new VectorList(vertices, 4);
		VectorList uvs = new VectorList(textures, 2);
		int[] vertexIndices = new int[indices.length];
	
		VectorList vertsFinal = new VectorList(indices.length, 4);
		
		System.out.println("Indices length: " + indices.length + ", VertsFinal size: " + vertsFinal.size());
		
		
		VectorList uvsFinal = new VectorList(indices.length, 2);
		for (int i = 0; i<indices.length; i++) {
			int index = indices[i];
			vertexIndices[i] = index;
			vertsFinal.add(verts.get(index));
			uvsFinal.add(uvs.get(index));
		}
		VectorList tangents = new VectorList(vertsFinal.size(), 3);
		VectorList bitangents = new VectorList(vertsFinal.size(), 3);
		for (int i = 0; i<vertsFinal.size(); i+=3) {
			Vector4 v0 = new Vector4().fromArray(vertsFinal.get(i));
			Vector4 v1 = new Vector4().fromArray(vertsFinal.get(i+1));
			Vector4 v2 = new Vector4().fromArray(vertsFinal.get(i+2));
			
			Vector2 uv0 = new Vector2(uvsFinal.get(i));
			Vector2 uv1 = new Vector2(uvsFinal.get(i+1));
			Vector2 uv2 = new Vector2(uvsFinal.get(i+2));
			
			Vector4 deltaPos1 = new Vector4(v1).subtract(v0);
			Vector4 deltaPos2 = new Vector4(v2).subtract(v0);
			
			Vector2 deltaUv1 = new Vector2(uv1).sub(uv0);
			Vector2 deltaUv2 = new Vector2(uv2).sub(uv0);
			
			float r = calcR(deltaUv1, deltaUv2);
			Vector4 pos1Muled1 = new Vector4(deltaPos1).multiply(deltaUv2.y);
			Vector4 pos2Muled1 = new Vector4(deltaPos2).multiply(deltaUv1.y);
			Vector3 tangent = new Vector3(pos1Muled1.subtract(pos2Muled1)).multiply(r);
			tangents.set(i, tangent.toArray());
			tangents.set(i+2, tangent.toArray());
			tangents.set(i+2, tangent.toArray());
			
			Vector4 pos1Muled2 = new Vector4(deltaPos1).multiply(deltaUv1.x);
			Vector4 pos2Muled2 = new Vector4(deltaPos2).multiply(deltaUv2.x);
			Vector3 bitangent = new Vector3(pos1Muled2.subtract(pos2Muled2)).multiply(r);
			bitangents.set(i, bitangent.toArray());
			bitangents.set(i+1, bitangent.toArray());
			bitangents.set(i+2, bitangent.toArray());
		}
		VectorList tansFinal = new VectorList(verts.size(), 4);
		VectorList bitansFinal = new VectorList(verts.size(), 4);
		boolean[] hasValue = new boolean[indices.length];
		for (boolean b : hasValue) {
			b = false;
		}
		System.out.println(vertsFinal.size());
		
		for (int i = 0; i<vertsFinal.size(); i++) {
			int index = vertexIndices[i];
			System.out.println("rebuilding index " + index);
			if (hasValue[index]) {
				System.out.println(new Vector3().fromArray(tangents.get(i)) + "Has Value!");
				Vector3 oldTan = new Vector3().fromArray(tansFinal.get(index));
				Vector3 newTan = new Vector3().fromArray(tangents.get(i));
				
				Vector4 tanFinal = new Vector4((newTan.add(oldTan)));
				tansFinal.set(index, tanFinal.toArray());
				
				Vector3 oldBi = new Vector3().fromArray(bitansFinal.get(index));
				Vector3 newBi = new Vector3().fromArray(bitangents.get(i));
				
				Vector4 bitanFinal = new Vector4((newBi.add(oldBi)));
				bitansFinal.set(index, bitanFinal.toArray());
			} else {
				System.out.println(new Vector3().fromArray(tangents.get(i)) + "Doesn't have value!");
				Vector4 tan = new Vector4(new Vector3().fromArray(tangents.get(i)));
				tan.w = 1;
				tansFinal.set(index, tan.toArray());
				
				Vector4 bitan = new Vector4(new Vector3().fromArray(bitangents.get(i)));
				bitan.w = 1;
				bitansFinal.set(index, bitan.toArray());
				hasValue[index] = true;
			}
		}
		return new float[][] {tansFinal.getRawArray(), bitansFinal.getRawArray()};
	}
	
	private static float[][] calculateTangentsAndBitangents(float[] vertices, float[] textures, int[] indices) {
		VectorList verts = new VectorList(vertices, 4);
		VectorList uvs = new VectorList(textures, 2);
		int[] vertexIndices = new int[indices.length];
	
		Vertex[] vertsFinal = new Vertex[indices.length];
		for (int i = 0; i<indices.length; i++) {
			int index = indices[i];
			vertexIndices[i] = index;
			vertsFinal[i] = new Vertex(new Vector3().fromArray(verts.get(index)), new Vector2(uvs.get(index)));
		}
		VectorList tangents = new VectorList(vertsFinal.length, 4);
		VectorList bitangents = new VectorList(vertsFinal.length, 4);
		
		for (int i = 0; i<vertsFinal.length; i+=3) {
			Vertex v0 = vertsFinal[i+0];
			Vertex v1 = vertsFinal[i+1];
			Vertex v2 = vertsFinal[i+2];
			
			Vector3 edge1 = v1.getPos().subtract(v0.getPos());
			Vector3 edge2 = v2.getPos().subtract(v0.getPos());
			
			float deltaU1 = v1.getUv().x - v0.getUv().x;
			float deltaV1 = v1.getUv().y - v0.getUv().y;
			float deltaU2 = v2.getUv().x - v0.getUv().x;
			float deltaV2 = v2.getUv().y - v0.getUv().y;
			
			float f = 1.0f/(deltaU1 * deltaV2 - deltaU2 * deltaV1);
			Vector4 tangent = new Vector4();
			Vector4 bitangent = new Vector4();
			
			tangent.x = f * (deltaV2 * edge1.x - deltaV1 * edge2.x);
			tangent.y = f * (deltaV2 * edge1.y - deltaV1 * edge2.y);
		    tangent.z = f * (deltaV2 * edge1.z - deltaV1 * edge2.z);
		    tangent.w = 1;
		    
		    bitangent.x = f * (-deltaU2 * edge1.x - deltaU1 * edge2.x);
		    bitangent.y = f * (-deltaU2 * edge1.y - deltaU1 * edge2.y);
		    bitangent.z = f * (-deltaU2 * edge1.z - deltaU1 * edge2.z);
		    bitangent.w = 1;
		    System.out.println("Values loaded into array: ");
		    System.out.println(tangent);
		    System.out.println(bitangent);
		    tangents.add(tangent.toArray());
		    tangents.add(tangent.toArray());
		    tangents.add(tangent.toArray());
		    bitangents.add(bitangent.toArray());
		    bitangents.add(bitangent.toArray());
		    bitangents.add(bitangent.toArray());
		}
		
		VectorList tansFinal = new VectorList(verts.size(), 4);
		VectorList bitansFinal = new VectorList(verts.size(), 4);
		boolean[] hasValue = new boolean[indices.length];
		for (int i = 0; i<hasValue.length; i++) {
			hasValue[i] = false;
		}
		if (indices.length != vertsFinal.length) {
			System.out.println("WTF");
			System.exit(-1);
		}
		for (int i = 0; i<verts.size(); i ++) {
			tansFinal.add(new float[]{0,0,0,0});
			bitansFinal.add(new float[]{0,0,0,0});
		}
		for (int i = 0; i<indices.length; i++) {
			int index = vertexIndices[i];
			System.out.println("Rebuilding index: " + index);
			if (!hasValue[index]) {
				System.out.println("does not have index");
				tansFinal.set(index, tangents.get(i));
				bitansFinal.set(index, bitangents.get(i));
				System.out.println(new Vector4().fromArray(tansFinal.get(index)));
				hasValue[index] = true;
				if (tangents.get(i)[0] == Float.NaN) {
					System.out.println("tangent failed");
					System.exit(-1);
				}
				if (bitangents.get(i)[0] == Float.NaN) {
					System.out.println("Bitangent failed");
					System.exit(-1);
				}
			} else {
				System.out.println("has index");
				Vector4 tangent = new Vector4().fromArray(tangents.get(i));
				Vector4 currentTangent = new Vector4().fromArray(tansFinal.get(index));
				currentTangent.add(tangent);
				currentTangent.w = 1;
				tansFinal.set(index, currentTangent.toArray());
				if (currentTangent.x == Float.NaN) {
					System.out.println("tangent failed");
					System.exit(-1);
				}
				System.out.println(currentTangent);
				Vector4 bitangent = new Vector4().fromArray(bitangents.get(i));
				Vector4 currentBitangent = new Vector4().fromArray(bitansFinal.get(index));
				currentBitangent.add(bitangent);
				currentBitangent.w = 1;
				if (currentBitangent.x == Float.NaN) {
					System.out.println("Bitangent failed");
					System.exit(-1);
				}
				bitansFinal.set(index, currentBitangent.toArray());
				System.out.println(currentBitangent);
			}
		}
	
		for (int i = 0; i<verts.size(); i++) {
			Vector4 tangent = new Vector4().fromArray(tansFinal.get(i));
			tangent.normalise();
			tansFinal.set(i, tangent.toArray());
			System.out.println("Tangent " + i + ": " + tangent);
			
			Vector4 bitangent = new Vector4().fromArray(bitansFinal.get(i));
			bitangent.normalise();
			bitansFinal.set(i, bitangent.toArray());
			System.out.println("Bitangent " + i + ": " + bitangent);
		}
		System.out.println("Tangents size: " + tansFinal.size() + " Bitangents size: " + bitansFinal.size());
		return new float[][] {tansFinal.getRawArray(), bitansFinal.getRawArray()};
		
	}

	private static float calcR(Vector2 deltaUv1, Vector2 deltaUv2) {
		return 1f/(deltaUv1.x * deltaUv2.y - deltaUv1.y * deltaUv2.x);
	}

	
}
