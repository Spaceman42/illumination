package com.ten.inteface;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.ten.asset.AssetMesh;
import com.ten.asset.ModelLoader;
import com.ten.asset.Utils;
import com.ten.asset.Vector2;
import com.ten.asset.Vector3index;
import com.ten.asset.binaryasset.AssetFile;
import com.ten.asset.binaryasset.LightChunk;
import com.ten.asset.binaryasset.ModelChunk;
import com.ten.game.GameObject;
import com.ten.graphics.RenderableObject;
import com.ten.graphics.renderer.light.Light;
import com.ten.graphics.renderer.light.PointLight;
import com.ten.graphics.renderer.model.Model;
import com.ten.graphics.shader.Shader;
import com.ten.graphics.shader.uniform.UniformNotFoundException;
import com.ten.graphics.shader.uniform.UniformVector3;
import com.ten.io.keyboard.IOKeyboard;
import com.ten.io.keyboard.Key;
import com.ten.math.Matrix4;
import com.ten.math.Vector3;
import com.ten.math.Vector4;
import com.ten.physics.PhysicalObject;
import com.ten.physics.PhysicsRigidBody;
import com.ten.physics.shape.BoundingBox;
import com.ten.physics.shape.BoundingShape;

public class EditorObject implements RenderableObject, GameObject,
		PhysicalObject {

	protected Vector4 translation;
	protected Model model;
	private String name;
	private String fileLocation;

	private static final Vector3 CLICKED_COLOR = new Vector3(0, 1, 0);
	private static final Vector3 NORMAL_COLOR = new Vector3(1, 1, 1);
	private List<Light> lights;
	private Vector3 color = NORMAL_COLOR;

	// protected RigidBody rigidBody = null;
	protected PhysicsRigidBody body = null;
	
	public EditorObject() {};

	public EditorObject(String modelLocation, Vector4 location, String name) {
		this.fileLocation = modelLocation;
		model = Model.loadModel(modelLocation, "res/noimage.png");
		this.translation = new Vector4(location);
		lights = new ArrayList<Light>();
		this.name = name;

	}

	@Override
	public PhysicsRigidBody getRigidBody() {
		return body;
	}

	@Override
	public void act() {
		if (model.loaded() && body == null) {
			body = new PhysicsRigidBody(model.getBBs()[0], new Vector3(
					translation.x, translation.y, translation.z), new Vector3(
					0, 0, 0), new Vector3(), 0f, 0.3f, 0.3f, this);
		}

		if (IOKeyboard.isKeyDown(Key.KEY_SPACE) && body != null) {
			body.setVelocity(new Vector3(0, 3, 0));
		}

		if (body != null) {
			Vector3 trans = body.getTranslation();
			translation = new Vector4(trans.x, trans.y, trans.z);
		}

	}

	@Override
	public void delete() {
		// TODO Auto-generated method stub

	}

	@Override
	public void draw(Shader shader) {
		shader.setUniform("colorIN", new UniformVector3(color));
		model.draw(shader, 0);

	}

	@Override
	public Matrix4 getModelMatrix() {
		// TODO Auto-generated method stub
		return new Matrix4().translate(translation);
	}

	@Override
	public boolean loaded() {
		return model.loaded() && (body != null);
	}

	@Override
	public BoundingShape[] getBBs() {
		return model.getBBs();
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public void onClick(Vector4 worldSpaceLocation) {
		lights.add(new PointLight(new Vector3(worldSpaceLocation), new Vector4(1,1,1,20)));
	}

	public void save(File file) {
		AssetMesh mesh = ModelLoader.loadModel(new File(fileLocation + ".obj"))
				.get("default");
		float[] vertices = mesh.getValue(AssetMesh.VERTICES);
		float[] normals = mesh.getValue(AssetMesh.NORMALS);
		float[] textures = mesh.getValue(AssetMesh.TEXTURES);
		float[] tangents = new float[(vertices.length/4)*3];
		float[] bitangents = new float[(vertices.length/4)*3];
		int[] indices = mesh.getIndices();
		boolean hasTextures = false;
		
		if (textures != null) {
			Vector3index[] verticesVec = Utils.expandVector3Array(vertices, 4, indices);
			Vector2[] texturesVec = Utils.expandVector2Array(textures, 2, indices);
			hasTextures = true;
			
			
			
			Vector3[][] arrays = calculateTangents(texturesVec, verticesVec, vertices.length/4);
			
			Vector3[] biTangentsOut = arrays[0];
			Vector3[] tangentsOut = arrays[1];
			
			List<Float> tangentsFloat = new ArrayList<Float>();
			List<Float> biTangentsFloat = new ArrayList<Float>();
			
			for (Vector3 v : tangentsOut) {
				Float[] array = new Float[3];
				float[] farray = v.toArray();
				
				for (int i = 0; i<array.length; i++) {
					array[i] = farray[i];
				}	
				
				tangentsFloat.addAll(Arrays.asList(array));
			}
			for (Vector3 v : biTangentsOut) {
				Float[] array = new Float[3];
				float[] farray = v.toArray();
				
				for (int i = 0; i<array.length; i++) {
					array[i] = farray[i];
				}	
				
				biTangentsFloat.addAll(Arrays.asList(array));
			}
			
			tangents = new float[tangentsFloat.size()];
			bitangents = new float[biTangentsFloat.size()];
			
			for (int i = 0; i<tangents.length; i++) {
				tangents[i] = tangentsFloat.get(i);
			}
			for (int i = 0; i<bitangents.length; i++) {
				bitangents[i] = biTangentsFloat.get(i);
			}
			
		}
		System.out.println("Done testsaving");
		AssetFile assetFile = new AssetFile();
		assetFile.addChunk(new ModelChunk("mesh", hasTextures, vertices, textures, normals, tangents, bitangents, indices));
		for (int i = 0; i<lights.size(); i++) {
			assetFile.addChunk(new LightChunk(String.valueOf(i), lights.get(i).getLocation(), lights.get(i).getColor()));
		}
		
		assetFile.save(file);
		
	}
	
	private Vector3[][] calculateTangents(Vector2[] textures, Vector3index[] vertices, int oldLength) {
		Vector3[] tangents = new Vector3[vertices.length];
		Vector3[] biTangents = new Vector3[vertices.length];
		
		for (int i = 0; i<vertices.length; i+=3) {
			Vector3 v0 = vertices[i];
			Vector3 v1 = vertices[i+1];
			Vector3 v2 = vertices[i+2];
			
			Vector2 uv0 = textures[i];
			Vector2 uv1 = textures[i+1];
			Vector2 uv2 = textures[i+2];
			
			Vector3 deltaPos1 = v1.subtract(v0);
			Vector3 deltaPos2 = v2.subtract(v0);
			
			Vector2 deltaUV1 = uv1.sub(uv0);
			Vector2 deltaUV2 = uv2.sub(uv0);
			
			float r = 1f/(deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
			Vector3 tangent = new Vector3(new Vector3(deltaPos1).multiply(deltaUV2.y).subtract(new Vector3(deltaPos2).multiply(deltaUV1.y)));
			tangent.multiply(r);
			Vector3 biTangent = new Vector3(new Vector3(deltaPos2).multiply(deltaUV1.x).subtract(new Vector3(deltaPos1).multiply(deltaUV2.x)));
			biTangent.multiply(r);
			
			tangents[i] = new Vector3(tangent);
			tangents[i+1] = new Vector3(tangent);
			tangents[i+2] = new Vector3(tangent);
			
			biTangents[i] = new Vector3(biTangent);
			biTangents[i+1] = new Vector3(biTangent);
			biTangents[i+2] = new Vector3(biTangent);
		}
		
		//Recombine vertices
		Vector3[] tangentsCombined = new Vector3[oldLength];
		Vector3[] biTangentsCombined = new Vector3[oldLength];
		for (int i = 0; i<vertices.length; i++) {
			int index = vertices[i].getIndex();
			if (tangentsCombined[index] == null) {
				tangentsCombined[index] = tangents[i];
			} else {
				tangentsCombined[index].add(tangents[i]);
			}
			if (biTangentsCombined[index] == null) {
				biTangentsCombined[index] = biTangents[i];
 			} else {
 				biTangentsCombined[index].add(biTangents[i]);
 			}
		}
		
		return new Vector3[][] {tangentsCombined, biTangentsCombined};
		
	}

}
