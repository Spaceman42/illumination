package com.ten.inteface;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.ten.graphics.renderer.light.Light;
import com.ten.graphics.renderer.light.PointLight;
import com.ten.math.Vector3;
import com.ten.math.Vector4;

public class LightEditorWindow extends JFrame{
	private final JTextField[] location = new JTextField[3];
	private final JTextField[] color = new JTextField[4];
	
	
	public LightEditorWindow(final Light light) {
		super("Light Creator");
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		JPanel panel = new JPanel(new BorderLayout());
		add(panel);
		
		JButton doneButton = new JButton("Done");
		doneButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		panel.add(doneButton, BorderLayout.SOUTH);
		
		JPanel toolsPanel = new JPanel();
		panel.add(toolsPanel, BorderLayout.CENTER);
		JPanel locationControl = new JPanel(new BorderLayout());
		toolsPanel.add(locationControl, BorderLayout.NORTH);
		setUpLocationControls(locationControl, light);
		
		JPanel colorControl = new JPanel(new BorderLayout());
		toolsPanel.add(colorControl, BorderLayout.SOUTH);
		setUpColorControls(colorControl, light);
		
		JButton updateLightButton = new JButton("Upadte Light");
		updateLightButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				float[] fColor = new float[3];
				for (int i = 0; i<3; i++) {
					fColor[i] = Float.parseFloat(color[i].getText());
				}
				Vector4 vColor = new Vector4(new Vector3().fromArray(fColor));
				vColor.w = 10;
				float[] fLoc = new float[3];
				for (int i = 0; i<3; i++) {
					fLoc[i] = Float.parseFloat(location[i].getText());
				}
				Vector3 vLoc = new Vector3().fromArray(fLoc);
				light.setColor(vColor);
				light.setLocation(vLoc);
			}
		});
		panel.add(updateLightButton, BorderLayout.EAST);
		
		
		
		pack();
		setVisible(true);
	}

	private void setUpColorControls(JPanel colorControl, Light light) {
		JTextField x = new JTextField();
		x.setColumns(6);
		x.setText(String.valueOf(light.getColor().x));
		color[0] = x;
		
		JTextField y = new JTextField();
		y.setColumns(6);
		y.setText(String.valueOf(light.getColor().y));
		color[1] = y;
		
		JTextField z = new JTextField();
		z.setColumns(6);
		z.setText(String.valueOf(light.getColor().z));
		color[2] = z;
		
		colorControl.add(new JLabel("Color"), BorderLayout.NORTH);
		colorControl.add(x, BorderLayout.WEST);
		colorControl.add(y, BorderLayout.CENTER);
		colorControl.add(z, BorderLayout.EAST);
	}

	private void setUpLocationControls(JPanel locationControl, Light light) {
		JTextField x = new JTextField();
		x.setColumns(6);
		x.setText(String.valueOf(light.getLocation().x));
		location[0] = x;
		
		JTextField y = new JTextField();
		y.setColumns(6);
		y.setText(String.valueOf(light.getLocation().y));
		location[1] = y;
		
		JTextField z = new JTextField();
		z.setColumns(6);
		z.setText(String.valueOf(light.getLocation().z));
		location[2] = z;
		locationControl.add(new JLabel("Location"), BorderLayout.NORTH);
		locationControl.add(x, BorderLayout.WEST);
		locationControl.add(y, BorderLayout.CENTER);
		locationControl.add(z, BorderLayout.EAST);
	}
}
