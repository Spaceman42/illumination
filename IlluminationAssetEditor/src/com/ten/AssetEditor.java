package com.ten;

import java.io.IOException;
import java.util.Scanner;

import com.ten.inteface.Interface;


public class AssetEditor {
	
	public static void main(String[] args) throws IOException {
		AssetEditor main = new AssetEditor();
		
		main.run();
	}
	
	
	public void run() throws IOException {
		GameEngine engine = new GameEngine(new Interface());
		engine.run();
	}
}
